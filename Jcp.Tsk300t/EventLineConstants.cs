﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    internal class EventLineConstants
    {
        internal const string H1_GET_PORT1_RUN = "H1 Get Port1 Run!";
        internal const string H1_PUT_PORT1_RUN = "H1 Put Port1 Run!";
        internal const string H1_GET_PORT2_RUN = "H1 Get Port2 Run!";
        internal const string H1_PUT_PORT2_RUN = "H1 Put Port2 Run!";
        internal const string LOT_OPERATION_START = "LOT OPERATION START.";
        internal const string LOT_OPERATION_FINISH = "LOT OPERATION FINISH.";
        internal const string ABORT_OPERATION = "ABORT OPERATION.";
        internal const string PORT_1_FINISH = "PORT-1 FINISH.";
        internal const string PORT_2_FINISH = "PORT-2 FINISH.";
        internal const string GR1_READY_ON = "GR1 READY ON";
        internal const string GR2_READY_ON = "GR2 READY ON";
        internal const string AUTO_MODE_SELECT = "AUTO MODE SELECT";
        internal const string SEMI_AUTO_MODE_SELECT = "SEMI AUTO MODE SELECT";
        internal const string MANUAL_MODE_SELECT = "MANUAL MODE SELECT";
    }
}
