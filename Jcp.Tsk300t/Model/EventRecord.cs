﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Jcp.Pg300
{
    public class EventRecord
    {
        public string SubCommand { get; set; }
        public DateTime GenerationDate { get; set; }
        public int LogId { get; set; }
        
        public string Param1 { get; set; }
        public string NumericDataID { get; set; }
    }
}
