﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
   public class EventLinkReport
    {
        public uint[] CEID { get; set; }

        public List<uint[]> ReportIds { get; set; }
    }
}
