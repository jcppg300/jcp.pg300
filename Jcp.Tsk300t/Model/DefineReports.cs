﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    public class DefineReports
    {
        public uint[] ReportId { get; set; }
        public uint[] DataId { get; set; }

        public List<uint[]> SvidList { get; set; }

    }   

}
