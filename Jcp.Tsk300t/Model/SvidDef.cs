﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    internal class SvidDef
    {
        /// <summary>
        /// 程序名
        /// </summary>
        internal const int RecipeName = 1;
        /// <summary>
        /// 设备状态
        /// </summary>
        internal const int ProcessState = 2;
        internal const int PreviousProcessState = 16;
        /// <summary>
        /// 控制状态
        /// </summary>
        internal const int ControlState = 3;

        internal const int GR1_BLADE_HEIGHT = 4;
        internal const int GR2_BLADE_HEIGHT = 5;
        internal const int AIR_SOURCE_PRESSURE = 6;
        internal const int GRINDING1_St_CHUCK_VACUUM = 7;
        internal const int GRINDING2_St_CHUCK_VACUUM = 8;
        internal const int GRINDING1_St_WHEEL_REVOLVING = 9;
        internal const int GRINDING2_St_WHEEL_REVOLVING = 10;

        internal const int Port1State = 11;
        internal const int Port2State = 12;

        internal const int VACUUM_SOURCE_PRESSURE = 13;
        internal const int GRINDING1_St_COOLANT_FLOWRATE = 14;
        internal const int GRINDING2_St_COOLANT_FLOWRATE = 15;
        

        /// <summary>
        /// sp1刀痕宽度
        /// </summary>
        internal const int Sp1KcWMin = 180001;
        /// <summary>
        /// sp1单侧最大
        /// </summary>
        internal const int Sp1KcHalfW = 180002;
        /// <summary>
        /// sp1偏离中心
        /// </summary>
        internal const int Sp1KcDy = 180003;
        /// <summary>
        /// sp1崩边
        /// </summary>
        internal const int Sp1KcChippingSize = 180004;
        /// <summary>
        /// sp1刀痕最大宽度
        /// </summary>
        internal const int Sp1KcWMax = 180005;
        /// <summary>
        /// sp1面数
        /// </summary>
        internal const int Sp1KcChannel = 180006;


        /// <summary>
        /// sp2刀痕宽度
        /// </summary>
        internal const int Sp2KcWMin = 280001;
        /// <summary>
        /// sp2单侧最大
        /// </summary>
        internal const int Sp2KcHalfW = 280002;
        /// <summary>
        /// sp2偏离中心
        /// </summary>
        internal const int Sp2KcDy = 280003;
        /// <summary>
        /// sp2崩边
        /// </summary>
        internal const int Sp2KcChippingSize = 280004;
        /// <summary>
        /// sp2刀痕最大宽度
        /// </summary>
        internal const int Sp2KcWMax = 280005;
        /// <summary>
        /// sp2面数
        /// </summary>
        internal const int Sp2KcChannel = 280006;



        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public Object Value { get; set; } = string.Empty;
    }
}
