﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{

    internal class ProdData
    {
        internal class Blade
        {
            public string Name { get; set; }
            public string Speed { get; set; }

            public string Height { get; set; }
            public string Expose { get; set; }

            public string Distance { get; set; }
        }

        public string RecipeName { get; set; }
        public string FeedSpeed { get; set; }
        public Blade Blade1 { get; set; } = new Blade();
        public Blade Blade2 { get; set; } = new Blade();
    }
}
