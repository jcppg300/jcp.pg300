﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    public class State
    {
        public string EquipmentStatus { get; set; }
        public string ProcessStatus { get; set; }

    }
}
