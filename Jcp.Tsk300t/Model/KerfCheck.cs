﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    public class KerfCheck
    {
        /// <summary>
        /// 轴位
        /// </summary>
        public int Spindle { get; set; } 
        /// <summary>
        /// 刀痕最大宽度，VARID_MULPOINT_AVRG_MAXW
        /// </summary>
        public double WMax { get; set; }
        /// <summary>
        /// 刀痕宽度
        /// </summary>
        public double WMin {get;set;}
        /// <summary>
        /// 单侧最大
        /// </summary>
        public double HalfW { get; set; }
        /// <summary>
        /// 偏离中心
        /// </summary>
        public double Dy { get; set; }
        /// <summary>
        /// 崩边
        /// </summary>
        public double ChippingSize { get; set; }
        /// <summary>
        /// 面数，哪一面, CH就是指90度切割还是180度切割，和第几把刀没有关系. VARID_MULTIPOINT_CURRENT_CH,
        /// </summary>
        public string Channel { get; set; } = string.Empty;
    }
}
