﻿

using ManagedWinapi.Windows;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun 
    {


        public void UpdateStatusText(DataTable dt)
        {
            try
            {
                DataRow[] datarows = dt.Select("MainCommand = 10001 and SubCommand =100", "GenerationDate desc");
                if (datarows.Length > 0)
                {
                    txtProductStatus.Text = Enum.GetName(typeof(StatusEnum.ProductStatusEnum), int.Parse(datarows[0]["Param1"].ToString()));
                    txtEQPstatus.Text = Enum.GetName(typeof(StatusEnum.EquipementStatusEnum), int.Parse(datarows[0]["Param2"].ToString()));
                    txtSemiStatus.Text = Enum.GetName(typeof(StatusEnum.ProcessStatusEnum), int.Parse(datarows[0]["Param3"].ToString()));

                    _dctSvidDef[SvidDef.ProcessState].Value = Enum.GetName(typeof(StatusEnum.ProcessStatusEnum), int.Parse(datarows[0]["Param3"].ToString()));
                }
            }
            catch (Exception ex)
            {
                _logger.Error("status strive error" + ex.Message);
            }

        }

        private void btnGetfromForm_Click(object sender, EventArgs e)
        {

            //var stautsArr = AutoItXNet20.GetText("[ID:1037]").Split(':');
            string[] stautsArr = { "", _semiStatus };
            var prostaus = "";
            var processstatus = "";
            if (stautsArr.Length > 1)
            {
                prostaus = stautsArr[0];
                processstatus = stautsArr[1];
            }
            //var alarmInfo = AutoItXNet20.GetAlarmText("[ID:5430]");
            //var alarmCode = AutoItXNet20.GetAlarmText("[ID:1254]");
            //var eqpstatus = AutoItXNet20.GetAlarmText("[ID:1038]");
            var alarmInfo = _alarmInfo;
            var alarmCode = _alarmCode;
            //var eqpstatus = AutoItXNet20.GetAlarmText("[ID:1038]");
            if (string.IsNullOrEmpty(this.txtProductStatus.Text))
            {
                this.txtProductStatus.Text = prostaus;
                this.txtSemiStatus.Text = processstatus;
                //this.txtEQPstatus.Text = eqpstatus;
                //this.txtsystemInfo.Text = systeminfo;
                this.txtAlarm.Text = alarmInfo;
                this.txtAlarmCode.Text = alarmCode;
            }
        }

        private List<string> _lstPpid = new List<string>();
        private void btnAllRecipeList_Click(object sender, EventArgs e)
        {
            _lstPpid = RecipeList();
            _logger.Info(JsonConvert.SerializeObject(_lstPpid));

            RecipeList().ForEach(x => lsboxRecipe.Items.Add(x));
        }
        private void btnPPSelect_Click(object sender, EventArgs e)
        {
            try
            {
                //_autoItX.ControlClick("", "", "[CLASS: Button; INSTANCE: 19]");

                //var selectRcp = lsboxRecipe.SelectedItem.ToString();
                //var index = _lstPpid.FindIndex(x => x == selectRcp);

                //_logger.Info("current recipe index: " + index);

                //var sw = SystemWindow.FromPoint(158, 388);
                //var listbox = SystemListBox.FromSystemWindow(sw);

                //listbox.Select(index);

                var choosedRcp = lsboxRecipe.SelectedItem.ToString();
                SelectRecipe(choosedRcp);
            }
            catch (Exception ex)
            {
                _logger.Error(ex); //薛总怎么老是不写日志？？？
            }
        }


        private void btnPpSelect2_Click(object sender, EventArgs e)
        {
            if(IsModeButtonShown() == false)
            {
                throw new Exception("machine is not in proper state");
            }

            //menu window
            var sw = SystemWindow.FromPoint(20, 676);
            sw.Title = "ButtonMenu";
           
            //_autoItX.ControlClick("", "ButtonMenu", "[CLASS: Button; INSTANCE: 19]");
            //_autoItX.ControlClick("ButtonMenu", "", "[ID:21]");
            //_autoItX.ControlClick("ButtonMenu", "ButtonMenu", "[ID:21]");
            //_autoItX.ControlClick("", "ButtonMenu", "[ID:21]");

            _autoIt.ControlEnable("", "ButtonMenu", "[ID:2]");
            var ret  = _autoIt.ControlClick("", "ButtonMenu", "[ID:2]");
            _logger.Info("control click ret:" + ret);

            Thread.Sleep(100);
            _autoIt.ControlEnable("", "ButtonMenu", "[ID:21]");
            var ret2 = _autoIt.ControlClick("", "ButtonMenu", "[ID:21]");
            _logger.Info("control click ret2:" + ret2);

            Thread.Sleep(100);
            _autoIt.ControlClick("", "SHOW", "[ID:30600]");
        }




        private void btnCurrentRecipe_Click(object sender, EventArgs e)
        {
            txtCurrentRecipe.Text = GetCurrentRecipeName();
        }



        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                string btnText1 = AutoItXNet20.GetText("开始");
                if (btnText1 == "开始")
                    AutoItXNet20.SendButtonClick("", "开始");
                _logger.Info("btnText1: -------------" + btnText1);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                string btnText = AutoItXNet20.GetText("停止");
                if (btnText == "停止")
                    AutoItXNet20.SendButtonClick("", "停止");
                _logger.Info("btnText: -------------" + btnText);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }


        private void btnUnloadWafer_Click(object sender, EventArgs e)
        {
            try
            {
                AutoItXNet20.SendAlarmButtonClick("清除", "[ID:5439]");
                Thread.Sleep(500);
                AutoItXNet20.LeftClickByMouse(1028, 8);
                _logger.Info("click manual operation ");
                Thread.Sleep(500);
                AutoItXNet20.SendButtonClick("下传", "[ID:1703]");
                _logger.Info("click xiachuan ");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }



        

    }
}
