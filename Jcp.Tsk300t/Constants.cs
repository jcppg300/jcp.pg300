﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    internal class Constants
    {
        internal const string AppFolder = @"D:\PG300";
        internal const string RecipeFolder = @"D:\PG300\RECIPE";
        internal const string RecipeOrderSetFolder = @"D:\PG300\ORDERSET";
        //internal const string RecipeFolder = @"D:\PG300\ORDERSET";
        //internal const string RecipeFolderOnServer = @"D:\Server\PG300\RECIPE";
        //internal const string RecipeOrderSetFolderOnServer = @"D:\Server\PG300\ORDERSET";
        internal const string CurrentRecipeFile = @"CURRENT.DAT";
        internal const string AlarmLogFolder = @"D:\PG300\LOG\ALARMLOG";
        internal const string OperationLogFolder = @"D:\PG300\LOG\OPELOG";
        internal const string LotLogFolder = @"D:\PG300\LOG\LOTLOG";
        internal const string ParamFolder = @"D:\PG300\PARAM";


        //internal const string AlarmLogFolder = @"E:\Customer\Jcap\Prj-Pg300\pg300-installed\LOG\ALARMLOG";
        //internal const string OperationLogFolder = @"E:\Customer\Jcap\Prj-Pg300\pg300-installed\LOG\OPELOG";


        //internal const string AppFolder = @"C:\Users\i\Desktop\PG300";
        ////internal const string RecipeFolder = @"C:\Users\i\Desktop\PG300\ORDERSET";
        //internal const string RecipeOrderSetFolder = @"C:\Users\i\Desktop\pg300\ORDERSET";
        ////internal const string RecipeOrderSetFolderOnServer = @"C:\Users\i\Desktop\Server\pg300\ORDERSET";
        //internal const string RecipeFolder = @"C:\Users\i\Desktop\pg300\RECIPE";
        //internal const string RecipeFolderOnServer = @"C:\Users\i\Desktop\Server\pg300\RECIPE";
        //internal const string CurrentRecipeFile = @"CURRENT.DAT";
        //internal const string OperationLogFolder = @"C:\Users\i\Desktop\pg300\LOG\OPELOG";
        //internal const string AlarmLogFolder = @"C:\Users\i\Desktop\pg300\LOG\ALARMLOG";
        //internal const string LotLogFolder = @"C:\Users\i\Desktop\pg300\LOG\LOTLOG";
        //internal const string ParamFolder = @"C:\Users\i\Desktop\pg300\PARAM";
    }
}
