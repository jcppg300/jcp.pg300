﻿using AutoItX3Lib;
using Ctg.XTap.SecsGem;
using Jcp.Pg300;
using Jcp.Pg300.Properties;
using log4net.Core;
using log4net.Repository.Hierarchy;
using ManagedWinapi.Windows;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using static Jcp.Pg300.StatusEnum;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun : Form
    {
        private HsmsHost _secs;
        private log4net.ILog _logger = log4net.LogManager.GetLogger("App");
        private FileSystemWatcher watcher;//未使用的声明

        //这三个委托需要用么？
        private delegate void UpdateWatchTextDelegate(DataTable dt);
        private delegate void GetAlarmDelegate(DataTable dt);
        private delegate void GetEventDelegate(DataTable dt);

        private SecsItemManager _secsItemManager = new SecsItemManager();

        //private AutoItX3Class _autoIt = new AutoItX3Class();
        //private Dm.dmsoft _dm = new Dm.dmsoft();
        private AutoItX3Class _autoIt;
        private Dm.dmsoft _dm;

        private bool _StartBtnState = false;
        private bool _controlState = true;

        private Dm.dmsoft _dmOfProcessstate;
        private Dm.dmsoft _dmOfCOUNTER;
        private Dm.dmsoft _dmOfUTILITY;
        private Dm.dmsoft _dmOfVACUUM;
        private Dm.dmsoft _dmOfGRINDING;
        string _dmOfCOUNTERHwnd;
        string _dmOfGRINDINGHwnd;
        string _dmOfVACUUMHwnd;
        string _dmOfUTILITYHwnd;
        public string DmOfCOUNTERHwnd
        {
            get => _dmOfCOUNTERHwnd;
            set
            {
                _dmOfCOUNTERHwnd = value;
                _dmOfCOUNTER = new CustomDmsoftClass(_dmOfCOUNTERHwnd);
            }
        }
        public string DmOfUTILITYHwnd
        {
            get => _dmOfUTILITYHwnd;
            set
            {
                _dmOfUTILITYHwnd = value;
                _dmOfUTILITY = new CustomDmsoftClass(_dmOfUTILITYHwnd);
            }
        }
        public string DmOfVACUUMHwnd
        {
            get => _dmOfVACUUMHwnd;
            set
            {
                _dmOfVACUUMHwnd = value;
                _dmOfVACUUM = new CustomDmsoftClass(_dmOfVACUUMHwnd);
            }
        }
        public string DmOfGRINDINGHwnd
        {
            get => _dmOfGRINDINGHwnd;
            set
            {
                _dmOfGRINDINGHwnd = value;
                _dmOfGRINDING = new CustomDmsoftClass(_dmOfGRINDINGHwnd);
            }
        }


        #region debugcode
        [StructLayout(LayoutKind.Sequential)]
        private struct MEMORYSTATUSEX
        {
            public uint dwLength;
            public uint dwMemoryLoad;
            public ulong ullTotalPhys;
            public ulong ullAvailPhys;
            public ulong ullTotalPageFile;
            public ulong ullAvailPageFile;
            public ulong ullTotalVirtual;
            public ulong ullAvailVirtual;
            public ulong ullAvailExtendedVirtual;
        }
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("Kernel32.dll")]
        private static extern bool GlobalMemoryStatusEx(ref MEMORYSTATUSEX lpBuffer);
        #endregion


        public SECSEngineRun()
        {
            InitializeComponent();

            //如果需要用Enginner Tab调试
            this.tabPage2.Parent = null;
            this.tabPage3.Parent = null;
        }




        private void SECSEngineRun_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info($"start application, {Assembly.GetExecutingAssembly().FullName} {Assembly.GetExecutingAssembly().ImageRuntimeVersion}");

                //等待机台软件窗口出现
                while (true)
                {
                    try
                    {
                        var sw = FindPg300Window();
                        sw.Title = "PG300 Window";
                        break;
                    }
                    catch (Exception exc)
                    {
                        _logger.Info(exc.Message);
                        Thread.Sleep(1000);
                    }
                }

                //testcode
                //new Thread(WatchMemoryState).Start();
                //new Thread(GetLogOfTaskList).Start();

                SecsHost.CheckValidationWithLicense();

                InitLogWatching();

                //Cmd.Instance.Regsvr32("AutoItX3.dll");
                //Cmd.Instance.Regsvr32("dm.dll");
                //文件夹名中不可以有空格，否则将注册失败
                Cmd.Instance.Regsvr32(Path.Combine(Application.StartupPath, "AutoItX3.dll"));
                Cmd.Instance.Regsvr32(Path.Combine(Application.StartupPath, "dm.dll"));
                //注册完成后需要等待一秒，否则将报错 
                //等一秒不够，有时候还是会报错，改为等5秒
                Thread.Sleep(1000 * 5);

                try
                {
                    _autoIt = new AutoItX3Class();
                    _dm = new Dm.dmsoft();
                }
                catch (Exception excp)
                {
                    _logger.Error(excp);
                    //首次Regsvr32有可能失败，重启后可能可以成功
                    Thread.Sleep(3000);
                    MessageBox.Show("please restart this program", "EAP for PG300", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    System.Environment.Exit(0);
                }

                //Directory.GetCurrentDirectory()不可行，启动路径和程序文件路径不同
                //_dm.SetPath(Directory.GetCurrentDirectory());
                _dm.SetPath(Application.StartupPath);
                _dm.SetDict(0, "dm_soft.txt");

                //if (Settings.Default.isDebugMode.ToUpper().Equals("TRUE"))
                //{
                //    while (true)
                //    {
                //        var dm = new CustomDmsoftClass("0x0000000000020710");
                //        _logger.Info(dm);
                //        //var dm0 = new CustomDmsoftClass("0x0000000000020719");
                //        //_logger.Info(dm0);
                //        string ocrRet = _dm.Ocr(4000, 15, 6400, 40, "ffffff-000000", 1.0);
                //        _logger.Info(ocrRet);
                //        Thread.Sleep(3000);
                //    }
                //}

                //if (Settings.Default.Thread_StartBtnState_Handler.ToUpper().Equals("TRUE"))
                //{
                //    new Thread(StartBtnState_Handler).Start();
                //}
                new Thread(RemoveLogOfNextDay).Start();
                new Thread(WatchWarmingUpModelEvent).Start();
                new Thread(WatchPortReadyEvent).Start();
                new Thread(WatchAlarmClear_winState).Start();


                //开机自启
                //RegistryKey RKey = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run");
                //RegistryKey RKey = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run");
                //RKey.SetValue("normal", Path.Combine(Application.StartupPath, "Jcp.Pg300.exe"));
                //改为由客户自己将快捷方式放入开机启动文件夹的方式开机自启

                //自动Connect
                tbnConnect_Click(new object(), new EventArgs());
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                //MessageBox.Show(ex.Message);
            }
        }

        private void Test()
        {
            while (true)
            {
                int windowState = _autoIt.WinGetState("[CLASS:Alarm]");

                //不报警为5 报警为7
                _logger.Info("windowState: " + windowState);
                Thread.Sleep(10);
            }

        }

        private void tbnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                tbnConnect.Text = "Connecting";
                var option = new GemOption();
                option.DeviceId = 0;
                option.Protocol = GemProtocol.HSMS;
                option.HsmsParameters.Mode = HsmsConnectProcedure.PASSIVE;
                option.HsmsParameters.IPAddress = txtIpaddress.Text;
                option.HsmsParameters.PortNo = int.Parse(txtPort.Text);

                _logger.Info("Start:" + JsonConvert.SerializeObject(option, Formatting.Indented));
                _secs = new HsmsHost(_logger, option.HsmsParameters);
                _secs.SmlLogEnabled = false;
                _secs.ReceivedPrimaryMessage += _secs_ReceivedPrimaryMessage; ;
                _secs.ReceivedSecondaryMessage += _secs_ReceivedSecondaryMessage;
                _secs.HsmsStateChanged += _secs_HsmsStateChanged;
                _secs.TracedSmlLog += _secs_TracedSmlLog;
                _secs.ErrorNotification += _secs_ErrorNotification;
                _secs.Connect();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void _secs_TracedSmlLog(object sender, TraceLogEventArgs e)
        {
            _logger.Info($"{e.Direction}  {e.SML}");
        }

        private void _secs_HsmsStateChanged(object sender, HsmsStateChangedEventArgs e)
        {
            if (e.State == HsmsState.SELECTED)
            {
                tbnConnect.Text = "Connected";
                tbnConnect.Enabled = false;
                Thread.Sleep(500);
                var msg = S1F13();
                _secs.Send(msg);
            }
            else
            {
                tbnConnect.Text = "Disconnected";
            }
        }
        private void _secs_ErrorNotification(object sender, SecsErrorNotificationEventArgs e)
        {
            string error = @"TransactionID:" + e.TransactionId + " error :" + e.Message;
            _logger.Error(error);
        }
        private void _secs_ReceivedSecondaryMessage(object sender, SecondarySecsMessageEventArgs e)
        {
        }

        private void _secs_ReceivedPrimaryMessage(object sender, PrimarySecsMessageEventArgs e)
        {
            try
            {
                SecsMessage secondary = new SecsMessage(0, 0, false);
                if (e.Primary.Name == "S1F1")
                {
                    secondary = S1F2();
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S1F3")
                {
                    var lstItem = (List<SecsItem>)e.Primary.Items[0].Value;
                    var lstSvid = new List<uint>();
                    lstItem.ForEach(x => lstSvid.Add(((uint[])x.Value)[0]));

                    secondary = S1F4(lstSvid);
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S1F13")
                {
                    secondary = S1F14();
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S2F41")
                {
                    if (_controlState)
                    {
                        List<SecsItem> commList = (List<SecsItem>)e.Primary.Items[0].Value;
                        var rcmd = commList[0].Value.ToString();
                        _logger.Info("receive remote command:" + rcmd);

                        switch (rcmd)
                        {
                            case "PP-SEL":
                                var PPlist = (List<SecsItem>)commList[1].Value;
                                var ppinfo = (List<SecsItem>)PPlist[0].Value;
                                var ppid = ppinfo[1].Value.ToString();
                                _logger.Info("PPSElete PPID is :" + ppid);
                                try
                                {
                                    if (SelectRecipe(ppid))//事实上SelectRecipe(ppid)只会返回true或者抛出异常
                                    {
                                        secondary = S2F42(true);
                                    }
                                    else
                                    {
                                        secondary = S2F42(false);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    secondary = S2F42(false, exc.Message);
                                }
                                break;
                            case "LOTID-SET":
                                var LOTIDlist = (List<SecsItem>)commList[1].Value;
                                var lotidinfo = (List<SecsItem>)LOTIDlist[0].Value;
                                var lotid = lotidinfo[1].Value.ToString();
                                _logger.Info("LOTID-SET lotid is :" + lotid);
                                try
                                {
                                    if (SetLotId(lotid))//事实上SetLotId(lotid)只会返回true或者抛出异常
                                    {
                                        secondary = S2F42(true);
                                    }
                                    else
                                    {
                                        secondary = S2F42(false);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    secondary = S2F42(false, exc.Message);
                                }
                                break;
                            case "STOP":
                                _logger.Info("send command to stop machine");
                                try
                                {
                                    if (SendRemoteCommand_STOP())//事实上sendRemoteCommand_STOP()只会返回true或者抛出异常
                                    {
                                        secondary = S2F42(true);
                                    }
                                    else
                                    {
                                        secondary = S2F42(false);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    secondary = S2F42(false, exc.Message);
                                }

                                break;
                            case "START":
                                _logger.Info("send command to start machine");
                                try
                                {
                                    if (SendRemoteCommand_START())//事实上sendRemoteCommand_START()只会返回true或者抛出异常
                                    {
                                        var ret = _autoIt.WinExists("Operation Error");
                                        _logger.Info("_autoIt.WinExists('Operation Error') :" + ret.ToString());
                                        if (ret == 1)
                                        {
                                            string errorText = _autoIt.ControlGetText("Operation Error", "", "[ID:1004]");
                                            secondary = S2F42(false, errorText);
                                            Thread.Sleep(5 * 1000);
                                            _autoIt.ControlClick("Operation Error", "", "[ID:1]");
                                        }
                                        else
                                        {
                                            secondary = S2F42(true);
                                        }
                                    }
                                    else
                                    {
                                        secondary = S2F42(false);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    secondary = S2F42(false, exc.Message);
                                }
                                break;
                            case "START-DISABLE":
                                _logger.Info("send command to disable start button");
                                try
                                {
                                    if (SendRemoteCommand_START_DisableOrEnable(false))//事实上sentRemoteCommand_START()只会返回true或者抛出异常
                                    {
                                        secondary = S2F42(true);
                                    }
                                    else
                                    {
                                        secondary = S2F42(false);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    secondary = S2F42(false, exc.Message);
                                }
                                break;
                            case "START-ENABLE":
                                _logger.Info("send command to enable start button");
                                try
                                {
                                    if (SendRemoteCommand_START_DisableOrEnable(true))//事实上sentRemoteCommand_START()只会返回true或者抛出异常
                                    {
                                        secondary = S2F42(true);
                                    }
                                    else
                                    {
                                        secondary = S2F42(false);
                                    }
                                }
                                catch (Exception exc)
                                {
                                    secondary = S2F42(false, exc.Message);
                                }
                                break;

                            default: //刘涛：你那边可以只回复PP-SEL的指令的S2F41，别的指令没必要回复，要回复也是回复0就行
                                _logger.Info("unknow remote command:" + rcmd);
                                secondary = S2F42(true);
                                break;
                        }
                    }
                    else
                    {
                        secondary = S2F42(false, "ControlState is LOCAL");
                    }

                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S7F1")//load inquire
                {
                    secondary = S7F2();
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S7F5")//upLoad to EAP return S7f6
                {
                    var recipeName = e.Primary.Items[0].Value.ToString();
                    secondary = S7F6(recipeName, RecipeGet(recipeName), OrderGet(recipeName));
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S7F3")//download to Local eqp
                {
                    try
                    {
                        var pplist = (List<SecsItem>)e.Primary.Items[0].Value;

                        var recipeName = pplist[0].Value.ToString();
                        byte[] rcpBodyAndOdBody = (byte[])pplist[1].Value;

                        byte[] rcpBody = new byte[1024];
                        byte[] odBody = new byte[1024];
                        Array.Copy(rcpBodyAndOdBody, 0, rcpBody, 0, 1024);
                        Array.Copy(rcpBodyAndOdBody, 1024, odBody, 0, 1024);

                        RecipeAdd(recipeName, rcpBody, odBody);
                        secondary = S7F4(0);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        secondary = S7F4(1);
                    }
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S7F19")//pplist
                {
                    var recipes = RecipeList();

                    secondary = new SecsMessage(7, 20, false);
                    SecsItemList itemlist = new SecsItemList("PPIDLIST");
                    foreach (var ppid in recipes)
                    {
                        SecsItemAscii secsItem = new SecsItemAscii("PPID");
                        secsItem.Value = ppid.ToString();
                        itemlist.AddItem(secsItem);
                    }
                    secondary.Items.Add(itemlist);
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                }
                else if (e.Primary.Name == "S10F3") //termainal message
                {
                    var messageList = (List<SecsItem>)e.Primary.Items[0].Value;
                    var display = messageList[1].Value.ToString();

                    secondary = S10F4();
                    secondary.TransactionId = e.Primary.TransactionId;
                    _secs.Send(secondary);
                    //MessageBox会阻塞线程，导致不点击确定按钮则无法执行后续代码，这里改为多线程
                    //MessageBox.Show(display, "Terminal Message", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    new Thread(() =>
                    {
                        MessageBox.Show(display, "Terminal Message", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }).Start();
                }


                else
                {
                    _logger.Warn("unhandled secs message:" + e.Primary.Name);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }



        private Dictionary<string, string> GetValueFromText(string fileFullName)
        {
            try
            {
                //_logger.Info("dicerUI.ini fileFullName:" + fileFullName);
                var retValue = new Dictionary<string, string>();
                using (FileStream aFile = new FileStream(fileFullName, FileMode.Open))
                {
                    StreamReader sr = new StreamReader(aFile);
                    string strLine = sr.ReadLine();

                    while (!string.IsNullOrEmpty(strLine))
                    {

                        strLine = sr.ReadLine();
                        if (!string.IsNullOrEmpty(strLine))
                        {
                            if (strLine.Split('=').Length > 1)
                            {
                                retValue[strLine.Split('=')[0]] = strLine.Split('=')[1];
                            }
                        }
                    }
                    sr.Close();
                };
                return retValue;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }



        private bool _bEngClosed = false;

        private void btnClose_Click(object sender, EventArgs e)
        {
            SendRemoteCommand_START_DisableOrEnable(true);
            _bEngClosed = true;
            this.Close();
        }

        private void SECSEngineRun_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_bEngClosed == false)
            {
                e.Cancel = true;
            }
        }

        private void SECSEngineRun_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                _secs.Disconnect();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }




        private void btnGetSelectText_Click_1(object sender, EventArgs e)
        {
            var upColor = GetColor(new Point(837, 706));
            var upColorHex = HexConverter(upColor);
            _logger.Info("upcolor:" + upColor + " " + HexConverter(upColor));

            var downColor = GetColor(new Point(856, 715));
            var downColorHex = HexConverter(downColor);
            _logger.Info("downColor:" + downColor + " " + HexConverter(downColor));
        }


        private bool SendRemoteCommand_STOP()
        {
            _logger.Info($"try to send RemoteCommand_STOP");
            var title = "PG300 Window";

            var sw = FindPg300Window();
            sw.Title = title;

            var isVisibleForStopOperation = _autoIt.ControlCommand(title, "", "[ID:1806]", "IsVisible", "");
            var isEnabledForStopOperation = _autoIt.ControlCommand(title, "", "[ID:1806]", "IsEnabled", "");

            _logger.Info("isVisibleForStopOperation:" + isVisibleForStopOperation);
            _logger.Info("isEnabledForStopOperation:" + isEnabledForStopOperation);
            if (isVisibleForStopOperation == "0" || isEnabledForStopOperation == "0") throw new Exception("pg300 application must be ready for STOP, make sure STOP Operation button is Visible and Enabled.");

            var ret = _autoIt.ControlClick(title, "", "[ID:1806]");
            //if (ret != 1) throw new Exception("failed to click STOP operation, make sure 'Stop' button is enable");
            if (ret != 1) throw new Exception("failed to click STOP operation");

            return true;
        }

        private bool SendRemoteCommand_START()
        {
            _logger.Info($"try to send RemoteCommand_START");

            var title = "PG300 Window";

            var sw = FindPg300Window();
            sw.Title = title;

            var isVisibleForStartOperation = _autoIt.ControlCommand(title, "", "[ID:1801]", "IsVisible", "");
            var isEnabledForStartOperation = _autoIt.ControlCommand(title, "", "[ID:1801]", "IsEnabled", "");

            _logger.Info("isVisibleForStartOperation:" + isVisibleForStartOperation);
            _logger.Info("isEnabledForStartOperation:" + isEnabledForStartOperation);
            if (isVisibleForStartOperation == "0" || isEnabledForStartOperation == "0") throw new Exception("pg300 application must be ready for START, make sure START Operation button is Visible and Enabled.");

            var ret = _autoIt.ControlClick(title, "", "[ID:1801]");
            if (ret != 1) throw new Exception("failed to click START operation");

            return true;
        }
        private bool SendRemoteCommand_START_DisableOrEnable(bool value)
        {
            _logger.Info($"try to send RemoteCommand_START_DisableOrEnable");

            var title = "PG300 Window";

            var sw = FindPg300Window();
            sw.Title = title;

            var isVisibleForStartOperation = _autoIt.ControlCommand(title, "", "[ID:1801]", "IsVisible", "");
            var isEnabledForStartOperation = _autoIt.ControlCommand(title, "", "[ID:1801]", "IsEnabled", "");
            _logger.Info("isVisibleForStartOperation:" + isVisibleForStartOperation);
            _logger.Info("isEnabledForStartOperation:" + isEnabledForStartOperation);


            //！！！测试是否需要在特定页面才可以使用该命令
            //if (isVisibleForStartOperation == "0" || isEnabledForStartOperation == "0") throw new Exception("pg300 application must be ready for START, make sure START Operation button is Visible and Enabled.");
            //var ret = _autoIt.ControlClick(title, "", "[ID:1801]");
            var ret = 0;
            if (value)
            {
                ret = _autoIt.ControlEnable(title, "", "[ID:1801]");
                //_autoIt.ControlEnable("[CLASS:MainWin]", "", "[ID:1801]");//确保成功被禁用
            }
            if (!value)
            {
                ret = _autoIt.ControlDisable(title, "", "[ID:1801]");
                //_autoIt.ControlDisable("[CLASS:MainWin]", "", "[ID:1801]");//确保成功被禁用
            }

            isVisibleForStartOperation = _autoIt.ControlCommand(title, "", "[ID:1801]", "IsVisible", "");
            isEnabledForStartOperation = _autoIt.ControlCommand(title, "", "[ID:1801]", "IsEnabled", "");
            _logger.Info("isVisibleForStartOperation:" + isVisibleForStartOperation);
            _logger.Info("isEnabledForStartOperation:" + isEnabledForStartOperation);


            if (ret != 1) throw new Exception("failed to DisableOrEnable START Button");

            return true;
        }



        #region test code
        /*
        /// <summary>
        /// recipe upload test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = lsboxRecipe.SelectedItem.ToString();
                string[] sourcePath = { Constants.RecipeOrderSetFolder, Constants.RecipeFolder };
                string[] targetPath = { Constants.RecipeOrderSetFolderOnServer, Constants.RecipeFolderOnServer };
                for (int i = 0; i < sourcePath.Length; i++)
                {
                    string sourceFile = Path.Combine(sourcePath[i], fileName);
                    string destFile = Path.Combine(targetPath[i], fileName);
                    Directory.CreateDirectory(targetPath[i]);
                    File.Copy(sourceFile, destFile, true);
                }

                MessageBox.Show("Recipe : "+fileName+" 上传成功");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }

        }

        private void btnRecipeDownload_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                //string[] sourcePath = { Constants.RecipeOrderSetFolder, Constants.RecipeFolder };
                //string[] targetPath = { Constants.RecipeOrderSetFolderOnServer, Constants.RecipeFolderOnServer };
                string[] sourcePath = { Constants.RecipeOrderSetFolderOnServer, Constants.RecipeFolderOnServer };
                string[] targetPath = { Constants.RecipeOrderSetFolder, Constants.RecipeFolder };
                for (int i = 0; i < sourcePath.Length; i++)
                {
                    string sourceFile = Path.Combine(sourcePath[i], fileName);
                    string destFile = Path.Combine(targetPath[i], fileName);
                    Directory.CreateDirectory(targetPath[i]);
                    File.Copy(sourceFile, destFile, true);
                }

                MessageBox.Show("Recipe : " + fileName + " 下载成功");
            }
            //catch (FileNotFoundException fex)
            //{
            //    _logger.Error(fex.Message);
            //    MessageBox.Show("未找到相关文件/r/nRecipeName : " + "");
            //}
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }
        */
        #endregion

        private void SwitchControlState(object sender, EventArgs e)
        {
            _controlState = !_controlState;
            label1.Text = _controlState ? "Remote" : "Local";
            if (!_controlState) SendRemoteCommand_START_DisableOrEnable(true);
            _secs.Send(S6F11(new uint[] { 207 }, new uint[] { 1 }, new List<string>(new string[] { _controlState ? "1" : "0" })));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //SetLotId("test lotid 333");

            //new Thread(WatchWarmingUpModel).Start();
        }


        //有问题：机台按钮有时会出现 看起来是disable ControlCommand查询到也是disable，但实际上可以点击，，手动切换机台模式时这种情况经常发生
        //private void StartBtnState_Handler()
        //{
        //    _logger.Info("StartBtnState_Handler start");

        //    var title = "PG300 Window";
        //    var sw = FindPg300Window();
        //    sw.Title = title;

        //    while (true)
        //    {
        //        if (_StartBtnState)
        //        {
        //            if (!_autoIt.ControlCommand(title, "", "[ID:1801]", "IsEnabled", "").Equals("1"))
        //            {
        //                _autoIt.ControlEnable(title, "", "[ID:1801]");
        //            }
        //        }
        //        else
        //        {
        //            if (_autoIt.ControlCommand(title, "", "[ID:1801]", "IsEnabled", "").Equals("1"))
        //            {
        //                _autoIt.ControlDisable(title, "", "[ID:1801]");
        //            }
        //        }

        //        Thread.Sleep(50);
        //    }
        //}

        private void WatchWarmingUpModelEvent()
        {
            _logger.Info("start WatchWarmingUpModelEvent");
            try
            {
                bool preModel = false;//true==WARMINGUP
                bool currModel;

                //检查、更改屏幕色深
                foreach (var screen in Screen.AllScreens)
                {
                    if (screen.BitsPerPixel != 32 && screen.BitsPerPixel != 16)
                    {
                        _logger.Info("screen.BitsPerPixel:" + screen.BitsPerPixel);
                        //_dm.SetScreen(_dm.GetScreenWidth(), _dm.GetScreenHeight(),16);
                        ChangeBitsPerPel(16);
                        Thread.Sleep(3000);
                    }
                }

                _dmOfProcessstate = new CustomDmsoftClass(_autoIt.WinGetHandle("[CLASS:MainWin]"));
                while (true)
                {
                    string ocrRet = _dmOfProcessstate.Ocr(400, 15, 640, 40, "ffffff-000000", 1.0);

                    if (ocrRet == "WARMING")
                    {
                        currModel = true;
                    }
                    else
                    {
                        currModel = false;
                    }

                    if (currModel != preModel && currModel)
                    {
                        _secs.Send(S6F11(new uint[] { 2001 }, new uint[] { 1 }, new List<string>(new string[] { "WARMING UP Model START" })));
                    }
                    if (currModel != preModel && !currModel)
                    {
                        _secs.Send(S6F11(new uint[] { 2001 }, new uint[] { 1 }, new List<string>(new string[] { "WARMING UP Model END" })));
                    }
                    #region testcode
                    //if (_dm.Ocr(404, 181, 646, 208, "ffffff-000000", 1.0) == "WARMING")
                    //{
                    //    currmodel = true;
                    //}
                    //else
                    //{
                    //    currmodel = false;
                    //}
                    //if (currmodel != premodel && currmodel)
                    //{
                    //    _logger.Info("WARMING UP Model");
                    //}
                    #endregion

                    preModel = currModel;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
        }

        //private void WatchPortReadyEvent()
        //{
        //    while (true)
        //    {
        //        int ret = _autoIt.WinWaitActive("[CLASS:WaferRecipeEdit]", "",1);
        //        if (ret!=0) 
        //            _secs.Send(S6F11(new uint[] { 2000 }, new uint[] { 10 }, new List<string>(new string[] { "Port Locked" }))); 
        //        Thread.Sleep(1000);
        //    }
        //}
        private void WatchPortReadyEvent()
        {
            try
            {
                _logger.Info("start WatchPortReadyEvent");

                bool prePortLockState = false;
                bool portLockState = false;

                while (true)
                {
                    int ret = _autoIt.WinActive("[CLASS:WaferRecipeEdit]");

                    if (ret != 0)
                    {
                        portLockState = true;
                    }
                    else
                    {
                        portLockState = false;
                    }

                    if (portLockState && !prePortLockState)
                    {
                        _secs.Send(S6F11(new uint[] { 2000 }, new uint[] { 10 }, new List<string>(new string[] { "Port Locked" })));
                    }

                    prePortLockState = portLockState;

                    Thread.Sleep(200);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }


        private void WatchMemoryState()
        {
            try
            {
                while (true)
                {
                    MEMORYSTATUSEX statEX = new MEMORYSTATUSEX();
                    statEX.dwLength = (uint)Marshal.SizeOf(statEX);
                    GlobalMemoryStatusEx(ref statEX);
                    //log4net.LogManager.GetLogger("App").Info($"#TotalPhys: {statEX.ullTotalPhys}");
                    //log4net.LogManager.GetLogger("App").Info($"#AvailPhys: {statEX.ullAvailPhys}");
                    //log4net.LogManager.GetLogger("App").Info($"#CurrentProcessPrivateMemorySize: {Process.GetCurrentProcess().PrivateMemorySize64}");
                    _secs.Send(S6F11(new uint[] { 99999 }, new uint[] { 10 }, new List<string>(new string[] {
                    $"#TotalPhys: {statEX.ullTotalPhys}", $"#AvailPhys: {statEX.ullAvailPhys}",$"#CurrentProcessPrivateMemorySize: {Process.GetCurrentProcess().PrivateMemorySize64}" })));

                    Thread.Sleep(1000 * 30);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }

        private void RemoveLogOfNextDay()
        {
            try
            {
                while (true)
                {
                    //删除去年明日的日志，避免错读日志BUG//解决机台本身存在的logbug
                    string[] NextDayLogFileNames = GetNextDayLogFileName();
                    _logger.Info("remove log : " +
                        JsonConvert.SerializeObject(NextDayLogFileNames, Formatting.Indented) +
                        JsonConvert.SerializeObject(DateTime.Now, Formatting.Indented));

                    for (int i = 0; i < NextDayLogFileNames.Length; i++)
                    {
                        if (File.Exists(Constants.OperationLogFolder + NextDayLogFileNames[i]))
                        {
                            File.Delete(Constants.OperationLogFolder + NextDayLogFileNames[i]);
                        }
                        if (File.Exists(Constants.AlarmLogFolder + NextDayLogFileNames[i]))
                        {
                            File.Delete(Constants.AlarmLogFolder + NextDayLogFileNames[i]);
                        }
                        if (File.Exists(Constants.LotLogFolder + NextDayLogFileNames[i]))
                        {
                            File.Delete(Constants.LotLogFolder + NextDayLogFileNames[i]);
                        }
                    }
                    //每3小时执行一次，因为sleep不准，所以不每24小时执行一次。
                    Thread.Sleep(3600000 * 3);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
        private string[] GetNextDayLogFileName()
        {
            try
            {
                DateTime nextDay = DateTime.Now.AddDays(1);

                string hexMonthSuffix = int.Parse(nextDay.ToString("MM")).ToString("X");
                switch (hexMonthSuffix)
                {
                    case "A":
                        hexMonthSuffix = "X";
                        break;
                    case "B":
                        hexMonthSuffix = "Y";
                        break;
                    case "C":
                        hexMonthSuffix = "Z";
                        break;
                    default:
                        break;
                }

                string decimalDaySuffix = nextDay.ToString("dd").ToString();
                string logSuffix = hexMonthSuffix + decimalDaySuffix;

                string[] nextDayLogFileName = {
                @"\OPELOG." + logSuffix,
                @"\ALARMLOG." + logSuffix,
                @"\LOTLOG." + logSuffix,
            };

                _logger.Info($"nextDayLogFileName: {JsonConvert.SerializeObject(nextDayLogFileName)}");

                return nextDayLogFileName;
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }



        private void GetLogOfTaskList()
        {
            while (true)
            {
                Process process = new Process();

                process.StartInfo.FileName = "cmd";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                process.StandardInput.WriteLine("tasklist" + "&exit");
                process.StandardInput.AutoFlush = true;

                string output = process.StandardOutput.ReadToEnd();
                process.WaitForExit();
                process.Close();
                string logFileName = @"\log_tasklist.txt_" + DateTime.Now.ToString("yyyyMMdd");
                File.AppendAllText(Constants.AppFolder + logFileName, DateTime.Now.ToString() + "\r\n");
                File.AppendAllText(Constants.AppFolder + logFileName, output);


                if (File.Exists(@"\log_tasklist.txt_" + DateTime.Now.AddDays(3).ToString("yyyyMMdd")))
                {
                    File.Delete(@"\log_tasklist.txt_" + DateTime.Now.AddDays(3).ToString("yyyyMMdd"));
                }

                Thread.Sleep(60000 * 30);
                //Thread.Sleep(3000);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {


            _logger.Info(_autoIt.ControlDisable("[CLASS:MainWin]", "", "[ID:1801]"));
        }

        private void button5_Click(object sender, EventArgs e)
        {

            _logger.Info(_autoIt.ControlEnable("[CLASS:MainWin]", "", "[ID:1801]"));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _StartBtnState = !_StartBtnState;
        }

        private void button7_Click(object sender, EventArgs e)
        {

            var isVisibleForStartOperation = _autoIt.ControlCommand("PG300 Window", "", "[ID:1801]", "IsVisible", "");
            var isEnabledForStartOperation = _autoIt.ControlCommand("PG300 Window", "", "[ID:1801]", "IsEnabled", "");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            new Thread(Test).Start();
        }
    }
}
