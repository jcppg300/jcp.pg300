﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using AutoItX3Lib;


namespace Jcp.Pg300
{

    public static class AutoItXNet20
    {
        private static log4net.ILog _logger = log4net.LogManager.GetLogger("App");
        private static string MainWindowTitle = "A-WD-300TX - DicerGUI";
        private static string AutoItMainTitle = string.Format(@"[TITLE:{0}]", MainWindowTitle);
        private static string AutoItAlarmTitle = string.Format(@"[TITLE:{0}]", "CSubInfoAlarmPanel");

        private static AutoItX3Class _autoItX = new AutoItX3Class();

        public static string GetText(string Control)
        {
            try
            {
                AutoItX3Class autoItX = new AutoItX3Class();
                //var parentID = autoItX.WinGetHandle(MainWindowTitle);
                //_logger.Info("parenthandlerID:" + parentID);
                //var childId = autoItX.ControlGetHandle(parentID, Control);
                //_logger.Info("childId:" + childId);
                var controltText = autoItX.ControlGetText(AutoItMainTitle, "", Control);
                _logger.Info(Control + ":GetcontroltText:" + controltText);
                return controltText;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }


        public static string GetTextEx(string Control)
        {
            try
            {
                var controltText = _autoItX.ControlGetText(AutoItMainTitle, "", Control);
                _logger.Info(Control + ":GetcontroltText:" + controltText);
                return controltText;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }

        public static int SendButtonClick(string text,string contorl)
        {
            try
            {
                AutoItX3Class autoItX = new AutoItX3Class();
                autoItX.WinActivate(AutoItMainTitle);
                autoItX.ControlFocus(AutoItMainTitle, text, contorl);
                var reslt= autoItX.ControlClick(AutoItMainTitle, text, contorl);
                _logger.Info("contorl:"+ contorl+" Click Result:"+ reslt);
                return reslt;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return 0;
            }
        }

        public static int SendAlarmButtonClick(string text, string contorl)
        {
            try
            {
                AutoItX3Class autoItX = new AutoItX3Class();
                autoItX.WinActivate(AutoItAlarmTitle);
                autoItX.ControlFocus(AutoItAlarmTitle, text, contorl);
                var reslt = autoItX.ControlClick(AutoItAlarmTitle, text, contorl);
                _logger.Info("contorl:" + contorl + " Click Result:" + reslt);
                return reslt;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return 0;
            }


        }

        public static void LeftClickByMouse(int x, int y)
        {
            AutoItX3Class autoItX = new AutoItX3Class();
            autoItX.MouseClick("left", x, y, 1);
        }

        public static void SendListViewComman(string recipeName, string contorl)
        {
            try
            {
                _logger.Info("Control :" + contorl);
                AutoItX3Class autoItX = new AutoItX3Class();
                autoItX.WinActivate(AutoItMainTitle);
                autoItX.ControlFocus(AutoItMainTitle, "", contorl);
                //var allcounts = autoItX.ControlListView(AutoItMainTitle, "", contorl, "GetItemCount", "", "");
                //autoItX.ControlListView(AutoItMainTitle, "", contorl, "Select", listIndex, "");//选中也没有用 还是得模拟点击
                autoItX.ControlFocus(AutoItMainTitle, "", "5467");//对焦搜索框
                autoItX.ControlSetText(AutoItMainTitle, "", "5467", recipeName);//把recipe 填入搜索框
                Thread.Sleep(500);
                SendButtonClick("Search","5471");//search
                var listIndex = autoItX.ControlListView(AutoItMainTitle, "", contorl, "FindItem", recipeName, "1");
                _logger.Info("listIndex:" + listIndex);
                if (listIndex == "0")
                {
                    autoItX.MouseClick("left", 559, 220, 1);
                    SendButtonClick("激活","5464");//激活
                    Thread.Sleep(500);
                    SendButtonClick("","4687");//确认
                }
                else
                {
                    throw new Exception("current List cannot find recipe Name:" + recipeName);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        //public static void SendClickSearchRecipe(string recipeName, string contorl)
        //{
        //    AutoItX3Class autoItX = new AutoItX3Class();
        //    autoItX.WinActivate(AutoItMainTitle);
        //    autoItX.ControlFocus(AutoItListTitle, "", contorl);
        //    Thread.Sleep(300);
        //    autoItX.ControlSetText(AutoItListTitle, "", contorl, recipeName);
        //    SendButtonClick("5471");

        //    autoItX.MouseClick("left", 469, 227, 1);

        //}
        public static void SendCommand()
        {
            AutoItX3Class autoItX = new AutoItX3Class();
            autoItX.WinActivate("[TITLE:句柄测试]");
            autoItX.ControlClick("[TITLE:句柄测试]","", "1512340");

        }
        public static string GetAlarmText(string Control)
        {
            try
            {
                AutoItX3Class autoItX = new AutoItX3Class();
                var controltText = autoItX.ControlGetText(AutoItAlarmTitle, "", Control);
                _logger.Info(Control + ":GetAlarmcontroltText:" + controltText);
                return controltText;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }
        public static void WaitAlarmDialogOpen()
        {
            AutoItX3Class autoItX = new AutoItX3Class();
            autoItX.WinWaitActive(AutoItAlarmTitle);
            //var txt = AutoItX.WinGetText(AlarmTitle);
           // WaitAlarmDialogClosed(AutoItAlarmTitle);
        }

        public static void WaitAlarmDialogClosed(string AlarmTitle)
        {
            //AutoItX.WinWaitClose(AlarmTitle);
        }

        public static void SelectList2(string AlarmTitle)
        {
            try
            {
                var ret1 = _autoItX.ControlCommand("", "SHOW", "[ID:30300]", "FindString", "CSP8-595U-195-TI3543-00");
                _logger.Info("ret1:" + ret1);

                var ret2 = _autoItX.ControlCommand("", "SHOW", "[ID:30300]", "SetCurrentSelection", "3");
                _logger.Info("ret2:" + ret2);

                //var ret = _autoItX.ControlCommand("", "SHOW", "[ID:30300]", "SelectString", "CSP8-595U-195-TI3543-00");

                //var ret1 = _autoItX.ControlSetText("", "SHOW", "[CLASS:Static; INSTANCE:1]", "CSP8-595U-195-TI3543-00");
                //_logger.Info("ret1:" + ret1);
                //var ret2 = _autoItX.ControlSetText("", "SHOW", "[ID:30201]", "CSP8-595U-195-TI3543-00");
                //_logger.Info("ret2:" + ret2);
                //var ret3 = _autoItX.ControlSetText("", "SHOW", "[ID:30203]", "print_all.scp");
                //_logger.Info("ret3:" + ret3);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }

        }


        public static void SelectList1(string AlarmTitle)
        {
            try
            {


                var ret = _autoItX.ControlCommand("List Box Find String", "", "[ID:3]", "SelectString", "083 : Random string");
                _logger.Info("ret:" + ret);
                //var ret1 = _autoItX.ControlSetText("", "SHOW", "[CLASS:Static; INSTANCE:1]", "CSP8-595U-195-TI3543-00");
                //_logger.Info("ret1:" + ret1);
                //var ret2 = _autoItX.ControlSetText("", "SHOW", "[ID:30201]", "CSP8-595U-195-TI3543-00");
                //_logger.Info("ret2:" + ret2);
                //var ret3 = _autoItX.ControlSetText("", "SHOW", "[ID:30203]", "print_all.scp");
                //_logger.Info("ret3:" + ret3);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

        }

        //public static void SelectList1(string AlarmTitle)
        //{
        //    try
        //    {


        //        //var ret = _autoItX.ControlCommand("", "SHOW", "[ID:30300]", "SelectString", "CSP8-595U-195-TI3543-00");

        //        var ret1 = _autoItX.ControlSetText("", "SHOW", "[CLASS:Static; INSTANCE:1]", "CSP8-595U-195-TI3543-00");
        //        _logger.Info("ret1:" + ret1);
        //        var ret2 = _autoItX.ControlSetText("", "SHOW", "[ID:30201]", "CSP8-595U-195-TI3543-00");
        //        _logger.Info("ret2:" + ret2);
        //        var ret3 = _autoItX.ControlSetText("", "SHOW", "[ID:30203]", "print_all.scp");
        //        _logger.Info("ret3:" + ret3);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }

        //}
    }
}
