﻿

using ManagedWinapi.Windows;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun
    {

        #region 更改显示位深
        // 映射 DEVMODE 结构
        // 参照 DEVMODE结构的指针定义：
        // http://msdn.microsoft.com/en-us/library/windows/desktop/dd183565(v=vs.85).aspx
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct DEVMODE
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string dmDeviceName;

            public short dmSpecVersion;
            public short dmDriverVersion;
            public short dmSize;
            public short dmDriverExtra;
            public int dmFields;
            public int dmPositionX;
            public int dmPositionY;
            public int dmDisplayOrientation;
            public int dmDisplayFixedOutput;
            public short dmColor;
            public short dmDuplex;
            public short dmYResolution;
            public short dmTTOption;
            public short dmCollate;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string dmFormName;

            public short dmLogPixels;
            public short dmBitsPerPel;
            public int dmPelsWidth;
            public int dmPelsHeight;
            public int dmDisplayFlags;
            public int dmDisplayFrequency;
            public int dmICMMethod;
            public int dmICMIntent;
            public int dmMediaType;
            public int dmDitherType;
            public int dmReserved1;
            public int dmReserved2;
            public int dmPanningWidth;
            public int dmPanningHeight;
        };

        // Win32 函数在托管环境下的声明
        public class NativeMethods
        {
            // 控制改变屏幕分辨率的常量
            public const int ENUM_CURRENT_SETTINGS = -1;
            public const int CDS_UPDATEREGISTRY = 0x01;
            public const int CDS_TEST = 0x02;
            public const int DISP_CHANGE_SUCCESSFUL = 0;
            public const int DISP_CHANGE_RESTART = 1;
            public const int DISP_CHANGE_FAILED = -1;

            // 控制改变方向的常量定义
            public const int DMDO_DEFAULT = 0;
            public const int DMDO_90 = 1;
            public const int DMDO_180 = 2;
            public const int DMDO_270 = 3;

            [DllImport("user32.dll")]
            public static extern int EnumDisplaySettings(string deviceName, int modeNum, ref DEVMODE devMode);
            [DllImport("user32.dll")]
            public static extern int ChangeDisplaySettings(ref DEVMODE devMode, int flags);
        }

        public void ChangeBitsPerPel(short dmBitsPerPel)
        {
            DEVMODE devmode = new DEVMODE();
            devmode.dmDeviceName = new String(new char[32]);
            devmode.dmFormName = new String(new char[32]);
            devmode.dmSize = (short)Marshal.SizeOf(devmode);

            if (0 != NativeMethods.EnumDisplaySettings(null, NativeMethods.ENUM_CURRENT_SETTINGS, ref devmode))
            {
                //devmode.dmPelsWidth = 1600;
                //devmode.dmPelsHeight = 900;
                devmode.dmBitsPerPel = dmBitsPerPel;

                int iRet = NativeMethods.ChangeDisplaySettings(ref devmode, NativeMethods.CDS_TEST);

                if (iRet == NativeMethods.DISP_CHANGE_FAILED)
                {
                    MessageBox.Show("不能执行你的请求", "信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    iRet = NativeMethods.ChangeDisplaySettings(ref devmode, NativeMethods.CDS_UPDATEREGISTRY);

                    switch (iRet)
                    {
                        case NativeMethods.DISP_CHANGE_SUCCESSFUL:
                            {
                                break;
                            }
                        case NativeMethods.DISP_CHANGE_RESTART:
                            {
                                MessageBox.Show("你需要重新启动电脑设置才能生效", "信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                        default:
                            {
                                MessageBox.Show("改变屏幕分辨率失败", "信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                    }
                }
            }
        }
        #endregion

        //public const int GWL_EXSTYLE = -20;
        //public const UInt32 WS_EX_TOPMOST = 0x0008;
        //public const int SWP_NOMOVE = 2;
        //public const int SWP_NOSIZE = 1;
        //public const int HWND_TOPMOST = -1;
        //public const int HWND_NOTOPMOST = -2;
        //[DllImport("User32.dll", SetLastError = false)]
        //static extern bool SetWindowPos(IntPtr hwnd, IntPtr hwndinsertafter, int x, int y, int cx, int cy, UInt32 flags);



        private void btnShowTopMostWindow_Click(object sender, EventArgs e)
        {
            var test = "TI8-SN74LVC2G08YZPRR-V8A-00";

            var extension = Path.GetExtension(test);
            var recipeId = test.Replace(extension, "");


            Thread t = new Thread(new ThreadStart(() =>
            {
                try
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        var form = new TopMostForm();
                        form.ShowTopMost("FRAMEID_NG", "Wafer ID does not match");
                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }));
            t.Start();

            //var form = new TopMostForm();
            //form.ShowTopMost("hello world");
        }




        private void btnComboSelect_Click(object sender, EventArgs e)
        {
            AutoItXNet20.SelectList1("ad");
        }

        private void btnFindStatePic_Click(object sender, EventArgs e)
        {
            try
            {
                var handle = _autoIt.ControlGetHandle("Calculator", "", "[CLASS:#32770; INSTANCE:1]");
                MessageBox.Show("control handle:" + handle);

                var sw = SystemWindow.FromPoint(295, 239);
                MessageBox.Show("control handle2:" + sw.HWnd.ToString());

                var i = int.Parse(handle, System.Globalization.NumberStyles.HexNumber);
                IntPtr p = new IntPtr(i);
                var sw2 = new SystemWindow(p);
                sw2.Highlight();
                sw2.VisibilityFlag = false;

                var ret = _dm.FindStrFast(0, 0, 700, 700, "IDLE", "000080-000000", 0.9, out object intX, out object intY);
                if (Convert.ToInt32(intX) >= 0 && Convert.ToInt32(intY) >= 0)
                {
                    ret = _dm.MoveTo(Convert.ToInt32(intX), Convert.ToInt32(intY));
                    MessageBox.Show("move to ret:" + ret);
                }

                else
                    MessageBox.Show("could not find IDLE:" + ret);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception:" + JsonConvert.SerializeObject(ex));
                _logger.Info(JsonConvert.SerializeObject(ex));
                throw;
            }
        }



        private void GetValueByORC(object sender, EventArgs e)
        {

            string name = valueName.Text;

            try
            {
                #region testcode
                ////MessageBox.Show("_dm.Capture(0, 0, 2000, 2000, 'screen.bmp'):" + _dm.Capture(0, 0, 2000, 2000, "screen.bmp"));
                ////MessageBox.Show("_dm.OcrInFile(0, 0, 2000, 2000, 'screen.bmp', '000000 - 000000', 1.0):" + _dm.OcrInFile(0, 0, 2000, 2000, "screen.bmp", "000000-000000", 1.0)); 
                ////MessageBox.Show("_dm.GetColor(100, 100):" + _dm.GetColor(100, 100));
                ////MessageBox.Show("_dm.GetColorBGR(200, 200):" + _dm.GetColorBGR(200, 200));
                ////MessageBox.Show("_dm.MoveTo(100, 100):" + _dm.MoveTo(100, 100));
                ////MessageBox.Show("_dm.Ocr(0, 0, 200, 200, '', 0.8):" + _dm.Ocr(0, 0, 200, 200, "", 0.8));

                ////_dm.EnableDisplayDebug(1);
                ////_dm.CapturePre("errorPci");

                ////title
                //MessageBox.Show("title:" + _dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0));
                ////value 
                //MessageBox.Show("value1:" + _dm.Ocr(835, 206, 866, 220, "000000-000000", 1.0));
                //MessageBox.Show("value2:" + _dm.Ocr(835, 256, 866, 270, "000000-000000", 1.0));
                //MessageBox.Show("value6:" + _dm.Ocr(835, 456, 866, 470, "000000-000000", 1.0));
                #endregion
                GetValue(name);


            }
            catch (Exception ex)
            {
                _logger.Info(JsonConvert.SerializeObject(ex));
                MessageBox.Show("Exception: " + JsonConvert.SerializeObject(ex));
            }

        }


        string GetValue(string valueName)
        {

            try
            {

                //检查、更改屏幕色深
                foreach (var screen in Screen.AllScreens)
                {
                    if (screen.BitsPerPixel != 32 && screen.BitsPerPixel != 16)
                    {
                        _logger.Info("screen.BitsPerPixel:" + screen.BitsPerPixel);
                        //_dm.SetScreen(_dm.GetScreenWidth(), _dm.GetScreenHeight(),16);
                        ChangeBitsPerPel(16);
                        Thread.Sleep(3000);
                    }
                }

                //使机台程序位于最上层,避免挡住目标值。
                //var handleStr = _autoIt.ControlGetHandle("[CLASS:MainScn]", "", "");
                //_logger.Info(handleStr);
                //var handle = int.Parse(handleStr, System.Globalization.NumberStyles.HexNumber);
                //SetWindowPos((IntPtr)handle, (IntPtr)HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

                //使机台程序位于最上层,避免挡住目标值。
                //_autoIt.WinActivate("[CLASS:MainScn]", "");
                //_dm.MoveTo(300, 100);
                //_dm.LeftClick();
                //Thread.Sleep(100);



                string handle;
                switch (valueName)
                {
                    case "GR-1 BLADE HEIGHT":
                    case "GR-2 BLADE HEIGHT":
                        handle = _autoIt.WinGetHandle("[CLASS:MONCNT]");
                        if (handle == "")
                        {
                            OpenWindow("COUNTER");
                            Thread.Sleep(50);
                            DmOfCOUNTERHwnd = _autoIt.WinGetHandle("[CLASS:MONCNT]");
                        }
                        else if (handle != DmOfCOUNTERHwnd)
                        {
                            DmOfCOUNTERHwnd = handle;
                        }

                        break;
                    case "AIR SOURCE PRESSURE":
                    case "VACUUM SOURCE PRESSURE":
                        handle = _autoIt.WinGetHandle("[CLASS:MONUTIL]");
                        _logger.Info(handle);
                        if (handle == "")
                        {
                            OpenWindow("UTILITY");
                            Thread.Sleep(50);
                            DmOfUTILITYHwnd = _autoIt.WinGetHandle("[CLASS:MONUTIL]");
                        }
                        else if (handle != DmOfUTILITYHwnd)
                        {
                            DmOfUTILITYHwnd = handle;
                        }

                        break;
                    case "GRINDING-1 St CHUCK VACUUM":
                    case "GRINDING-2 St CHUCK VACUUM":
                        handle = _autoIt.WinGetHandle("[CLASS:MONVAC]");
                        if (handle == "")
                        {
                            OpenWindow("VACUUM");
                            Thread.Sleep(50);
                            DmOfVACUUMHwnd = _autoIt.WinGetHandle("[CLASS:MONVAC]");
                        }
                        else if (handle != DmOfVACUUMHwnd)
                        {
                            DmOfVACUUMHwnd = handle;
                        }

                        break;
                    case "GRINDING-1 St WHEEL REVOLVING":
                    case "GRINDING-2 St WHEEL REVOLVING":
                    case "GRINDING-1 St COOLANT FLOWRATE":
                    case "GRINDING-2 St COOLANT FLOWRATE":
                        handle = _autoIt.WinGetHandle("[CLASS:MONGR]");
                        if (handle == "")
                        {
                            OpenWindow("GRINDING");
                            Thread.Sleep(50);
                            DmOfGRINDINGHwnd = _autoIt.WinGetHandle("[CLASS:MONGR]");
                        }
                        else if (handle != DmOfGRINDINGHwnd)
                        {
                            DmOfGRINDINGHwnd = handle;
                        }

                        break;

                    default: break;
                }




                #region testcode


                /*

                            //_autoIt.WinWait("Notepad");
                            _logger.Info("###");
                            var handleStr = _autoIt.ControlGetHandle("[CLASS:MainScn]", "", "");
                            //var handleStr = _autoIt.ControlGetHandle("[CLASS:Edit]", "", "");

                            _logger.Info(handleStr);
                            var handle = int.Parse(handleStr, System.Globalization.NumberStyles.HexNumber);
                            SetWindowPos((IntPtr)handle, (IntPtr)HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);





                            //var handleStr11 = _autoIt.ControlGetHandle("EAP for PG300", "", "");
                            //_logger.Info("###11: " + handleStr11);
                            //var handleStr = _autoIt.WinGetHandle("EAP for PG300");
                            //_autoIt.Run("notepad.exe");
                            //var handleStr1 = _autoIt.WinGetHandle(("[CLASS:Notepad]"));
                            //_logger.Info("###1: " + handleStr1);
                            //string v = _dm.EnumWindowByProcess("", "EAP for PG300", "", 1 + 16);
                            //_logger.Info("###: " + v);

                            int v = _autoIt.WinExists("EAP for PG300");
                            _logger.Info("###WinExists : " + v.ToString());

                            //string errorText = _autoIt.ControlGetText("Operation Error", "", "[ID:1004]");
                            //_logger.Info("###errorText : " + errorText);

                            //var handleStr11 = _autoIt.ControlGetHandle("EAP for PG300", "", "");
                            //_logger.Info("###ControlGetHandle : " + handleStr11);
                            //var handleStr = _autoIt.WinGetHandle("EAP for PG300");
                            //_logger.Info("###WinGetHandle : " + handleStr);



                */
                #endregion
                #region oldcode
                //    switch (valueName)
                //    {
                //        case "GR-1 BLADE HEIGHT":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "COUNTER")
                //            {
                //                //打开COUNTER窗口
                //                OpenWindow("COUNTER");
                //            }
                //            value = _dm.Ocr(760, 442, 866, 460, "000000-000000", 1.0);
                //            break;
                //        case "GR-2 BLADE HEIGHT":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "COUNTER")
                //            {
                //                //打开COUNTER窗口
                //                OpenWindow("COUNTER");
                //            }
                //            value = _dm.Ocr(760, 476, 866, 494, "000000-000000", 1.0);
                //            break;
                //        case "AIR SOURCE PRESSURE":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "UTILITY")
                //            {
                //                //打开UTILITY窗口
                //                OpenWindow("UTILITY");
                //            }
                //            value = _dm.Ocr(835, 206, 866, 220, "000000-000000", 1.0);
                //            break;
                //        case "VACUUM SOURCE PRESSURE":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "UTILITY")
                //            //打开UTILITY窗口
                //            {
                //                OpenWindow("UTILITY");
                //            }
                //            value = _dm.Ocr(835, 256, 866, 270, "000000-000000", 1.0);
                //            break;
                //        case "GRINDING-1 St CHUCK VACUUM":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "VACUUM")
                //            {
                //                //打开VACUUM窗口
                //                OpenWindow("VACUUM");
                //            }
                //            value = _dm.Ocr(835, 206, 866, 220, "000000-000000", 1.0);
                //            break;
                //        case "GRINDING-2 St CHUCK VACUUM":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "VACUUM")
                //            {
                //                //打开VACUUM窗口
                //                OpenWindow("VACUUM");
                //            }
                //            value = _dm.Ocr(835, 256, 866, 270, "000000-000000", 1.0);
                //            break;
                //        case "GRINDING-1 St WHEEL REVOLVING":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "GRINDIN")
                //            {
                //                //打开GRINDING窗口
                //                OpenWindow("GRINDING");
                //            }
                //            value = _dm.Ocr(835, 256, 866, 270, "000000-000000", 1.0);
                //            break;
                //        case "GRINDING-2 St WHEEL REVOLVING":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "GRINDIN")
                //            {
                //                //打开GRINDING窗口
                //                OpenWindow("GRINDING");
                //            }
                //            value = _dm.Ocr(835, 456, 866, 470, "000000-000000", 1.0);
                //            break;
                //        case "GRINDING-1 St COOLANT FLOWRATE":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "GRINDIN")
                //            {
                //                //打开GRINDING窗口
                //                OpenWindow("GRINDING");
                //            }
                //            value = _dm.Ocr(835, 356, 866, 370, "000000-000000", 1.0);
                //            break;
                //        case "GRINDING-2 St COOLANT FLOWRATE":
                //            if (_dm.Ocr(633, 177, 902, 192, "ffffff-000000", 1.0) != "GRINDIN")
                //            {
                //                //打开GRINDING窗口
                //                OpenWindow("GRINDING");
                //            }
                //            value = _dm.Ocr(835, 556, 866, 570, "000000-000000", 1.0);
                //            break;

                //        default:
                //            _logger.Info($"{valueName} no defined");
                //            break;
                //    }

                //    _logger.Info($"{valueName}:  {value}");
                //    return value;
                //}
                #endregion

                string value = "";
                switch (valueName)
                {
                    case "GR-1 BLADE HEIGHT":
                        value = _dmOfCOUNTER.Ocr(760 - 628, 442 - 170, 866 - 628, 460 - 170, "000000-000000", 1.0);
                        break;
                    case "GR-2 BLADE HEIGHT":
                        value = _dmOfCOUNTER.Ocr(760 - 628, 476 - 170, 866 - 628, 494 - 170, "000000-000000", 1.0);
                        break;
                    case "AIR SOURCE PRESSURE":
                        //value = _dmOfUTILITY.Ocr(835, 206, 866, 220, "000000-000000", 1.0);
                        value = _dmOfUTILITY.Ocr(835 - 628, 206 - 170, 866 - 628, 220 - 170, "000000-000000", 1.0);
                        break;
                    case "VACUUM SOURCE PRESSURE":
                        value = _dmOfUTILITY.Ocr(835 - 628, 256 - 170, 866 - 628, 270 - 170, "000000-000000", 1.0);
                        break;
                    case "GRINDING-1 St CHUCK VACUUM":
                        value = _dmOfVACUUM.Ocr(835 - 628, 206 - 170, 866 - 628, 220 - 170, "000000-000000", 1.0);
                        break;
                    case "GRINDING-2 St CHUCK VACUUM":
                        value = _dmOfVACUUM.Ocr(835 - 628, 256 - 170, 866 - 628, 270 - 170, "000000-000000", 1.0);
                        break;
                    case "GRINDING-1 St WHEEL REVOLVING":
                        value = _dmOfGRINDING.Ocr(835 - 628, 256 - 170, 866 - 628, 270 - 170, "000000-000000", 1.0);
                        break;
                    case "GRINDING-2 St WHEEL REVOLVING":
                        value = _dmOfGRINDING.Ocr(835 - 628, 456 - 170, 866 - 628, 470 - 170, "000000-000000", 1.0);
                        break;
                    case "GRINDING-1 St COOLANT FLOWRATE":
                        value = _dmOfGRINDING.Ocr(835 - 628, 356 - 170, 866 - 628, 370 - 170, "000000-000000", 1.0);
                        break;
                    case "GRINDING-2 St COOLANT FLOWRATE":
                        value = _dmOfGRINDING.Ocr(835 - 628, 556 - 170, 866 - 628, 570 - 170, "000000-000000", 1.0);
                        break;

                    default:
                        _logger.Info($"{valueName} no defined");
                        break;
                }

                _logger.Info($"{valueName}:  {value}");
                return value;
            }
            catch (Exception ex)
            {
                _logger.Error($"{ex.Message}");
                return "-99";
            }
        }


        void OpenWindow(string windowName)
        {
            int sleepInterval = 30;
            var title = "PG300 Window";
            var sw = FindPg300Window();//这里没必要设置title，用class即可。
            sw.Title = title;

            #region MyRegion
            //关闭edit recipe界面
            //int ret = _autoIt.ControlClick("[CLASS:WaferRecipeEdit]", "", "Cancel") == 1 ? 1 :
            //_autoIt.ControlClick("[CLASS:DrsRecipeEdit]", "", "Cancel") == 1 ? 1 :
            //_autoIt.ControlClick("[CLASS:PadCondEdit]", "", "Cancel") == 1 ? 1 :
            //_autoIt.ControlClick("[CLASS:SgrRecipeEdit]", "", "Cancel") == 1 ? 1 :
            //_autoIt.ControlClick("[CLASS:OrderEdit]", "", "Cancel") == 1 ? 1 : 0;

            //关闭edit recipe界面
            //if (_autoIt.ControlClick("[CLASS:WaferRecipeEdit]", "", "Cancel") == 0)
            //    if (_autoIt.ControlClick("[CLASS:DrsRecipeEdit]", "", "Cancel") == 0)
            //        if (_autoIt.ControlClick("[CLASS:PadCondEdit]", "", "Cancel") == 0)
            //            if (_autoIt.ControlClick("[CLASS:SgrRecipeEdit]", "", "Cancel") == 0)
            //                if (_autoIt.ControlClick("[CLASS:OrderEdit]", "", "Cancel") == 0) { };
            //关闭edit recipe界面
            //MessageBox.Show("_autoIt.ControlClick('[CLASS: WaferRecipeEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:WaferRecipeEdit]", "", "Cancel"));
            //MessageBox.Show("_autoIt.ControlClick('[CLASS: DrsRecipeEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:DrsRecipeEdit]", "", "Cancel"));
            //MessageBox.Show("_autoIt.ControlClick('[CLASS: PadCondEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:PadCondEdit]", "", "Cancel"));
            //MessageBox.Show("_autoIt.ControlClick('[CLASS: SgrRecipeEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:SgrRecipeEdit]", "", "Cancel"));
            //MessageBox.Show("_autoIt.ControlClick('[CLASS: OrderEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:OrderEdit]", "", "Cancel"));

            //关闭edit recipe界面
            //_logger.Info("_autoIt.ControlClick('[CLASS: WaferRecipeEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:WaferRecipeEdit]", "", "[ID:32500]"));
            //_logger.Info("_autoIt.ControlClick('[CLASS: DrsRecipeEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:DrsRecipeEdit]", "", "[ID:32500]"));
            //_logger.Info("_autoIt.ControlClick('[CLASS: PadCondEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:PadCondEdit]", "", "[ID:32500]"));
            //_logger.Info("_autoIt.ControlClick('[CLASS: SgrRecipeEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:SgrRecipeEdit]", "", "[ID:32500]"));
            //_logger.Info("_autoIt.ControlClick('[CLASS: OrderEdit]', '', 'Cancel');" + _autoIt.ControlClick("[CLASS:OrderEdit]", "", "[ID:32500]"));

            //_logger.Info("_autoIt.ControlClick('', 'Cancel', '[ID: 32500]');" + _autoIt.ControlClick("", "Cancel", "[ID:32500]"));

            //关闭edit recipe界面
            //SystemWindow.FromPoint(35, 106).Title = "EditRecipeSubWindow";
            //Thread.Sleep(1000);
            //_logger.Info("_autoIt.ControlClick('EditRecipeSubWindow', '', '[ID: 32500]');" + _autoIt.ControlClick("EditRecipeSubWindow", "", "[ID:32500]"));
            ////_autoIt.ControlClick("EditRecipeSubWindow", "", "[ID:32500]");
            //Thread.Sleep(sleepInterval);

            #endregion
            //这里可以用autoit做，目前使用的是文字识别//用autoit做需要处理很多按钮，文字识别更简单
            //关闭edit recipe界面//只有老厂有这个界面
            var findStrCancelFastRet = _dm.FindStrFast(0, 0, 2000, 2000, "Cancel", "000000-000000", 1.0, out object intXCancel, out object intYCancel);
            _logger.Info("findStrFastRet Cancel: " + findStrCancelFastRet);
            if (Convert.ToInt32(intXCancel) >= 0 && Convert.ToInt32(intYCancel) >= 0)
            {
                var moveToRet = _dm.MoveTo(Convert.ToInt32(intXCancel), Convert.ToInt32(intYCancel));
                _logger.Info("moveTo Ret Cancel: " + moveToRet);
                int leftClickRet = _dm.LeftClick();
                _logger.Info("leftClickRet Cancel: " + leftClickRet);
            }
            Thread.Sleep(sleepInterval);

            ////关闭"O K"界面
            //var findStrO_KFastRet = _dm.FindStrFast(0, 0, 2000, 2000, "O_K", "000000-000000", 1.0, out object intXO_K, out object intYO_K);
            //_logger.Info("findStrFastRet O_K: " + findStrO_KFastRet);
            //if (Convert.ToInt32(intXO_K) >= 0 && Convert.ToInt32(intYO_K) >= 0)
            //{
            //    var moveToRetO_K = _dm.MoveTo(Convert.ToInt32(intXO_K), Convert.ToInt32(intYO_K));
            //    _logger.Info("moveToRet O_K: " + moveToRetO_K);
            //    int leftClickRetO_K = _dm.LeftClick();
            //    _logger.Info("leftClickRet O_K: " + leftClickRetO_K);
            //    Thread.Sleep(sleepInterval);

            //    //关闭"OK"界面
            //    var findStrOKFastRet = _dm.FindStrFast(0, 0, 2000, 2000, "OK", "000000-000000", 1.0, out object intXOK, out object intYOK);
            //    _logger.Info("findStrFastRet OK: " + findStrOKFastRet);
            //    if (Convert.ToInt32(intXOK) >= 0 && Convert.ToInt32(intYOK) >= 0)
            //    {
            //        var moveToRetOK = _dm.MoveTo(Convert.ToInt32(intXOK), Convert.ToInt32(intYOK));
            //        _logger.Info("moveToRet OK: " + moveToRetOK);
            //        int leftClickRetOK = _dm.LeftClick();
            //        _logger.Info("leftClickRet OK: " + leftClickRetOK);
            //    }
            //}
            //Thread.Sleep(sleepInterval);

            //关闭"O K"界面//只有新厂有这个界面
            _autoIt.ControlClick("[CLASS:PpgFile]", "", "[ID:32501]");
            Thread.Sleep(sleepInterval);
            //关闭"OK"界面//只有新厂有这个界面
            _autoIt.ControlClick("Operation Confirm", "", "[ID:1]");
            Thread.Sleep(sleepInterval);


            //点击return
            _autoIt.ControlClick(title, "", "[ID:48]");
            Thread.Sleep(sleepInterval);

            //点击overview
            _autoIt.ControlClick(title, "", "[ID:1]");
            Thread.Sleep(sleepInterval);

            //点击windowName对应的按钮
            string btnId = "";
            switch (windowName)
            {
                case "COUNTER":
                    btnId = "15";
                    break;
                case "GRINDING":
                    btnId = "11";
                    break;
                case "VACUUM":
                    btnId = "13";
                    break;
                case "UTILITY":
                    btnId = "14";
                    break;
                default:
                    _logger.Info($"{windowName} no defined");
                    break;
            }
            _autoIt.ControlClick(title, "", $"[ID:{btnId}]");
        }

        private bool GetControlVisibility(string strTitle, string controlAccessId)
        {
            var handle = _autoIt.ControlGetHandle(strTitle, "", controlAccessId);
            _logger.Info($"{controlAccessId} handle:" + handle);

            var i = int.Parse(handle, System.Globalization.NumberStyles.HexNumber);
            IntPtr p = new IntPtr(i);
            var sw2 = new SystemWindow(p);
            return sw2.VisibilityFlag;
        }





    }
}
