﻿using System;

namespace Jcp.Pg300
{
    partial class SECSEngineRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbnConnect = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtIpaddress = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.txtCurrentRecipe = new System.Windows.Forms.TextBox();
            this.btnPpSelect2 = new System.Windows.Forms.Button();
            this.btnUnloadWafer = new System.Windows.Forms.Button();
            this.btnParseRecipe = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.txtAlarmCode = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lsboxRecipe = new System.Windows.Forms.ListBox();
            this.txtSemiStatus = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnPPSelect = new System.Windows.Forms.Button();
            this.txtEQPstatus = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCurrentRecipe = new System.Windows.Forms.Button();
            this.btnGetfromForm = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAlarm = new System.Windows.Forms.TextBox();
            this.btnAllRecipeList = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEventName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtProductStatus = new System.Windows.Forms.TextBox();
            this.btnRecipeDownload = new System.Windows.Forms.Button();
            this.btnRecipeUpload = new System.Windows.Forms.Button();
            this.btnDeleteRecipe = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.valueName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnFindStatePic = new System.Windows.Forms.Button();
            this.btnComboSelect = new System.Windows.Forms.Button();
            this.btnShowTopMostWindow = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbnConnect
            // 
            this.tbnConnect.Enabled = false;
            this.tbnConnect.Location = new System.Drawing.Point(28, 32);
            this.tbnConnect.Margin = new System.Windows.Forms.Padding(2);
            this.tbnConnect.Name = "tbnConnect";
            this.tbnConnect.Size = new System.Drawing.Size(93, 29);
            this.tbnConnect.TabIndex = 46;
            this.tbnConnect.Text = "Start SECS";
            this.tbnConnect.UseVisualStyleBackColor = true;
            this.tbnConnect.Click += new System.EventHandler(this.tbnConnect_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(21, 103);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 51;
            this.label16.Text = "Port";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(58, 100);
            this.txtPort.Margin = new System.Windows.Forms.Padding(2);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(77, 21);
            this.txtPort.TabIndex = 50;
            this.txtPort.Text = "5000";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(26, 73);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 49;
            this.label17.Text = "IP";
            // 
            // txtIpaddress
            // 
            this.txtIpaddress.Location = new System.Drawing.Point(58, 70);
            this.txtIpaddress.Margin = new System.Windows.Forms.Padding(2);
            this.txtIpaddress.Name = "txtIpaddress";
            this.txtIpaddress.Size = new System.Drawing.Size(77, 21);
            this.txtIpaddress.TabIndex = 48;
            this.txtIpaddress.Text = "127.0.0.1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(916, 441);
            this.tabControl1.TabIndex = 62;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.btnClose);
            this.tabPage1.Controls.Add(this.tbnConnect);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.txtIpaddress);
            this.tabPage1.Controls.Add(this.txtPort);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(908, 415);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Basic";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 58;
            this.label1.Text = "Remote";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(28, 136);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 23);
            this.button2.TabIndex = 57;
            this.button2.Text = "Local/Remote";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SwitchControlState);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(28, 185);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(129, 20);
            this.btnClose.TabIndex = 52;
            this.btnClose.Text = "Close by ENG";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.txtCurrentRecipe);
            this.tabPage2.Controls.Add(this.btnPpSelect2);
            this.tabPage2.Controls.Add(this.btnUnloadWafer);
            this.tabPage2.Controls.Add(this.btnParseRecipe);
            this.tabPage2.Controls.Add(this.buttonStop);
            this.tabPage2.Controls.Add(this.buttonStart);
            this.tabPage2.Controls.Add(this.txtAlarmCode);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.lsboxRecipe);
            this.tabPage2.Controls.Add(this.txtSemiStatus);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.btnPPSelect);
            this.tabPage2.Controls.Add(this.txtEQPstatus);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.btnCurrentRecipe);
            this.tabPage2.Controls.Add(this.btnGetfromForm);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtAlarm);
            this.tabPage2.Controls.Add(this.btnAllRecipeList);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtEventName);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txtProductStatus);
            this.tabPage2.Controls.Add(this.btnRecipeDownload);
            this.tabPage2.Controls.Add(this.btnRecipeUpload);
            this.tabPage2.Controls.Add(this.btnDeleteRecipe);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(908, 415);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Engineer";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(744, 277);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 68;
            this.button3.Text = "SetLotId";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtCurrentRecipe
            // 
            this.txtCurrentRecipe.Location = new System.Drawing.Point(519, 336);
            this.txtCurrentRecipe.Name = "txtCurrentRecipe";
            this.txtCurrentRecipe.Size = new System.Drawing.Size(161, 21);
            this.txtCurrentRecipe.TabIndex = 67;
            // 
            // btnPpSelect2
            // 
            this.btnPpSelect2.Location = new System.Drawing.Point(678, 369);
            this.btnPpSelect2.Margin = new System.Windows.Forms.Padding(2);
            this.btnPpSelect2.Name = "btnPpSelect2";
            this.btnPpSelect2.Size = new System.Drawing.Size(141, 27);
            this.btnPpSelect2.TabIndex = 66;
            this.btnPpSelect2.Text = "PPSelect2";
            this.btnPpSelect2.UseVisualStyleBackColor = true;
            this.btnPpSelect2.Click += new System.EventHandler(this.btnPpSelect2_Click);
            // 
            // btnUnloadWafer
            // 
            this.btnUnloadWafer.Location = new System.Drawing.Point(22, 369);
            this.btnUnloadWafer.Margin = new System.Windows.Forms.Padding(2);
            this.btnUnloadWafer.Name = "btnUnloadWafer";
            this.btnUnloadWafer.Size = new System.Drawing.Size(95, 27);
            this.btnUnloadWafer.TabIndex = 65;
            this.btnUnloadWafer.Text = "Unload Wafer";
            this.btnUnloadWafer.UseVisualStyleBackColor = true;
            this.btnUnloadWafer.Click += new System.EventHandler(this.btnUnloadWafer_Click);
            // 
            // btnParseRecipe
            // 
            this.btnParseRecipe.Location = new System.Drawing.Point(352, 241);
            this.btnParseRecipe.Name = "btnParseRecipe";
            this.btnParseRecipe.Size = new System.Drawing.Size(141, 27);
            this.btnParseRecipe.TabIndex = 64;
            this.btnParseRecipe.Text = "ParseRecipe";
            this.btnParseRecipe.UseVisualStyleBackColor = true;
            this.btnParseRecipe.Click += new System.EventHandler(this.btnParseRecipe_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(138, 329);
            this.buttonStop.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(99, 27);
            this.buttonStop.TabIndex = 61;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(22, 329);
            this.buttonStart.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(95, 27);
            this.buttonStart.TabIndex = 60;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtAlarmCode
            // 
            this.txtAlarmCode.Location = new System.Drawing.Point(23, 274);
            this.txtAlarmCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtAlarmCode.Name = "txtAlarmCode";
            this.txtAlarmCode.Size = new System.Drawing.Size(229, 21);
            this.txtAlarmCode.TabIndex = 59;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 255);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 12);
            this.label18.TabIndex = 58;
            this.label18.Text = "当前警报Code";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 91);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "当前Process状态";
            // 
            // lsboxRecipe
            // 
            this.lsboxRecipe.FormattingEnabled = true;
            this.lsboxRecipe.ItemHeight = 12;
            this.lsboxRecipe.Location = new System.Drawing.Point(352, 34);
            this.lsboxRecipe.Margin = new System.Windows.Forms.Padding(2);
            this.lsboxRecipe.Name = "lsboxRecipe";
            this.lsboxRecipe.Size = new System.Drawing.Size(316, 160);
            this.lsboxRecipe.TabIndex = 38;
            // 
            // txtSemiStatus
            // 
            this.txtSemiStatus.Location = new System.Drawing.Point(30, 109);
            this.txtSemiStatus.Margin = new System.Windows.Forms.Padding(2);
            this.txtSemiStatus.Name = "txtSemiStatus";
            this.txtSemiStatus.Size = new System.Drawing.Size(81, 21);
            this.txtSemiStatus.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(141, 46);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 12);
            this.label12.TabIndex = 30;
            this.label12.Text = "当前设备状态";
            // 
            // btnPPSelect
            // 
            this.btnPPSelect.Location = new System.Drawing.Point(352, 201);
            this.btnPPSelect.Margin = new System.Windows.Forms.Padding(2);
            this.btnPPSelect.Name = "btnPPSelect";
            this.btnPPSelect.Size = new System.Drawing.Size(141, 27);
            this.btnPPSelect.TabIndex = 35;
            this.btnPPSelect.Text = "PPSelect";
            this.btnPPSelect.UseVisualStyleBackColor = true;
            this.btnPPSelect.Click += new System.EventHandler(this.btnPPSelect_Click);
            // 
            // txtEQPstatus
            // 
            this.txtEQPstatus.Location = new System.Drawing.Point(138, 63);
            this.txtEQPstatus.Margin = new System.Windows.Forms.Padding(2);
            this.txtEQPstatus.Name = "txtEQPstatus";
            this.txtEQPstatus.Size = new System.Drawing.Size(81, 21);
            this.txtEQPstatus.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 329);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 12);
            this.label11.TabIndex = 28;
            this.label11.Text = " ";
            // 
            // btnCurrentRecipe
            // 
            this.btnCurrentRecipe.Location = new System.Drawing.Point(352, 329);
            this.btnCurrentRecipe.Margin = new System.Windows.Forms.Padding(2);
            this.btnCurrentRecipe.Name = "btnCurrentRecipe";
            this.btnCurrentRecipe.Size = new System.Drawing.Size(141, 27);
            this.btnCurrentRecipe.TabIndex = 34;
            this.btnCurrentRecipe.Text = "get current recipe";
            this.btnCurrentRecipe.UseVisualStyleBackColor = true;
            this.btnCurrentRecipe.Click += new System.EventHandler(this.btnCurrentRecipe_Click);
            // 
            // btnGetfromForm
            // 
            this.btnGetfromForm.Location = new System.Drawing.Point(35, 15);
            this.btnGetfromForm.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetfromForm.Name = "btnGetfromForm";
            this.btnGetfromForm.Size = new System.Drawing.Size(83, 27);
            this.btnGetfromForm.TabIndex = 26;
            this.btnGetfromForm.Text = "GETFORMDATA";
            this.btnGetfromForm.UseVisualStyleBackColor = true;
            this.btnGetfromForm.Click += new System.EventHandler(this.btnGetfromForm_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 201);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "当前报警信息";
            // 
            // txtAlarm
            // 
            this.txtAlarm.Location = new System.Drawing.Point(22, 221);
            this.txtAlarm.Margin = new System.Windows.Forms.Padding(2);
            this.txtAlarm.Name = "txtAlarm";
            this.txtAlarm.Size = new System.Drawing.Size(229, 21);
            this.txtAlarm.TabIndex = 20;
            // 
            // btnAllRecipeList
            // 
            this.btnAllRecipeList.Location = new System.Drawing.Point(352, 4);
            this.btnAllRecipeList.Margin = new System.Windows.Forms.Padding(2);
            this.btnAllRecipeList.Name = "btnAllRecipeList";
            this.btnAllRecipeList.Size = new System.Drawing.Size(315, 27);
            this.btnAllRecipeList.TabIndex = 33;
            this.btnAllRecipeList.Text = "recipeList";
            this.btnAllRecipeList.UseVisualStyleBackColor = true;
            this.btnAllRecipeList.Click += new System.EventHandler(this.btnAllRecipeList_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 143);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "当前事件";
            // 
            // txtEventName
            // 
            this.txtEventName.Location = new System.Drawing.Point(25, 170);
            this.txtEventName.Margin = new System.Windows.Forms.Padding(2);
            this.txtEventName.Name = "txtEventName";
            this.txtEventName.Size = new System.Drawing.Size(225, 21);
            this.txtEventName.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 46);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "当前产品状态";
            // 
            // txtProductStatus
            // 
            this.txtProductStatus.Location = new System.Drawing.Point(30, 63);
            this.txtProductStatus.Margin = new System.Windows.Forms.Padding(2);
            this.txtProductStatus.Name = "txtProductStatus";
            this.txtProductStatus.Size = new System.Drawing.Size(81, 21);
            this.txtProductStatus.TabIndex = 11;
            // 
            // btnRecipeDownload
            // 
            this.btnRecipeDownload.Location = new System.Drawing.Point(532, 241);
            this.btnRecipeDownload.Margin = new System.Windows.Forms.Padding(2);
            this.btnRecipeDownload.Name = "btnRecipeDownload";
            this.btnRecipeDownload.Size = new System.Drawing.Size(135, 27);
            this.btnRecipeDownload.TabIndex = 45;
            this.btnRecipeDownload.Text = "recipeDownLoad";
            this.btnRecipeDownload.UseVisualStyleBackColor = true;
            this.btnRecipeDownload.Click += new System.EventHandler(this.btnRecipeDownload_Click);
            // 
            // btnRecipeUpload
            // 
            this.btnRecipeUpload.Location = new System.Drawing.Point(532, 201);
            this.btnRecipeUpload.Margin = new System.Windows.Forms.Padding(2);
            this.btnRecipeUpload.Name = "btnRecipeUpload";
            this.btnRecipeUpload.Size = new System.Drawing.Size(135, 27);
            this.btnRecipeUpload.TabIndex = 44;
            this.btnRecipeUpload.Text = "recipeUpload";
            this.btnRecipeUpload.UseVisualStyleBackColor = true;
            this.btnRecipeUpload.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDeleteRecipe
            // 
            this.btnDeleteRecipe.Location = new System.Drawing.Point(352, 274);
            this.btnDeleteRecipe.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteRecipe.Name = "btnDeleteRecipe";
            this.btnDeleteRecipe.Size = new System.Drawing.Size(141, 27);
            this.btnDeleteRecipe.TabIndex = 39;
            this.btnDeleteRecipe.Text = "delete recipe";
            this.btnDeleteRecipe.UseVisualStyleBackColor = true;
            this.btnDeleteRecipe.Click += new System.EventHandler(this.btnDeleteRecipe_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button8);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.valueName);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.btnFindStatePic);
            this.tabPage3.Controls.Add(this.btnComboSelect);
            this.tabPage3.Controls.Add(this.btnShowTopMostWindow);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(908, 415);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Test";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(553, 161);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 69;
            this.button8.Text = "alarmwindow";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(553, 219);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 68;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(553, 308);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 67;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(553, 278);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 66;
            this.button5.Text = "enable";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(553, 248);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 65;
            this.button4.Text = "disable";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // valueName
            // 
            this.valueName.Location = new System.Drawing.Point(206, 214);
            this.valueName.Name = "valueName";
            this.valueName.Size = new System.Drawing.Size(100, 21);
            this.valueName.TabIndex = 64;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(29, 206);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(162, 35);
            this.button1.TabIndex = 63;
            this.button1.Text = "GetValue";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.GetValueByORC);
            // 
            // btnFindStatePic
            // 
            this.btnFindStatePic.Location = new System.Drawing.Point(29, 145);
            this.btnFindStatePic.Name = "btnFindStatePic";
            this.btnFindStatePic.Size = new System.Drawing.Size(162, 39);
            this.btnFindStatePic.TabIndex = 62;
            this.btnFindStatePic.Text = "FindStatePic";
            this.btnFindStatePic.UseVisualStyleBackColor = true;
            this.btnFindStatePic.Click += new System.EventHandler(this.btnFindStatePic_Click);
            // 
            // btnComboSelect
            // 
            this.btnComboSelect.Location = new System.Drawing.Point(29, 100);
            this.btnComboSelect.Name = "btnComboSelect";
            this.btnComboSelect.Size = new System.Drawing.Size(162, 33);
            this.btnComboSelect.TabIndex = 61;
            this.btnComboSelect.Text = "ComboSelect";
            this.btnComboSelect.UseVisualStyleBackColor = true;
            this.btnComboSelect.Click += new System.EventHandler(this.btnComboSelect_Click);
            // 
            // btnShowTopMostWindow
            // 
            this.btnShowTopMostWindow.Location = new System.Drawing.Point(29, 38);
            this.btnShowTopMostWindow.Name = "btnShowTopMostWindow";
            this.btnShowTopMostWindow.Size = new System.Drawing.Size(117, 41);
            this.btnShowTopMostWindow.TabIndex = 3;
            this.btnShowTopMostWindow.Text = "ShowTopMost";
            this.btnShowTopMostWindow.UseVisualStyleBackColor = true;
            this.btnShowTopMostWindow.Click += new System.EventHandler(this.btnShowTopMostWindow_Click);
            // 
            // SECSEngineRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(916, 441);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "SECSEngineRun";
            this.Text = "EAP for PG300";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SECSEngineRun_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SECSEngineRun_FormClosed);
            this.Load += new System.EventHandler(this.SECSEngineRun_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        private void btnRecipeDownload_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void btnDeleteRecipe_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void btnParseRecipe_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion
        private System.Windows.Forms.Button tbnConnect;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtIpaddress;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnShowTopMostWindow;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnComboSelect;
        private System.Windows.Forms.Button btnFindStatePic;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnUnloadWafer;
        private System.Windows.Forms.Button btnParseRecipe;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox txtAlarmCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListBox lsboxRecipe;
        private System.Windows.Forms.TextBox txtSemiStatus;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnPPSelect;
        private System.Windows.Forms.TextBox txtEQPstatus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCurrentRecipe;
        private System.Windows.Forms.Button btnGetfromForm;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAlarm;
        private System.Windows.Forms.Button btnAllRecipeList;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEventName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtProductStatus;
        private System.Windows.Forms.Button btnRecipeDownload;
        private System.Windows.Forms.Button btnRecipeUpload;
        private System.Windows.Forms.Button btnDeleteRecipe;
        private System.Windows.Forms.Button btnPpSelect2;
        private System.Windows.Forms.TextBox txtCurrentRecipe;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox valueName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
    }
}