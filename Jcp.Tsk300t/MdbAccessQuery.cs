﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace Jcp.Pg300
{
    public class MdbAccessQuery
    {

        private static log4net.ILog _logger = log4net.LogManager.GetLogger("App");
        public static string connectionString = "";


        public static void Start(string fileName)
        {
            connectionString =
                @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                $@"Data Source=C:\dicer\acct_mdb\" + fileName;

        }

        public static void Stop()
        {
            //_connection.Dispose();
        }
        public static DataTable GetDataTableByQuerying(string query)
        {
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    using (OleDbCommand command = new OleDbCommand(query, connection))
                    {
                        OleDbDataReader reader = command.ExecuteReader();

                        DataTable myDataTable = new DataTable();
                        myDataTable.Load(reader);
                        reader.Close();

                        return myDataTable;
                    }
                }

                catch (Exception ex)
                {
                    _logger.Error(ex);
                    _logger.Error("Wrong SQL: " + query);
                    return null;
                }
            }
            //finally
            //{
            //    connection.Close();
            //}
        }
    }
}
