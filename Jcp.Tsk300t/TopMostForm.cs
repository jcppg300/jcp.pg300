﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Jcp.Pg300
{
    public partial class TopMostForm : Form
    {
        private log4net.ILog _logger = log4net.LogManager.GetLogger("App");

        public const int GWL_EXSTYLE = -20;
        public const UInt32 WS_EX_TOPMOST = 0x0008;
        public const int SWP_NOMOVE = 2;
        public const int SWP_NOSIZE = 1;
        public const int HWND_TOPMOST = -1;
        public const int HWND_NOTOPMOST = -2;

        [DllImport("User32.dll", SetLastError = false)]
        static extern bool SetWindowPos(IntPtr hwnd, IntPtr hwndinsertafter, int x, int y, int cx, int cy, UInt32 flags);

        public TopMostForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.MaximumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
            this.WindowState = FormWindowState.Maximized;
            TopMost = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            _logger.Info("close top most dialog.");
            base.OnClosed(e);
        }

        public void ShowTopMost(string message)
        {
            _logger.Info("show top most dialog.");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.Append(message);

            this.txtMessage.Clear();
            this.txtMessage.Text = sb.ToString();

            this.Show();
            SetWindowPos(this.Handle, (IntPtr)HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        }

        public void ShowTopMost(string caption, string message)
        {
            _logger.Info("show top most dialog.");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.Append(caption);
            sb.Append(" \r\n");
            sb.Append(message);

            this.txtMessage.Clear();
            this.txtMessage.Text = sb.ToString();

            this.Show();
            SetWindowPos(this.Handle, (IntPtr)HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        }

        //public void ShowTopMost(IWin32Window parent, string message)
        //{
        //    _logger.Info("show top most dialog.");
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine();
        //    sb.AppendLine();
        //    sb.Append(message);

        //    this.txtMessage.Clear();
        //    this.txtMessage.Text = sb.ToString();

        //    this.ShowDialog(parent);
        //    SetWindowPos(this.Handle, (IntPtr)HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        //}

        private void btnRelease_Click(object sender, EventArgs e)
        {
            if (txtPwd.Text.Trim() == "1203012")
            {
                this.Close();
            }
            else
            {
                _logger.Info($"wrong password:[{txtPwd.Text}]");
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtPwd.Text = txtPwd.Text + "9";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPwd.Text = string.Empty;
        }
    }
}
