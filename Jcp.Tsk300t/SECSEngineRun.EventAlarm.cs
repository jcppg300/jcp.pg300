﻿using Ctg.XTap.SecsGem;
using Jcp.Pg300;


using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static Jcp.Pg300.StatusEnum;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun
    {
        private List<EventRecord> _eventRecordList = new List<EventRecord>();
        //Dictionary<int, string> _dctAlarmDef = new Dictionary<int, string>();
        private Dictionary<string, string> _dctAlarmDef = new Dictionary<string, string>();

        private LogWatcher _alarmLogWatcher;
        private LogWatcher _opeLogWatcher;
        private LogWatcher _lotLogWatcher;
        private LogWatcher _opeLogChangedWatcher;
        private DateTime _previousEventTime = new DateTime();
        private DateTime _previousAlarmSetTime = new DateTime();
        private DateTime _previousAlarmClearTime = new DateTime();

        private List<string> _lstPreviousLines = null;
        private List<string> _lstCurrentLines = null;
        private List<string> _lstAddedLines = null;

        private Queue<string> _alarmsQueue = new Queue<string>();

        private string _alarmInfo = string.Empty;
        private string _alarmCode = string.Empty;
        //private string _semiStatus = string.Empty;
        //private string _previousSemiStatus = string.Empty;
        private string _semiStatus = ProcessStatusEnum.INIT.ToString();
        private string _previousSemiStatus = ProcessStatusEnum.INIT.ToString();
        public string PreviousSemiStatus { get => _previousSemiStatus; set => _previousSemiStatus = value; }
        public string SemiStatus
        {
            get => _semiStatus;
            set
            {
                _previousSemiStatus = _semiStatus;

                switch (value)
                {
                    case "ABORT1":
                    case "ABORT2":
                        value = "ABORT";
                        break;
                    case "APAUSED":
                        value = "A_PAUSED";
                        break;
                    case "APAUSIN":
                        value = "A_PAUSING";
                        break;
                    case "STOPPIN":
                        value = "STOPPING";
                        break;
                    default:
                        break;
                }
                if (value.Contains("STOP") && value != "STOPPING") value = "E_STOP";
                _semiStatus = value;
            }
        }


        private string _port1State = ProcessStatusEnum.INIT.ToString();
        private string _port2State = ProcessStatusEnum.INIT.ToString();

        //弃用 改为OCR
        ////在ready状态下报警将转变为什么状态？
        //private readonly StatusEnum.ProcessStatusEnum[,] _processState2DArray = new StatusEnum.ProcessStatusEnum[10, 9] {
        //                { ProcessStatusEnum.IDLE,ProcessStatusEnum.SETUP,ProcessStatusEnum.READY,ProcessStatusEnum.RUNNING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.PAUSING,ProcessStatusEnum.PAUSING,ProcessStatusEnum.notfound},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.SETUP,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound},
        //                { ProcessStatusEnum.IDLE,ProcessStatusEnum.notfound,ProcessStatusEnum.READY,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.SETUP,ProcessStatusEnum.notfound,ProcessStatusEnum.RUNNING,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.STOPPING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.PAUSING,ProcessStatusEnum.PAUSING,ProcessStatusEnum.notfound},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.PAUSING,ProcessStatusEnum.PAUSING,ProcessStatusEnum.IDLE},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.PAUSED},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.RUNNING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.A_PAUSED},
        //                { ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.RUNNING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.STOPPING,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound,ProcessStatusEnum.notfound},
        //            };
        //private readonly StatusEnum.PortStatusEnum[,] _portState2DArray = new StatusEnum.PortStatusEnum[5, 5] {
        //                { PortStatusEnum.INIT,PortStatusEnum.IDLE,PortStatusEnum.SETUP,PortStatusEnum.READY,PortStatusEnum.UDONE},
        //                { PortStatusEnum.IDLE,PortStatusEnum.IDLE,PortStatusEnum.SETUP,PortStatusEnum.READY,PortStatusEnum.UDONE},
        //                { PortStatusEnum.SETUP,PortStatusEnum.SETUP,PortStatusEnum.SETUP,PortStatusEnum.READY,PortStatusEnum.UDONE},
        //                { PortStatusEnum.READY,PortStatusEnum.READY,PortStatusEnum.READY,PortStatusEnum.READY,PortStatusEnum.UDONE},
        //                { PortStatusEnum.UDONE,PortStatusEnum.UDONE,PortStatusEnum.UDONE,PortStatusEnum.UDONE,PortStatusEnum.UDONE}
        //            };

        private void InitLogWatching()
        {
            _alarmLogWatcher = new LogWatcher(Constants.AlarmLogFolder);
            _alarmLogWatcher.LinesAddedEvent += _alarmLogWatcher_LinesAddedEvent;

            _opeLogWatcher = new LogWatcher(Constants.OperationLogFolder);
            _opeLogWatcher.LinesAddedEvent += _opeLogWatcher_LinesAddedEvent;

            _lotLogWatcher = new LogWatcher(Constants.LotLogFolder);
            _lotLogWatcher.LinesAddedEvent += _lotLogWatcher_LinesAddedEvent;

            _opeLogChangedWatcher = new LogWatcher(Constants.OperationLogFolder);
            _opeLogChangedWatcher.LinesAddedEvent += _opeLogWatcher_FileChangedEvent;
        }

        private void _alarmLogWatcher_LinesAddedEvent(object sender, LinesAddedArgs e)
        {
            try
            {
                _logger.Info(JsonConvert.SerializeObject(e.LstLins, Formatting.Indented));

                ThreadPool.QueueUserWorkItem(WatchAlarmSet, e.LstLins);
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex));
            }
        }

        private void _opeLogWatcher_LinesAddedEvent(object sender, LinesAddedArgs e)
        {
            try
            {
                _logger.Info(JsonConvert.SerializeObject(e.LstLins, Formatting.Indented));

                ThreadPool.QueueUserWorkItem(WatchPortState, e.LstLins);
                ThreadPool.QueueUserWorkItem(WatchEvent, e.LstLins);
                ThreadPool.QueueUserWorkItem(WatchAlarmClear_opeLog, e.LstLins);
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex));
            }
        }

        private void _lotLogWatcher_LinesAddedEvent(object sender, LinesAddedArgs e)
        {
            try
            {
                _logger.Info(JsonConvert.SerializeObject(e.LstLins, Formatting.Indented));

                ThreadPool.QueueUserWorkItem(WatchEvent, e.LstLins);
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex));
            }
        }
        //OCR抓取process state
        private void _opeLogWatcher_FileChangedEvent(object sender, EventArgs e)
        {
            try
            {
                SemiStatus = _dmOfProcessstate.Ocr(927, 29, 1010, 45, "ffffff-000000", 1.0);
                _logger.Info("SemiStatus: " + SemiStatus);

                if (SemiStatus != PreviousSemiStatus)
                {
                    ProcessStatusEnum processStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), SemiStatus);
                    ProcessStatusEnum previousprocessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), PreviousSemiStatus);
                    _secs.Send(S6F11(new uint[] { 208 }, new uint[] { 2 }, new List<string>(new string[] { ((int)processStatusEnum).ToString(), ((int)previousprocessStatusEnum).ToString() })));
                    this.Invoke(new Action(() => txtSemiStatus.Text = SemiStatus));
                    _logger.Info("SemiStatus : " + SemiStatus);

                    if (_dmOfProcessstate.Ocr(400, 15, 640, 40, "ffffff-000000", 1.0).Equals("WARMING") && SemiStatus.Equals(ProcessStatusEnum.RUNNING.ToString()))
                    {
                        _secs.Send(S6F11(new uint[] { 2002 }, new uint[] { 1 }, new List<string>(new string[] { "WARMING UP Model RUNNING" })));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex, Formatting.Indented));
            }

        }


        #region testcode
        //private void WatchPortState(object lstLins)
        //{
        //    try
        //    {
        //        List<string> recentStLins = (List<string>)lstLins;

        //        List<string> processStateChangeLines = recentStLins.FindAll(x =>
        //                    //移除时间字符
        //                    x.Remove(0, 9).StartsWith("PORT")
        //                    );

        //        _logger.Info(JsonConvert.SerializeObject(processStateChangeLines, Formatting.Indented));

        //        lock (this)
        //        {

        //            foreach (var line in processStateChangeLines)
        //            {
        //                //移除日志记录中的时间字符串
        //                var statusKeyWords = line.Remove(0, 9);
        //                _logger.Info("StateKeyWords : " + statusKeyWords);
        //                #region testcode

        //                //switch (statusKeyWords)
        //                //{
        //                //    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                //    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                //    case StatusLineConstants.PORT1_READY_STATUS:
        //                //    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                //    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                //    case StatusLineConstants.PORT2_READY_STATUS:
        //                //        PortStatusEnum previousProcessStatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _semiStatus);
        //                //        StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(statusKeyWords);
        //                //        PortStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                //        _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                //        break;

        //                //    ////这里应该放在WatchAlarmClear中
        //                //    //case StatusLineConstants.AUTO_START:
        //                //    //    if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSED.ToString())
        //                //    //        ClearAlarm(line);
        //                //    //    break;
        //                //    default:
        //                //        break;
        //                //}

        //                #endregion

        //                //判断关键字
        //                switch (statusKeyWords)
        //                {
        //                    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.IDLE.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.SETUP.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT1_READY_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.READY.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.UDONE.ToString();
        //                        break;

        //                    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.IDLE.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.SETUP.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT2_READY_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.READY.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.UDONE.ToString();
        //                        break;

        //                    default:
        //                        break;
        //                }

        //                switch (statusKeyWords)
        //                {
        //                    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                    case StatusLineConstants.PORT1_READY_STATUS:
        //                    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                        {
        //                            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                            _portState = portStatusEnum.ToString();

        //                            _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));
        //                        }
        //                        break;
        //                    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                    case StatusLineConstants.PORT2_READY_STATUS:
        //                    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                        {
        //                            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                            _portState = portStatusEnum.ToString();

        //                            _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));
        //                        }
        //                        break;

        //                    default:
        //                        break;
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(JsonConvert.SerializeObject(ex));
        //    }
        //}
        #endregion
        #region oldcode
        /*private void WatchProcessState(object lstLins)//PortState与ProcessState强相关，拆分易出现多线程误读
        {
            try
            {
                List<string> recentStLins = (List<string>)lstLins;

                List<string> processStateChangeLines = recentStLins.FindAll(x =>
                            //移除时间字符
                            x.Remove(0, 9).StartsWith("AUTO") ||
                            x.Remove(0, 9).StartsWith("PORT")
                            );

                _logger.Info(JsonConvert.SerializeObject(processStateChangeLines, Formatting.Indented));

                lock (this)
                {

                    foreach (var line in processStateChangeLines)
                    {
                        //移除日志记录中的时间字符串
                        var statusKeyWords = line.Remove(0, 9);
                        _logger.Info("StateKeyWords : " + statusKeyWords);

                        //判断关键字
                        switch (statusKeyWords)
                        {
                            case StatusLineConstants.PORT1_IDLE_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.IDLE.ToString();
                                break;
                            case StatusLineConstants.PORT1_SETUP_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.SETUP.ToString();
                                break;
                            case StatusLineConstants.PORT1_READY_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.READY.ToString();
                                break;
                            case StatusLineConstants.PORT1_UDONE_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.UDONE.ToString();
                                break;

                            case StatusLineConstants.PORT2_IDLE_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.IDLE.ToString();
                                break;
                            case StatusLineConstants.PORT2_SETUP_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.SETUP.ToString();
                                break;
                            case StatusLineConstants.PORT2_READY_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.READY.ToString();
                                break;
                            case StatusLineConstants.PORT2_UDONE_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.UDONE.ToString();
                                break;

                            default:
                                break;
                        }

                        #region oldcode


                        //switch (statusKeyWords)
                        //{
                        //    case StatusLineConstants.PORT1_IDLE_STATUS:
                        //    case StatusLineConstants.PORT1_SETUP_STATUS:
                        //    case StatusLineConstants.PORT1_READY_STATUS:
                        //    case StatusLineConstants.PORT1_UDONE_STATUS:
                        //        {
                        //            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
                        //            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
                        //            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
                        //            _portState = portStatusEnum.ToString();
                        //            _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));
                        //            if (_portState != PortStatusEnum.UDONE.ToString())
                        //            {
                        //                ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                        //                StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
                        //                ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

                        //                _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
                        //            }

                        //        }
                        //        break;
                        //    case StatusLineConstants.PORT2_IDLE_STATUS:
                        //    case StatusLineConstants.PORT2_SETUP_STATUS:
                        //    case StatusLineConstants.PORT2_READY_STATUS:
                        //    case StatusLineConstants.PORT2_UDONE_STATUS:
                        //        {
                        //            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
                        //            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
                        //            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
                        //            _portState = portStatusEnum.ToString();
                        //            _secs.Send(S6F11(new uint[] { 217 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));
                        //            if (_portState != PortStatusEnum.UDONE.ToString())
                        //            {
                        //                ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                        //                StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
                        //                ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

                        //                _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
                        //            }
                        //        }
                        //        break;

                        //    case StatusLineConstants.AUTO_RUNNING_STATE:
                        //    case StatusLineConstants.AUTO_STOPPING_STATE:
                        //    case StatusLineConstants.AUTO_STOP:
                        //    case StatusLineConstants.AUTO_PAUSE:
                        //    case StatusLineConstants.AUTO_HALT:
                        //    case StatusLineConstants.AUTO_FINISH:
                        //        { 
                        //            ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                        //            StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(statusKeyWords);
                        //            ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

                        //            _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
                        //        }
                        //        break;

                        //    //这里应该放在WatchAlarmClear中
                        //    case StatusLineConstants.AUTO_START:
                        //        if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSED.ToString())
                        //            ClearAlarm(line);
                        //        break;
                        //    default:
                        //        break;
                        //}
                        #endregion
                        switch (statusKeyWords)
                        {
                            case StatusLineConstants.PORT1_IDLE_STATUS:
                            case StatusLineConstants.PORT1_SETUP_STATUS:
                            case StatusLineConstants.PORT1_READY_STATUS:
                            case StatusLineConstants.PORT1_UDONE_STATUS:
                                {
                                    PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
                                    PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
                                    PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
                                    _portState = portStatusEnum.ToString();

                                    _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));
                                    
                                    if (_portState != PortStatusEnum.UDONE.ToString())
                                    {
                                        ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                                        StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
                                        ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

                                        _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
                                    }

                                }
                                break;
                            case StatusLineConstants.PORT2_IDLE_STATUS:
                            case StatusLineConstants.PORT2_SETUP_STATUS:
                            case StatusLineConstants.PORT2_READY_STATUS:
                            case StatusLineConstants.PORT2_UDONE_STATUS:
                                {
                                    PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
                                    PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
                                    PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
                                    _portState = portStatusEnum.ToString();
                                    
                                    _secs.Send(S6F11(new uint[] { 217 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));
                                    
                                    if (_portState != PortStatusEnum.UDONE.ToString())
                                    {
                                        ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                                        StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
                                        ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];
                                        
                                        _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
                                    }
                                }
                                break;

                            case StatusLineConstants.AUTO_RUNNING_STATE:
                            case StatusLineConstants.AUTO_STOPPING_STATE:
                            case StatusLineConstants.AUTO_STOP:
                            case StatusLineConstants.AUTO_PAUSE:
                            case StatusLineConstants.AUTO_HALT:
                            case StatusLineConstants.AUTO_FINISH:
                                {
                                    ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                                    StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(statusKeyWords);
                                    ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

                                    _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
                                }
                                break;


                            //这里应该放在WatchAlarmClear中
                            case StatusLineConstants.AUTO_START:
                                if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSED.ToString())
                                    ClearAlarm(line);
                                break;
                            default:
                                break;
                        }

                        #region MyRegion

                        ////判断关键字
                        //switch (statusKeyWords)
                        //{
                        //    case StatusLineConstants.AUTO_HOME_START:
                        //    case StatusLineConstants.AUTO_HOME_ABORT:
                        //    case StatusLineConstants.AUTO_HOME_FINISH:
                        //    case StatusLineConstants.PORT1_IDLE_STATUS:
                        //        _semiStatus = StatusEnum.ProcessStatusEnum.IDLE.ToString();
                        //        break;

                        //    case StatusLineConstants.PORT1_SETUP_STATUS:
                        //        _semiStatus = StatusEnum.ProcessStatusEnum.SETUP.ToString();
                        //        break;

                        //    case StatusLineConstants.PORT1_READY_STATUS:
                        //        _semiStatus = StatusEnum.ProcessStatusEnum.READY.ToString();
                        //        break;

                        //    case StatusLineConstants.AUTO_START:
                        //        if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSE.ToString())
                        //        {
                        //            ClearAlarm(line);
                        //        }
                        //        break;

                        //    case StatusLineConstants.AUTO_RUNNING_STATE:
                        //        _semiStatus = StatusEnum.ProcessStatusEnum.RUNNING.ToString();
                        //        break;

                        //    case StatusLineConstants.AUTO_STOP:
                        //    case StatusLineConstants.AUTO_STOPPING_STATE:
                        //        _semiStatus = StatusEnum.ProcessStatusEnum.STOPPING.ToString();
                        //        break;

                        //    case StatusLineConstants.AUTO_PAUSE:
                        //        if (!string.IsNullOrEmpty(_alarmCode))
                        //        {
                        //            _semiStatus = StatusEnum.ProcessStatusEnum.A_PAUSE.ToString();
                        //        }
                        //        else
                        //        {
                        //            _semiStatus = StatusEnum.ProcessStatusEnum.PAUSING.ToString();
                        //        }
                        //        break;

                        //    case StatusLineConstants.AUTO_FINISH:
                        //        if (_semiStatus == StatusEnum.ProcessStatusEnum.PAUSING.ToString())
                        //        {
                        //            _semiStatus = StatusEnum.ProcessStatusEnum.PAUSED.ToString();
                        //        }
                        //        if (_semiStatus == StatusEnum.ProcessStatusEnum.STOPPING.ToString())
                        //        {
                        //            _semiStatus = StatusEnum.ProcessStatusEnum.IDLE.ToString();
                        //        }
                        //        break;

                        //    case StatusLineConstants.AUTO_HALT:
                        //        _semiStatus = StatusEnum.ProcessStatusEnum.PAUSING.ToString();
                        //        break;

                        //    default:
                        //        break;
                        //}

                        #endregion
                        //状态改变，发StreamFunction消息
                        if (_previousSemiStatus != _semiStatus)
                        {
                            ProcessStatusEnum processStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
                            ProcessStatusEnum previousprocessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _previousSemiStatus);
                            _secs.Send(S6F11(new uint[] { 208 }, new uint[] { 2 }, new List<string>(new string[] { ((int)processStatusEnum).ToString(), ((int)previousprocessStatusEnum).ToString() })));
                            _previousSemiStatus = _semiStatus;
                            this.Invoke(new Action(() => txtSemiStatus.Text = _semiStatus));
                            _logger.Info("_semiStatus : " + _semiStatus);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex));
            }
        }*/
        #endregion

        private void WatchPortState(object lstLins)
        {
            try
            {
                List<string> recentStLins = (List<string>)lstLins;
                _logger.Info(JsonConvert.SerializeObject(recentStLins, Formatting.Indented));

                List<string> processStateChangeLines = recentStLins.FindAll(x =>
                            //移除时间字符
                            x.Remove(0, 9).StartsWith("PORT")
                            );

                lock (this)
                {
                    foreach (var line in processStateChangeLines)
                    {
                        //移除日志记录中的时间字符串
                        var statusKeyWords = line.Remove(0, 9);
                        _logger.Info("StateKeyWords : " + statusKeyWords);

                        //update_port1State、_port2State
                        switch (statusKeyWords)
                        {
                            case StatusLineConstants.PORT1_IDLE_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.IDLE.ToString();
                                break;
                            case StatusLineConstants.PORT1_SETUP_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.SETUP.ToString();
                                break;
                            case StatusLineConstants.PORT1_READY_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.READY.ToString();
                                break;
                            case StatusLineConstants.PORT1_UDONE_STATUS:
                                _port1State = StatusEnum.PortStatusEnum.UDONE.ToString();
                                break;

                            case StatusLineConstants.PORT2_IDLE_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.IDLE.ToString();
                                break;
                            case StatusLineConstants.PORT2_SETUP_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.SETUP.ToString();
                                break;
                            case StatusLineConstants.PORT2_READY_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.READY.ToString();
                                break;
                            case StatusLineConstants.PORT2_UDONE_STATUS:
                                _port2State = StatusEnum.PortStatusEnum.UDONE.ToString();
                                break;

                            default:
                                break;
                        }
                        //send _port1State、_port2State change event
                        switch (statusKeyWords)
                        {
                            case StatusLineConstants.PORT1_IDLE_STATUS:
                            case StatusLineConstants.PORT1_SETUP_STATUS:
                            case StatusLineConstants.PORT1_READY_STATUS:
                            case StatusLineConstants.PORT1_UDONE_STATUS:
                                PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
                                _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));
                                break;
                            case StatusLineConstants.PORT2_IDLE_STATUS:
                            case StatusLineConstants.PORT2_SETUP_STATUS:
                            case StatusLineConstants.PORT2_READY_STATUS:
                            case StatusLineConstants.PORT2_UDONE_STATUS:
                                PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
                                _secs.Send(S6F11(new uint[] { 217 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));
                                break;
                            default:
                                break;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex));
            }

        }
        //根据log判断ProcessState太复杂且不准确，已弃用
        //改为使用ocr判断
        //private void WatchProcessState(object lstLins)
        //{
        //    //PortState与ProcessState强相关，拆分易出现多线程误读
        //    try
        //    {
        //        List<string> recentStLins = (List<string>)lstLins;

        //        _logger.Info(JsonConvert.SerializeObject(recentStLins, Formatting.Indented));

        //        List<string> processStateChangeLines = recentStLins.FindAll(x =>
        //                    //移除时间字符
        //                    x.Remove(0, 9).StartsWith("AUTO") ||
        //                    x.Remove(0, 9).StartsWith("PORT")
        //                    );

        //        lock (this)
        //        {
        //            foreach (var line in processStateChangeLines)
        //            {
        //                //移除日志记录中的时间字符串
        //                var statusKeyWords = line.Remove(0, 9);
        //                _logger.Info("StateKeyWords : " + statusKeyWords);

        //                //update_port1State、_port2State
        //                switch (statusKeyWords)
        //                {
        //                    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.IDLE.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.SETUP.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT1_READY_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.READY.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                        _port1State = StatusEnum.PortStatusEnum.UDONE.ToString();
        //                        break;

        //                    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.IDLE.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.SETUP.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT2_READY_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.READY.ToString();
        //                        break;
        //                    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                        _port2State = StatusEnum.PortStatusEnum.UDONE.ToString();
        //                        break;

        //                    default:
        //                        break;
        //                }

        //                #region oldcode
        //                //switch (statusKeyWords)
        //                //{
        //                //    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                //    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                //    case StatusLineConstants.PORT1_READY_STATUS:
        //                //    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                //        {
        //                //            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                //            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                //            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                //            _portState = portStatusEnum.ToString();
        //                //            _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));
        //                //            if (_portState != PortStatusEnum.UDONE.ToString())
        //                //            {
        //                //                ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                //                StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
        //                //                ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                //                _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                //            }

        //                //        }
        //                //        break;
        //                //    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                //    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                //    case StatusLineConstants.PORT2_READY_STATUS:
        //                //    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                //        {
        //                //            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                //            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                //            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                //            _portState = portStatusEnum.ToString();
        //                //            _secs.Send(S6F11(new uint[] { 217 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));
        //                //            if (_portState != PortStatusEnum.UDONE.ToString())
        //                //            {
        //                //                ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                //                StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
        //                //                ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                //                _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                //            }
        //                //        }
        //                //        break;

        //                //    case StatusLineConstants.AUTO_RUNNING_STATE:
        //                //    case StatusLineConstants.AUTO_STOPPING_STATE:
        //                //    case StatusLineConstants.AUTO_STOP:
        //                //    case StatusLineConstants.AUTO_PAUSE:
        //                //    case StatusLineConstants.AUTO_HALT:
        //                //    case StatusLineConstants.AUTO_FINISH:
        //                //        { 
        //                //            ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                //            StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(statusKeyWords);
        //                //            ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                //            _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                //        }
        //                //        break;

        //                //    //这里应该放在WatchAlarmClear中
        //                //    case StatusLineConstants.AUTO_START:
        //                //        if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSED.ToString())
        //                //            ClearAlarm(line);
        //                //        break;
        //                //    default:
        //                //        break;
        //                //}
        //                #endregion
        //                #region oldcode


        //                /*switch (statusKeyWords)
        //                {
        //                    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                    case StatusLineConstants.PORT1_READY_STATUS:
        //                    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                        {
        //                            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                            _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));

        //                            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                            _portState = portStatusEnum.ToString();
        //                            if (_portState != PortStatusEnum.UDONE.ToString())
        //                            {
        //                                ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                                StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
        //                                ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                                _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                            }

        //                        }
        //                        break;
        //                    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                    case StatusLineConstants.PORT2_READY_STATUS:
        //                    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                        {
        //                            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                            _secs.Send(S6F11(new uint[] { 217 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));

        //                            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                            PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                            _portState = portStatusEnum.ToString();
        //                            if (_portState != PortStatusEnum.UDONE.ToString())
        //                            {
        //                                ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                                StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
        //                                ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                                _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                            }
        //                        }
        //                        break;

        //                    case StatusLineConstants.AUTO_RUNNING_STATE:
        //                    case StatusLineConstants.AUTO_STOPPING_STATE:
        //                    case StatusLineConstants.AUTO_STOP:
        //                    case StatusLineConstants.AUTO_PAUSE:
        //                    case StatusLineConstants.AUTO_HALT:
        //                    case StatusLineConstants.AUTO_FINISH:
        //                        {
        //                            ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                            StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(statusKeyWords);
        //                            ProcessStatusEnum currentProcessStatusEnum = _procesState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                            _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                        }
        //                        break;

        //                    //这里应该放在WatchAlarmClear中
        //                    case StatusLineConstants.AUTO_START:
        //                        if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSED.ToString())
        //                            ClearAlarm(line);
        //                        break;
        //                    default:
        //                        break;
        //                }*/
        //                #endregion
        //                //send _port1State、_port2State change event
        //                switch (statusKeyWords)
        //                {
        //                    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                    case StatusLineConstants.PORT1_READY_STATUS:
        //                    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                        PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                        _secs.Send(S6F11(new uint[] { 216 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port1StatusEnum).ToString() })));
        //                        break;
        //                    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                    case StatusLineConstants.PORT2_READY_STATUS:
        //                    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                        PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                        _secs.Send(S6F11(new uint[] { 217 }, new uint[] { 10 }, new List<string>(new string[] { ((int)port2StatusEnum).ToString() })));
        //                        break;
        //                    default:
        //                        break;
        //                }

        //                //update _semiStatus
        //                switch (statusKeyWords)
        //                {
        //                    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                    case StatusLineConstants.PORT1_READY_STATUS:
        //                    case StatusLineConstants.PORT1_UDONE_STATUS:
        //                    case StatusLineConstants.PORT2_IDLE_STATUS:
        //                    case StatusLineConstants.PORT2_SETUP_STATUS:
        //                    case StatusLineConstants.PORT2_READY_STATUS:
        //                    case StatusLineConstants.PORT2_UDONE_STATUS:
        //                        PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State);
        //                        PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State);
        //                        PortStatusEnum portStatusEnum = _portState2DArray[(int)port1StatusEnum, (int)port2StatusEnum];
        //                        _portState = portStatusEnum.ToString();
        //                        //if (portStatusEnum == PortStatusEnum.SETUP) new Thread(WatchPortReadyEvent).Start();
        //                        if (_portState != PortStatusEnum.UDONE.ToString())
        //                        {
        //                            ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                            StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(_portState);
        //                            ProcessStatusEnum currentProcessStatusEnum = _processState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                            //_semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                            _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? ProcessStatusEnum.INIT.ToString() : currentProcessStatusEnum.ToString();
        //                        }
        //                        break;

        //                    case StatusLineConstants.AUTO_RUNNING_STATE:
        //                    case StatusLineConstants.AUTO_STOPPING_STATE:
        //                    case StatusLineConstants.AUTO_STOP:
        //                    case StatusLineConstants.AUTO_PAUSE:
        //                    case StatusLineConstants.AUTO_HALT:
        //                    case StatusLineConstants.AUTO_FINISH:
        //                        {
        //                            ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                            StatusLineEnum statusLineEnum = StatusLineConstants.ConvertStringToStatusLineEnum(statusKeyWords);
        //                            ProcessStatusEnum currentProcessStatusEnum = _processState2DArray[(int)previousProcessStatusEnum, (int)statusLineEnum];

        //                            _semiStatus = currentProcessStatusEnum == ProcessStatusEnum.notfound ? _semiStatus : currentProcessStatusEnum.ToString();
        //                        }
        //                        break;

        //                    //这里应该放在WatchAlarmClear中
        //                    case StatusLineConstants.AUTO_START:
        //                        //if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSED.ToString())
        //                        //if (_alarmsQueue.Count > 0 && _alarmsQueue.Peek() == "0039")
        //                        //    ClearAlarm(line);
        //                        break;
        //                    default:
        //                        break;
        //                }
        //                //状态改变，发StreamFunction消息
        //                if (_previousSemiStatus != _semiStatus)
        //                {
        //                    ProcessStatusEnum processStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus);
        //                    ProcessStatusEnum previousprocessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _previousSemiStatus);
        //                    _secs.Send(S6F11(new uint[] { 208 }, new uint[] { 2 }, new List<string>(new string[] { ((int)processStatusEnum).ToString(), ((int)previousprocessStatusEnum).ToString() })));
        //                    this.Invoke(new Action(() => txtSemiStatus.Text = _semiStatus));
        //                    _logger.Info("_semiStatus : " + _semiStatus);
        //                }

        //                #region oldcode

        //                ////判断关键字
        //                //switch (statusKeyWords)
        //                //{
        //                //    case StatusLineConstants.AUTO_HOME_START:
        //                //    case StatusLineConstants.AUTO_HOME_ABORT:
        //                //    case StatusLineConstants.AUTO_HOME_FINISH:
        //                //    case StatusLineConstants.PORT1_IDLE_STATUS:
        //                //        _semiStatus = StatusEnum.ProcessStatusEnum.IDLE.ToString();
        //                //        break;

        //                //    case StatusLineConstants.PORT1_SETUP_STATUS:
        //                //        _semiStatus = StatusEnum.ProcessStatusEnum.SETUP.ToString();
        //                //        break;

        //                //    case StatusLineConstants.PORT1_READY_STATUS:
        //                //        _semiStatus = StatusEnum.ProcessStatusEnum.READY.ToString();
        //                //        break;

        //                //    case StatusLineConstants.AUTO_START:
        //                //        if (_semiStatus == StatusEnum.ProcessStatusEnum.A_PAUSE.ToString())
        //                //        {
        //                //            ClearAlarm(line);
        //                //        }
        //                //        break;

        //                //    case StatusLineConstants.AUTO_RUNNING_STATE:
        //                //        _semiStatus = StatusEnum.ProcessStatusEnum.RUNNING.ToString();
        //                //        break;

        //                //    case StatusLineConstants.AUTO_STOP:
        //                //    case StatusLineConstants.AUTO_STOPPING_STATE:
        //                //        _semiStatus = StatusEnum.ProcessStatusEnum.STOPPING.ToString();
        //                //        break;

        //                //    case StatusLineConstants.AUTO_PAUSE:
        //                //        if (!string.IsNullOrEmpty(_alarmCode))
        //                //        {
        //                //            _semiStatus = StatusEnum.ProcessStatusEnum.A_PAUSE.ToString();
        //                //        }
        //                //        else
        //                //        {
        //                //            _semiStatus = StatusEnum.ProcessStatusEnum.PAUSING.ToString();
        //                //        }
        //                //        break;

        //                //    case StatusLineConstants.AUTO_FINISH:
        //                //        if (_semiStatus == StatusEnum.ProcessStatusEnum.PAUSING.ToString())
        //                //        {
        //                //            _semiStatus = StatusEnum.ProcessStatusEnum.PAUSED.ToString();
        //                //        }
        //                //        if (_semiStatus == StatusEnum.ProcessStatusEnum.STOPPING.ToString())
        //                //        {
        //                //            _semiStatus = StatusEnum.ProcessStatusEnum.IDLE.ToString();
        //                //        }
        //                //        break;

        //                //    case StatusLineConstants.AUTO_HALT:
        //                //        _semiStatus = StatusEnum.ProcessStatusEnum.PAUSING.ToString();
        //                //        break;

        //                //    default:
        //                //        break;
        //                //}

        //                #endregion
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(JsonConvert.SerializeObject(ex));
        //    }
        //    finally
        //    {
        //        _previousSemiStatus = _semiStatus;
        //    }
        //}

        private void WatchEvent(object lstLins)
        {
            try
            {
                List<string> recentStLins = (List<string>)lstLins;
                //List<string> eventLines = recentStLins.FindAll(x =>
                //            //移除时间字符(0, 9)
                //            x.Remove(0, 9).StartsWith("H1 Get Port1 Run!") ||
                //            x.Remove(0, 9).StartsWith("H1 Put Port1 Run!") ||
                //            x.Remove(0, 9).StartsWith("H1 Get Port2 Run!") ||
                //            x.Remove(0, 9).StartsWith("H1 Put Port2 Run!") ||
                //            x.Remove(0, 9).StartsWith("LOT OPERATION START.") ||
                //            x.Remove(0, 9).StartsWith("LOT OPERATION FINISH.") ||
                //            x.Remove(0, 9).StartsWith("ABORT OPERATION.") ||
                //            x.Remove(0, 9).StartsWith("GR1 READY ON") ||
                //            x.Remove(0, 9).StartsWith("GR2 READY ON") ||
                //            x.Remove(0, 9).StartsWith("AUTO MODE SELECT") ||
                //            x.Remove(0, 9).StartsWith("SEMI AUTO MODE SELECT") ||
                //            x.Remove(0, 9).StartsWith("MANUAL MODE SELECT")
                //            );
                List<string> eventLines = recentStLins.FindAll(x =>
                            //移除时间字符(0, 9)
                            x.Remove(0, 9).StartsWith(EventLineConstants.H1_GET_PORT1_RUN) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.H1_PUT_PORT1_RUN) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.H1_GET_PORT2_RUN) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.H1_PUT_PORT2_RUN) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.LOT_OPERATION_START) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.PORT_1_FINISH) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.PORT_2_FINISH) ||
                            //x.Remove(0, 9).StartsWith(EventLineConstants.LOT_OPERATION_FINISH) ||
                            //x.Remove(0, 9).StartsWith(EventLineConstants.ABORT_OPERATION) ||                            
                            x.Remove(0, 9).StartsWith(EventLineConstants.GR1_READY_ON) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.GR2_READY_ON) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.AUTO_MODE_SELECT) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.SEMI_AUTO_MODE_SELECT) ||
                            x.Remove(0, 9).StartsWith(EventLineConstants.MANUAL_MODE_SELECT)
                            );

                _logger.Info(JsonConvert.SerializeObject(eventLines, Formatting.Indented));

                lock (this)
                {
                    foreach (var line in eventLines)
                    {
                        if (line.Contains(EventLineConstants.H1_GET_PORT1_RUN))
                        {
                            var indexOfWaferGot = int.Parse(line.Remove(0, 9).Split(':')[1][0].ToString(), System.Globalization.NumberStyles.HexNumber);
                            //这里可以使用EventHandle-DateTable方式，但没有什么意义，直接发送s6f11更简单直接
                            _secs.Send(S6F11(new uint[] { 209 }, new uint[] { 3 }, new List<string>(new string[] { indexOfWaferGot.ToString() })));
                        }
                        else if (line.Contains(EventLineConstants.H1_PUT_PORT1_RUN))
                        {
                            var indexOfWaferPut = int.Parse(line.Remove(0, 9).Split(':')[1][0].ToString(), System.Globalization.NumberStyles.HexNumber);
                            _secs.Send(S6F11(new uint[] { 210 }, new uint[] { 4 }, new List<string>(new string[] { indexOfWaferPut.ToString() })));
                        }
                        if (line.Contains(EventLineConstants.H1_GET_PORT2_RUN))
                        {
                            var indexOfWaferGot = int.Parse(line.Remove(0, 9).Split(':')[1][0].ToString(), System.Globalization.NumberStyles.HexNumber);
                            _secs.Send(S6F11(new uint[] { 218 }, new uint[] { 3 }, new List<string>(new string[] { indexOfWaferGot.ToString() })));
                        }
                        else if (line.Contains(EventLineConstants.H1_PUT_PORT2_RUN))
                        {
                            var indexOfWaferPut = int.Parse(line.Remove(0, 9).Split(':')[1][0].ToString(), System.Globalization.NumberStyles.HexNumber);
                            _secs.Send(S6F11(new uint[] { 219 }, new uint[] { 4 }, new List<string>(new string[] { indexOfWaferPut.ToString() })));
                        }
                        else if (line.Contains(EventLineConstants.LOT_OPERATION_START))
                        {
                            _secs.Send(S6F11(new uint[] { 211 }, new uint[] { 5 }, new List<string>(new string[] { "LOT OPERATION START." })));

                            List<string> eventValuelist = new List<string>();
                            SecsMessage secsMessage = S1F4(new List<uint>() { 4, 5, 6, 3007, 3008, 3009, 3023, 3024, 3025, 3001, 3003, 3017, 3019, 13, 14, 15 });
                            foreach (var item in secsMessage.Items)
                            {
                                List<SecsItem> secsItemList = (List<SecsItem>)item.Value;
                                foreach (var secsItem in secsItemList)
                                {
                                    eventValuelist.Add(secsItem.Value.ToString());
                                }
                            }
                            _secs.Send(S6F11(new uint[] { 213 }, new uint[] { 7 }, eventValuelist));
                        }
                        else if (line.Contains(EventLineConstants.PORT_1_FINISH) || line.Contains(EventLineConstants.PORT_2_FINISH))
                        {
                            var countOfWafer = int.Parse(line.Split('.')[1].Trim(' ').Split('/')[0]);
                            _secs.Send(S6F11(new uint[] { 212 }, new uint[] { 6 }, new List<string>(new string[] { "LOT OPERATION FINISH.", countOfWafer.ToString() })));
                        }
                        //else if (line.Contains(EventLineConstants.LOT_OPERATION_FINISH))
                        //{
                        //    var countOfWafer = int.Parse(line.Split('=')[1].Trim(' '));
                        //    _secs.Send(S6F11(new uint[] { 212 }, new uint[] { 6 }, new List<string>(new string[] { "LOT OPERATION FINISH.", countOfWafer.ToString() })));
                        //}
                        //else if (line.Contains(EventLineConstants.ABORT_OPERATION))
                        //{
                        //    var countOfWafer = int.Parse(line.Split('=')[1].Trim(' '));
                        //    _secs.Send(S6F11(new uint[] { 212 }, new uint[] { 6 }, new List<string>(new string[] { "ABORT OPERATION.", countOfWafer.ToString() })));
                        //}
                        else if (line.Contains(EventLineConstants.GR1_READY_ON))
                        {
                            List<string> eventValuelist = new List<string>();
                            SecsMessage secsMessage = S1F4(new List<uint>() { 7, 9 });
                            foreach (var item in secsMessage.Items)
                            {
                                List<SecsItem> secsItemList = (List<SecsItem>)item.Value;
                                foreach (var secsItem in secsItemList)
                                {
                                    eventValuelist.Add(secsItem.Value.ToString());
                                }
                            }
                            _secs.Send(S6F11(new uint[] { 214 }, new uint[] { 8 }, eventValuelist));
                        }
                        else if (line.Contains(EventLineConstants.GR2_READY_ON))
                        {
                            List<string> eventValuelist = new List<string>();
                            SecsMessage secsMessage = S1F4(new List<uint>() { 8, 10 });
                            foreach (var item in secsMessage.Items)
                            {
                                List<SecsItem> secsItemList = (List<SecsItem>)item.Value;
                                foreach (var secsItem in secsItemList)
                                {
                                    eventValuelist.Add(secsItem.Value.ToString());
                                }
                            }
                            _secs.Send(S6F11(new uint[] { 215 }, new uint[] { 9 }, eventValuelist));
                        }
                        else if (line.Contains(EventLineConstants.SEMI_AUTO_MODE_SELECT))
                        {
                            _secs.Send(S6F11(new uint[] { 2001 }, new uint[] { 1 }, new List<string>(new string[] { EventLineConstants.SEMI_AUTO_MODE_SELECT })));
                        }
                        else if (line.Contains(EventLineConstants.AUTO_MODE_SELECT))
                        {
                            _secs.Send(S6F11(new uint[] { 2001 }, new uint[] { 1 }, new List<string>(new string[] { EventLineConstants.AUTO_MODE_SELECT })));
                        }
                        else if (line.Contains(EventLineConstants.MANUAL_MODE_SELECT))
                        {
                            _secs.Send(S6F11(new uint[] { 2001 }, new uint[] { 1 }, new List<string>(new string[] { EventLineConstants.MANUAL_MODE_SELECT })));
                        }
                        else
                        {

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(JsonConvert.SerializeObject(ex));
            }
        }

        private void WatchAlarmClear_winState()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    //如果当前有报警，则轮询报警窗口，窗口消失则消警
                    if (_autoIt.WinGetState("[CLASS:Alarm]") == 7)
                    {
                        continue;
                    }

                    if (_alarmsQueue.Count() == 0)
                    {
                        continue;
                    }
                    //消警
                    DataTable dt = new DataTable();
                    DataColumn dc1 = new DataColumn("GenerationDate", typeof(DateTime));
                    DataColumn dc2 = new DataColumn("Command", typeof(Int32));
                    DataColumn dc3 = new DataColumn("Param1", typeof(String));
                    dt.Columns.AddRange(new DataColumn[] { dc1, dc2, dc3 });

                    _logger.Info($"_alarmsQueue: {JsonConvert.SerializeObject(_alarmsQueue, Formatting.Indented)}");
                    DataRow dr = dt.NewRow();
                    dr["GenerationDate"] = DateTime.Now;
                    dr["Command"] = 2;
                    dr["Param1"] = _alarmsQueue.Peek();
                    dt.Rows.Add(dr);
                    AlarmHandler(dt);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
        }
        private void WatchAlarmClear_opeLog(object lstLins)
        {

            List<string> recentStLins = (List<string>)lstLins;
            List<string> eventLines = recentStLins.FindAll(x =>
                        x.Contains(":0051"));

            _logger.Info(JsonConvert.SerializeObject(eventLines, Formatting.Indented));

            lock (this)
            {
                foreach (var line in eventLines)
                {
                    try
                    {
                        if (line.Contains(":0051"))
                        {
                            //消警
                            DataTable dt = new DataTable();
                            DataColumn dc1 = new DataColumn("GenerationDate", typeof(DateTime));
                            DataColumn dc2 = new DataColumn("Command", typeof(Int32));
                            DataColumn dc3 = new DataColumn("Param1", typeof(String));
                            dt.Columns.AddRange(new DataColumn[] { dc1, dc2, dc3 });

                            _logger.Info($"_alarmsQueue: {JsonConvert.SerializeObject(_alarmsQueue, Formatting.Indented)}");
                            DateTime.TryParse(line.Substring(0, 8), out DateTime dateTime);
                            DataRow dr = dt.NewRow();
                            dr["GenerationDate"] = dateTime;
                            dr["Command"] = 2;
                            dr["Param1"] = _alarmsQueue.Peek();
                            dt.Rows.Add(dr);
                            AlarmHandler(dt);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(JsonConvert.SerializeObject(ex));
                    }
                }
            }
        }

        private void WatchAlarmSet(object lstLins)
        {
            List<string> recentStLins = (List<string>)lstLins;
            _logger.Info("AlarmLine : " + recentStLins);


            lock (this)
            {
                //轮询2秒
                for (int i = 0; i < 200; i++)
                {
                    //不报警为5 报警为7
                    int alarmWindowState = _autoIt.WinGetState("[CLASS:Alarm]");
                    //_logger.Debug("alarmWindowState: " + alarmWindowState);
                    if (alarmWindowState == 7)
                    {
                        break;
                    }
                    if (i == 199)
                    {
                        return;
                    }
                    Thread.Sleep(10);
                }


                foreach (var line in recentStLins)
                {
                    try
                    {
                        //[0039]报警不会在机台屏幕上显示，不记录该报警
                        //if (line.Contains("[0039]"))
                        //{
                        //    continue;
                        //}

                        //获取AlarmCode
                        int startIndex = line.IndexOf("[") + 1;
                        int endIndex = line.IndexOf("]");
                        //int alarmCode = int.Parse(line.Substring(startIndex, endIndex-startIndex));
                        string alarmCode = line.Substring(startIndex, endIndex - startIndex);

                        _alarmCode = alarmCode;
                        _alarmInfo = line.Remove(0, 16);
                        //为alarmcode添加描述
                        if (!_dctAlarmDef.ContainsKey(_alarmCode))
                        {
                            _dctAlarmDef.Add(_alarmCode, _alarmInfo);
                        }

                        //报警
                        DataTable dt = new DataTable();
                        DataColumn dc1 = new DataColumn("GenerationDate", typeof(DateTime));
                        DataColumn dc2 = new DataColumn("Command", typeof(Int32));
                        DataColumn dc3 = new DataColumn("Param1", typeof(String));
                        dt.Columns.AddRange(new DataColumn[] { dc1, dc2, dc3 });

                        DateTime.TryParse(line.Substring(0, 8), out DateTime dateTime);
                        DataRow dr = dt.NewRow();
                        dr["GenerationDate"] = dateTime;
                        dr["Command"] = 1;
                        dr["Param1"] = alarmCode;
                        dt.Rows.Add(dr);
                        AlarmHandler(dt);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(JsonConvert.SerializeObject(ex));
                    }
                }
            }
        }

        //弃用
        //private void ClearAlarm()
        //{
        //    //避免set和clear间隔过短导致先触发clear后触发set。
        //    Thread.Sleep(1000);

        //    //_alarmCode = "0051";
        //    //_alarmInfo = "//Clear";
        //    //if (!_dctAlarmDef.ContainsKey(_alarmCode))
        //    //{
        //    //    _dctAlarmDef.Add(_alarmCode, _alarmInfo);
        //    //}
        //    //消警
        //    DataTable dt = new DataTable();
        //    DataColumn dc1 = new DataColumn("GenerationDate", typeof(DateTime));
        //    DataColumn dc2 = new DataColumn("Command", typeof(Int32));
        //    DataColumn dc3 = new DataColumn("Param1", typeof(String));
        //    dt.Columns.AddRange(new DataColumn[] { dc1, dc2, dc3 });

        //    DataRow dr = dt.NewRow();
        //    dr["GenerationDate"] = DateTime.Now;
        //    dr["Command"] = 2;
        //    //dr["Param1"] = "0051";//这里暂定为0051，具体应该是几以后再讨论
        //    _logger.Info($"_alarmsQueue: {JsonConvert.SerializeObject(_alarmsStack, Formatting.Indented)}");
        //    dr["Param1"] = _alarmsStack.Pop();
        //    dt.Rows.Add(dr);
        //    AlarmHandler(dt);
        //}

        ////两种情况会被调用 1、a_pause=>auto_start 2、:0051
        //private void ClearAlarm(string line)
        //{
        //    //避免set和clear间隔过短导致先触发clear后触发set。
        //    Thread.Sleep(1000);

        //    //_alarmCode = "0051";
        //    //_alarmInfo = "//Clear";
        //    //if (!_dctAlarmDef.ContainsKey(_alarmCode))
        //    //{
        //    //    _dctAlarmDef.Add(_alarmCode, _alarmInfo);
        //    //}
        //    //消警
        //    DataTable dt = new DataTable();
        //    DataColumn dc1 = new DataColumn("GenerationDate", typeof(DateTime));
        //    DataColumn dc2 = new DataColumn("Command", typeof(Int32));
        //    DataColumn dc3 = new DataColumn("Param1", typeof(String));
        //    dt.Columns.AddRange(new DataColumn[] { dc1, dc2, dc3 });

        //    DateTime.TryParse(line.Substring(0, 8), out DateTime dateTime);
        //    DataRow dr = dt.NewRow();
        //    dr["GenerationDate"] = dateTime;
        //    dr["Command"] = 2;
        //    //dr["Param1"] = "0051";//这里暂定为0051，具体应该是几以后再讨论
        //    _logger.Info($"_alarmsQueue: {JsonConvert.SerializeObject(_alarmsQueue, Formatting.Indented)}");
        //    dr["Param1"] = _alarmsQueue.Dequeue();
        //    dt.Rows.Add(dr);
        //    AlarmHandler(dt);
        //}

        public void AlarmHandler(DataTable dt)
        {
            try
            {
                //DataRow[] datarowsSet = dt.Select("MainCommand = 10002 and SubCommand =1", "GenerationDate desc");
                //DataRow[] datarowsClear = dt.Select("MainCommand = 10002 and SubCommand =2", "GenerationDate desc");

                //Command 1代表报警，2代表消警
                //这里，没有可能出现查询出多条记录的可能么？
                //不会的出现上述情况，因为每加一条数据，都会调用一次该方法，
                DataRow[] datarowsSet = dt.Select("Command = 1", "GenerationDate desc");
                DataRow[] datarowsClear = dt.Select("Command = 2", "GenerationDate desc");

                _logger.Info("datarowsSet : " + JsonConvert.SerializeObject(datarowsSet, Formatting.Indented));
                _logger.Info("datarowsClear : " + JsonConvert.SerializeObject(datarowsClear, Formatting.Indented));

                if (datarowsSet.Length > 0)
                {
                    try
                    {
                        var alarmSetTime = (DateTime)datarowsSet[0]["GenerationDate"];
                        if (alarmSetTime >= _previousAlarmSetTime)//为什么要做这个判断？
                        {
                            _previousAlarmSetTime = alarmSetTime;

                            //var alarmCode = int.Parse(datarowsSet[0]["Param1"].ToString());
                            var alarmCode = datarowsSet[0]["Param1"].ToString();
                            var alarmText = _dctAlarmDef[alarmCode];
                            //var alarmType = AutoItXNet20.GetAlarmText("[ID:5174]");

                            this.Invoke(new Action(() =>
                            {
                                txtAlarmCode.Text = alarmCode;
                                txtAlarm.Text = alarmText;
                            }));

                            //_logger.Info($"Alarm Set, Code:{alarmCode}, Text:{alarmType},{alarmText}");
                            _logger.Info($"Alarm Set, Code:{alarmCode}, Text:{alarmText}");

                            //var msg = S5F1((uint)alarmCode, $"{alarmType},{alarmText}", 128);
                            //var msg = S5F1(alarmCode, $"{alarmText}", 128);
                            var msg = S5F1(Convert.ToUInt32(alarmCode, 16), $"{alarmText}", 0x80);
                            _secs.Send(msg);
                            _alarmsQueue.Enqueue(alarmCode);
                        }
                    }
                    finally
                    {
                        //失败的send要不要清理掉？不清理会造成失败消息堆积。清理掉会丢失失败的记录。（失败记录可以在日志中找到）
                        //dt已经改为方法变量
                        dt.Rows.Remove(datarowsSet[0]);
                    }
                }


                if (datarowsClear.Length > 0)
                {
                    try
                    {
                        var alarmClearTime = (DateTime)datarowsClear[0]["GenerationDate"];

                        if (alarmClearTime >= _previousAlarmClearTime)//为什么要做这个判断？
                        {
                            _previousAlarmClearTime = alarmClearTime;

                            //var alarmCode = int.Parse(datarowsClear[0]["Param1"].ToString());
                            var alarmCode = datarowsClear[0]["Param1"].ToString();
                            var alarmText = _dctAlarmDef[alarmCode];
                            //var alarmType = AutoItXNet20.GetAlarmText("[ID:5174]");

                            //txtAlarm.Text = string.Empty;
                            //txtAlarmCode.Text = string.Empty;
                            this.Invoke(new Action(() =>
                            {
                                txtAlarmCode.Text = string.Empty;
                                txtAlarm.Text = string.Empty;
                            }));

                            //_logger.Info($"Alarm Clear, Code:{alarmCode}, Text:{alarmType},{alarmText}");
                            _logger.Info($"Alarm Clear, Code:{alarmCode}, Text:{alarmText}");

                            //var msg = S5F1((uint)alarmCode, $"{alarmType},{alarmText}", 0);
                            //var msg = S5F1(alarmCode, $"{alarmText}", 0);
                            var msg = S5F1(Convert.ToUInt32(alarmCode, 16), $"{alarmText}", 0x0);
                            _secs.Send(msg);
                            _alarmsQueue.Dequeue();
                        }
                    }
                    finally
                    {
                        //失败的send要不要清理掉？不清理会造成失败消息堆积。清理掉会丢失失败的记录。（失败记录可以在日志中找到）
                        //dt已经改为方法变量
                        dt.Rows.Remove(datarowsClear[0]);
                    }
                }


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }




    }
}
