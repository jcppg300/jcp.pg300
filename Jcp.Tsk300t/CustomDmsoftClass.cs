﻿using Dm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Jcp.Pg300
{
    class CustomDmsoftClass : dmsoftClass
    {
        public CustomDmsoftClass()
        {
            SetPath(Application.StartupPath);
            SetDict(0, "dm_soft.txt");
        }
        public CustomDmsoftClass(string Handle) : this()
        {

            if (Handle == "")
            {
                throw new Exception("Handle is Empty, error");
            }
            int hwnd = Convert.ToInt32(Handle, 16);
            BindWindow(hwnd, "gdi", "normal", "normal", 0);
            //BindWindow(hwnd, "normal", "normal", "normal", 0);
        }
    }
}