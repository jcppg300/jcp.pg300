﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    internal class StatusLineConstants
    {
        internal const string PORT1_IDLE_STATUS = "PORT1 IDLE STATUS : 0";
        internal const string PORT1_SETUP_STATUS = "PORT1 SETUP STATUS : 1";
        //internal const string PORT1_READYKEYDOWN_STATUS = "PORT1 ST WAFER INFO TO ST : 4";
        internal const string PORT1_READY_STATUS = "PORT1 READY STATUS : 2";
        internal const string PORT1_UDONE_STATUS = "PORT1 UDONE STATUS : 6";
        
        internal const string PORT2_IDLE_STATUS = "PORT2 IDLE STATUS : 0";
        internal const string PORT2_SETUP_STATUS = "PORT2 SETUP STATUS : 1";
        //internal const string PORT2_READYKEYDOWN_STATUS = "PORT2 ST WAFER INFO TO ST : 4";
        internal const string PORT2_READY_STATUS = "PORT2 READY STATUS : 2";
        internal const string PORT2_UDONE_STATUS = "PORT2 UDONE STATUS : 6"; 



        internal const string AUTO_START = "AUTO START";
        internal const string AUTO_RUNNING_STATE = "AUTO RUNNING STATE";
        internal const string AUTO_STOPPING_STATE = "AUTO STOPPING STATE";
        
        internal const string AUTO_STOP = "AUTO STOP";
        internal const string AUTO_PAUSE = "AUTO PAUSE";
        internal const string AUTO_HALT = "AUTO HALT";
        
        internal const string AUTO_FINISH = "AUTO FINISH";
        
        
        internal const string AUTO_HOME_START = "AUTO HOME START";
        internal const string AUTO_HOME_FINISH = "AUTO HOME FINISH";
        internal const string AUTO_HOME_ABORT = "AUTO HOME ABORT";
        internal const string AUTO_MODE_SELECT = "AUTO MODE SELECT";
        
        
        public static StatusLineEnum ConvertStringToStatusLineEnum(string StatusLineConstant)
        {
            StatusLineEnum statusLineEnum = new StatusLineEnum();

            switch (StatusLineConstant)
            {
                case "IDEL":
                    statusLineEnum = StatusLineEnum.PORT_IDLE_STATUS;
                    break;
                case "SETUP":
                    statusLineEnum = StatusLineEnum.PORT_SETUP_STATUS;
                    break;
                case "READY":
                    statusLineEnum = StatusLineEnum.PORT_READY_STATUS;
                    break;
                case "AUTO RUNNING STATE":
                    statusLineEnum = StatusLineEnum.AUTO_RUNNING_STATE;
                    break;
                case "AUTO STOPPING STATE":
                    statusLineEnum = StatusLineEnum.AUTO_STOPPING_STATE;
                    break;
                case "AUTO STOP":
                    statusLineEnum = StatusLineEnum.AUTO_STOP;
                    break;
                case "AUTO PAUSE":
                    statusLineEnum = StatusLineEnum.AUTO_PAUSE;
                    break;
                case "AUTO HALT":
                    statusLineEnum = StatusLineEnum.AUTO_HALT;
                    break;
                case "AUTO FINISH":
                    statusLineEnum = StatusLineEnum.AUTO_FINISH;
                    break;                

                default:
                    break;
            }

            return statusLineEnum;
        }
    }

    internal enum StatusLineEnum
    {
        PORT_IDLE_STATUS,
        PORT_SETUP_STATUS,
        PORT_READY_STATUS,
        AUTO_RUNNING_STATE,
        AUTO_STOPPING_STATE,
        AUTO_STOP,
        AUTO_PAUSE,
        AUTO_HALT,
        AUTO_FINISH,

        PORT1_IDLE_STATUS,
        PORT1_SETUP_STATUS,
        PORT1_READY_STATUS,
        PORT2_IDLE_STATUS,
        PORT2_SETUP_STATUS,
        PORT2_READY_STATUS,
        AUTO_START,
        AUTO_HOME_START,
        AUTO_HOME_FINISH,
        AUTO_HOME_ABORT,
        AUTO_MODE_SELECT,

    }

    
}
