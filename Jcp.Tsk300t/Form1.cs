﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SecsTesterDemo
{
    public partial class Form1 : Form
    {
        public static Thread thread;
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("USER32.DLL", EntryPoint = "FindWindowEx", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32.dll")]
        public static extern int GetWindowText(int hwnd, StringBuilder lpString, int nMaxCount);//获取窗体标题名称
        [DllImport("user32.dll")]
        public static extern int GetClassName(int hwnd, StringBuilder lpstring, int nMaxCount); //获取窗体类名称      
                                                                                                //给CheckBox发送信息
                                                                                                //给Text发送信息
        [DllImport("USER32.DLL", EntryPoint = "SendMessage")]
        private static extern void SendMessage(IntPtr hwnd, int wMsg, int wParam, StringBuilder lParam);
        [DllImport("USER32.DLL")]
        public static extern void keybd_event(Byte bVk, Byte bScan, Int32 dwFlags, Int32 dwExtraInfo);
        public delegate bool CallBack(IntPtr hwnd, int lParam);
        [DllImport("user32.dll")]
        public static extern int EnumChildWindows(IntPtr hWndParent, CallBack lpfn, int lParam);
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public Form1()
        {

            ThreadStart start = WaitAlarmDialogOpen;
            thread = new System.Threading.Thread(start);
            thread.IsBackground = true;
            thread.Start();
            InitializeComponent();
            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 1000 * 3;//定时周期10毫秒
            t.Elapsed += T_Elapsed;
            t.Enabled = true;//是否不断重复定时操作
            t.Start();//开始定时器


        }

        private void T_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IntPtr ab = new IntPtr();
            StringBuilder className = new StringBuilder(256);
            IntPtr hWnd = FindWindow(null, "句柄测试");
            if (hWnd != IntPtr.Zero)
            {
                //设置游戏窗口到最前
                SetForegroundWindow(hWnd);
            }


            IntPtr childHwnd = FindWindowEx(hWnd, ab, "WindowsForms10.EDIT.app.0.378734a", null);


            //var parentID=AutoItX.WinGetHandle("句柄测试");
            //var childId = AutoItX.ControlGetHandle(parentID, "[CLASS:WindowsForms10.EDIT.app.0.378734a; INSTANCE:1]");
            //var childId = AutoItX.ControlGetHandle(parentID, "WindowsForms10.EDIT.app.0.378734a2");
          // var cc = AutoItX.ControlGetText(parentID, childId);
            if (childHwnd != IntPtr.Zero)
            {
               // var bb = AutoItX.ControlGetText(hWnd, childHwnd);
                StringBuilder strbu = new StringBuilder(102);
                SendMessage(childHwnd, 0x0d, 100, strbu);
                var aa = strbu.ToString();
            }
        }
        private void WaitAlarmDialogClosed()
        {
            //AutoItX.WinWaitClose("Alarm");
         
            thread.Abort();
        }

        private void WaitAlarmDialogOpen()
        {
            //AutoItX.WinWaitActive("Alarm");
            //var txt = AutoItX.WinGetText("Alarm");
            WaitAlarmDialogClosed();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // AutoItX.ControlSend("句柄测试", "", "[CLASS:WindowsForms10.EDIT.app.0.378734a; INSTANCE:1]", "This is some text");

           
        }
    }
}
