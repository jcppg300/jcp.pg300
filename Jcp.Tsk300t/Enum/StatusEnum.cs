﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    public class StatusEnum
    {
       public enum ProductStatusEnum
        {
            //39页
            Startup=1,
            Autostop=2,
            Pause=3,
            ProductWait =4,
            Autorun = 5,
            Manualstop=6,
            Manualrun=7,
            Maintenance=8,
            Calibration=9,
            Bladereplacing=10,
            Cutterset=11,
            Tablechange=12,
            ModelRegist=15,
            KerfRegist=16,

        }
        public enum EquipementStatusEnum
        {
            Offline = 1,
        }
        public enum ProcessStatusEnum
        {
            INIT = 0,//未获取到机台状态
            IDLE = 1,//stopping时auto finish则idle,或者port1，2 idle keyline 同时出现时idle
            SETUP = 2,//port1/2 setup keyline 出现时setup
            READY = 3,//port1/2 ready keyline 出现时ready
            RUNNING = 4,//auto running
            STOPPING = 5,//auto atop 或 auto stopping
            PAUSING = 6,//auto pause出现时不存在报警则PAUSING; auto halt则pausing
            PAUSED = 7,//PAUSING时发生auto finish，则PAUSED
            A_PAUSING = 8,//存在报警时发生auto pause则A_PAUSEING,A_PAUSEING之后遇到auto finish则A_PAUSED，//有两种恢复路径，1、手动点击消警（:0051），此时操作仅删除报警框，并未真正消警，真正消警需要遇到auto  2、机台自动消警（自动消警完成的标志：auto finish或auto start）
            A_PAUSED = 9,
            E_STOP = 10,
            ABORT = 11,
            INIT_RUNNING = 12,

            notfound = 99//未定义
        }
        public enum PortStatusEnum
        {
            INIT = 0,//未获取到机台状态
            IDLE = 1,//stopping时auto finish则idle,或者port1，2 idle keyline 同时出现时idle
            SETUP = 2,//port1/2 setup keyline 出现时setup
            READY = 3,//port1/2 ready keyline 出现时ready
            UDONE = 4,//port1/2 ready keyline 出现时ready
            //READYKEYDOWN = 5,

            notfound//未定义
        }

    }
}
