﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    public enum EventEnum
    {
        CassetteOn=208,
        CassetteOff=208,
        CassetteSet=208,//
        StartButton=205,
        LoadCassette =206, //secs manual does not contain this ceid. this id is decided by us and liutao
        StartCassetteProcess=209,
        CompleteCassetteProcess=210,
        StartLoadWafer=217,
        StartCutting=251,
        EndCutting=255,
        StatusChange=301,
        AlignmentStart=285,
        AlignmentEnd=286,
        KerfCheckStart=287,
        SpinnerCleaningStart=226,
        SpinnerCleaningComplete=227,
        /// <summary>
        /// 刀痕检测结束
        /// </summary>
        CompleteAutoKerfCheck =412,
        /// <summary>
        /// 测高结束
        /// </summary>
        CompleteOpcCutterSet = 1306,
    }
}
