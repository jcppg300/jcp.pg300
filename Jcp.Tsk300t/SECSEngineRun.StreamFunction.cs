﻿using Ctg.XTap.SecsGem;
using Jcp.Pg300.RcpParser;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static Jcp.Pg300.StatusEnum;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun
    {

        private static uint _dataId =0;
        public static uint DataId { get => ++_dataId; }

        public SecsMessage S1F2()
        {
            var msg = new SecsMessage(1, 2, false);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = _secsItemManager.CreateItem("MDLN");
            item1.Value = "TSK PG300";
            itemList.AddItem(item1);
            var item2 = _secsItemManager.CreateItem("SOFTREV");
            item2.Value = "V1.0";
            itemList.AddItem(item2);
            msg.Items.Add(itemList);
            //_logger.Info("S1F2:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }
        //public SecsMessage S5F1(uint errorCode, string errorMsg)
        //{
        //    var msg = new SecsMessage(5, 1, true);
        //    SecsItemList itemList = new SecsItemList("L");
        //    var item1 = _secsItemManager.CreateItem("ALCD");
        //    item1.Value = new byte[] { Convert.ToByte(128) };
        //    itemList.AddItem(item1);
        //    var item2 = _secsItemManager.CreateItem("ALID");
        //    item2.Value = new uint[] { errorCode };
        //    itemList.AddItem(item2);
        //    var item3 = _secsItemManager.CreateItem("ALTX");
        //    item3.Value = errorMsg;
        //    itemList.AddItem(item3);
        //    msg.Items.Add(itemList);
        //    _logger.Info("S5F1:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
        //    return msg;
        //}

        public SecsMessage S5F1(uint errorCode, string errorMsg, int setclear)
        {
            var msg = new SecsMessage(5, 1, true);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = _secsItemManager.CreateItem("ALCD");
            item1.Value = new byte[] { Convert.ToByte(setclear) };
            itemList.AddItem(item1);
            var item2 = _secsItemManager.CreateItem("ALID");
            item2.Value = new uint[] { errorCode };
            itemList.AddItem(item2);
            var item3 = _secsItemManager.CreateItem("ALTX");
            item3.Value = errorMsg;
            itemList.AddItem(item3);
            msg.Items.Add(itemList);
            //_logger.Info("S5F1:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }
        public SecsMessage S5F1(string errorCode, string errorMsg, int setclear)
        {
            var msg = new SecsMessage(5, 1, true);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = _secsItemManager.CreateItem("ALCD");
            item1.Value = new byte[] { Convert.ToByte(setclear) };
            itemList.AddItem(item1);
            var item2 = _secsItemManager.CreateItem("ALID");
            item2.Value = new string[] { errorCode };
            itemList.AddItem(item2);
            var item3 = _secsItemManager.CreateItem("ALTX");
            item3.Value = errorMsg;
            itemList.AddItem(item3);
            msg.Items.Add(itemList);
            //_logger.Info("S5F1:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }

        public SecsMessage S1F14()
        {
            var msg = new SecsMessage(1, 14, false);
            SecsItemList itemLista = new SecsItemList("L2a");
            var item = _secsItemManager.CreateItem("COMMACK");
            item.Value = new byte[] { Convert.ToByte(0) };
            itemLista.AddItem(item);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = _secsItemManager.CreateItem("MDLN");
            item1.Value = "TSK PG300";
            itemList.AddItem(item1);
            var item2 = _secsItemManager.CreateItem("SOFTREV");
            item2.Value = "V1.0";
            itemList.AddItem(item2);
            itemLista.AddItem(itemList);
            msg.Items.Add(itemLista);
            //_logger.Info("S1F2:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }
        //public SecsMessage S1F4(uint svid)
        //{
        //    var msg = new SecsMessage(1, 4, false);
        //    SecsItemList itemList = new SecsItemList("L");
        //    SecsItemAscii itemAscii = new SecsItemAscii("undefined");

        //    switch (svid)
        //    {
        //        case SvidDef.RecipeName:
        //            var item1 = _secsItemManager.CreateItem("SV");
        //            var textValue = GetValueFromText(@"C:\dicer\acct_sys\DicerGUI.ini");
        //            //_logger.Info(" textValue.Count：" + textValue.Count); 
        //            if (!string.IsNullOrEmpty(textValue["Rec"]))
        //            {
        //                item1.Value = textValue["Rec"];
        //            }
        //            itemList.AddItem(item1);
        //            break;
        //        case SvidDef.ProcessState://34353:
        //            btnGetfromForm_Click(null, null);
        //            var item = new SecsItemBinary("SV");
        //            int statusId = (int)Enum.Parse(typeof(StatusEnum.ProcessStatusEnum), txtSemiStatus.Text);
        //            var value = new byte[] { Convert.ToByte(statusId) };
        //            item.Value = value;
        //            itemList.AddItem(item);
        //            break;
        //        case SvidDef.Sp1CutLineLength:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp1CutLineLength].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp1Exposure:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp1Exposure].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp1Height:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp1Height].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp1ModelName:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp1ModelName].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp1RotationSpeed:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp1RotationSpeed].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp2CutLineLength:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp2CutLineLength].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp2Exposure:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp2Exposure].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp2Height:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp2Height].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp2ModelName:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp2ModelName].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        case SvidDef.Sp2RotationSpeed:
        //            itemAscii = new SecsItemAscii("SV", _dctSvidDef[SvidDef.Sp2RotationSpeed].Value.ToString());
        //            itemList.AddItem(itemAscii);
        //            break;
        //        default:
        //            break;
        //    }

        //    msg.Items.Add(itemList);
        //    //_logger.Info("S1F4:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
        //    return msg;
        //}

        public SecsMessage S1F4(List<uint> lstSvid)
        {
            Dictionary<uint, string> dctVidValue = new Dictionary<uint, string>();
            lstSvid.ForEach(x => dctVidValue.Add(x, string.Empty));

            var lstNormalSvid = lstSvid.FindAll(x => x <= 1000);
            if (lstNormalSvid != null && lstNormalSvid.Count > 0)
            {
                foreach (var svid in lstSvid)
                {
                    switch (svid)
                    {
                        case SvidDef.RecipeName:
                            var vidVal = GetCurrentRecipeName();
                            dctVidValue[svid] = vidVal;
                            break;
                        case SvidDef.ProcessState:
                            ProcessStatusEnum processStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _semiStatus.ToUpper());
                            dctVidValue[svid] = ((int)processStatusEnum).ToString();
                            break;
                        case SvidDef.PreviousProcessState:
                            ProcessStatusEnum previousProcessStatusEnum = (ProcessStatusEnum)Enum.Parse(typeof(ProcessStatusEnum), _previousSemiStatus.ToUpper());
                            dctVidValue[svid] = ((uint)previousProcessStatusEnum).ToString();
                            break;
                        case SvidDef.ControlState:
                            dctVidValue[svid] = _controlState ? "1" : "0";
                            break;
                        case SvidDef.Port1State:
                            PortStatusEnum port1StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port1State.ToUpper());
                            dctVidValue[svid] = ((int)port1StatusEnum).ToString();
                            break;
                        case SvidDef.Port2State:
                            PortStatusEnum port2StatusEnum = (PortStatusEnum)Enum.Parse(typeof(PortStatusEnum), _port2State.ToUpper());
                            dctVidValue[svid] = ((int)port2StatusEnum).ToString();
                            break;

                        case SvidDef.AIR_SOURCE_PRESSURE:
                            dctVidValue[svid] = GetValue("AIR SOURCE PRESSURE");
                            break;
                        case SvidDef.VACUUM_SOURCE_PRESSURE:
                            dctVidValue[svid] = GetValue("VACUUM SOURCE PRESSURE");
                            break;
                        case SvidDef.GR1_BLADE_HEIGHT:
                            dctVidValue[svid] = GetValue("GR-1 BLADE HEIGHT");
                            break;
                        case SvidDef.GR2_BLADE_HEIGHT:
                            dctVidValue[svid] = GetValue("GR-2 BLADE HEIGHT");
                            break;
                        case SvidDef.GRINDING1_St_CHUCK_VACUUM:
                            dctVidValue[svid] = GetValue("GRINDING-1 St CHUCK VACUUM");
                            break;
                        case SvidDef.GRINDING2_St_CHUCK_VACUUM:
                            dctVidValue[svid] = GetValue("GRINDING-2 St CHUCK VACUUM");
                            break;
                        case SvidDef.GRINDING1_St_WHEEL_REVOLVING:
                            dctVidValue[svid] = GetValue("GRINDING-1 St WHEEL REVOLVING");
                            break;
                        case SvidDef.GRINDING2_St_WHEEL_REVOLVING:
                            dctVidValue[svid] = GetValue("GRINDING-2 St WHEEL REVOLVING");
                            break;
                        case SvidDef.GRINDING1_St_COOLANT_FLOWRATE:
                            dctVidValue[svid] = GetValue("GRINDING-1 St COOLANT FLOWRATE");
                            break;
                        case SvidDef.GRINDING2_St_COOLANT_FLOWRATE:
                            dctVidValue[svid] = GetValue("GRINDING-2 St COOLANT FLOWRATE");
                            break;
                        

                        default:
                            break;
                    }
                }
            }

            var lstGeneralSvid = lstSvid.FindAll(x => x > 1000 && x <= 2000);
            if (lstGeneralSvid != null && lstGeneralSvid.Count > 0)
            {
                var filePath = Path.Combine(Constants.ParamFolder, "GENERAL.PRM");
                var parser = new RecipeParser(PrmType.General, File.ReadAllBytes(filePath));

                FillValuesByIds(dctVidValue, lstGeneralSvid, parser);
            }

            var lstLimitSvid = lstSvid.FindAll(x => x > 2000 && x <=3000);
            if (lstLimitSvid != null && lstLimitSvid.Count > 0)
            {
                var filePath = Path.Combine(Constants.ParamFolder, "MONLIMIT.PRM");
                var parser = new RecipeParser(PrmType.Limit, File.ReadAllBytes(filePath));

                FillValuesByIds(dctVidValue, lstLimitSvid, parser);
            }

            var lstRecipeSvid = lstSvid.FindAll(x => x > 3000 && x <= 4000);
            if (lstRecipeSvid != null && lstRecipeSvid.Count > 0)
            {
                var filePath = Path.Combine(Constants.RecipeFolder, GetCurrentRecipeName());
                var parser = new RecipeParser(PrmType.Recipe, File.ReadAllBytes(filePath));

                FillValuesByIds(dctVidValue, lstRecipeSvid, parser);
            }

            var lstSystemSvid = lstSvid.FindAll(x => x > 4000 && x <= 5000);
            if (lstSystemSvid != null && lstSystemSvid.Count > 0)
            {
                var filePath = Path.Combine(Constants.ParamFolder, "SYSTEM.PRM");
                var parser = new RecipeParser(PrmType.System, File.ReadAllBytes(filePath));

                FillValuesByIds(dctVidValue, lstSystemSvid, parser);
            }

            var msg = new SecsMessage(1, 4, false);
            SecsItemList itemList = new SecsItemList("L");

            foreach(var item in dctVidValue)
            {
                var itemAscii = new SecsItemAscii("SV", item.Value);
                itemList.AddItem(itemAscii);
            }

            msg.Items.Add(itemList);
            return msg;
        }

        public Dictionary<int, string> GetValuesByIds(List<uint> lstSvid, RecipeParser parser)
        {
            Dictionary<int, string> dctVidVal = new Dictionary<int, string>();

            if (lstSvid != null && lstSvid.Count > 0)
            {
                foreach (var item in lstSvid)
                {
                    string vidValue = string.Empty;
                    try
                    {
                        vidValue = parser.GetValueById((int)item);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }

                    dctVidVal.Add((int)item, vidValue);
                }
            }

            return dctVidVal;
        }

        public void FillValuesByIds(Dictionary<uint, string> dctVidVal, List<uint> lstSvid, RecipeParser parser)
        {
            if (lstSvid != null && lstSvid.Count > 0)
            {
                foreach (var item in lstSvid)
                {
                    string vidValue = string.Empty;
                    try
                    {
                        vidValue = parser.GetValueById((int)item);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }

                    dctVidVal[item] = vidValue;
                }
            }
        }



        //private SecsMessage S7F6(string ppId)
        //{
        //    var msg = new SecsMessage(7, 6, false);
        //    //SecsItemAscii item = new SecsItemAscii("PPID");
        //    //item.Value = ppId;
        //    //msg.Items.Add(item);
        //    SecsItemList itemList = new SecsItemList("L");
        //    var item1 = _secsItemManager.CreateItem("PPID");
        //    item1.Value = ppId;
        //    itemList.AddItem(item1);
        //    var item2 = _secsItemManager.CreateItem("PPBODY");
        //    var bodyBanry = ReturnPPBody(ppId);
        //    item2.Value = bodyBanry;
        //    itemList.AddItem(item2);
        //    msg.Items.Add(itemList);
        //    //_logger.Info("S7F6:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
        //    return msg;
        //}

        //private SecsMessage S7F6(string ppid, byte[] ppbody)
        private SecsMessage S7F6(string ppid, byte[] ppbody, byte[] odbody)
        {      
            var msg = new SecsMessage(7, 6, false);

            SecsItemList itemList = new SecsItemList("L");
            var item1 = _secsItemManager.CreateItem("PPID");
            item1.Value = ppid;
            itemList.AddItem(item1);

            var secsRcpBody = _secsItemManager.CreateItem("PPBODY");
            secsRcpBody.Value = Merge(ppbody, odbody);
            itemList.AddItem(secsRcpBody);

            //var secsOdBody = _secsItemManager.CreateItem("PPBODY");
            //secsOdBody.Value = odbody;
            //itemList.AddItem(secsOdBody);

            msg.Items.Add(itemList);
            return msg;
        }
        public byte[] Merge(byte[] mainRecipe, byte[] ordersetRecipe)
        {
            byte[] mergedBytes = new byte[1024 * 2];

            mainRecipe.CopyTo(mergedBytes, 0);
            ordersetRecipe.CopyTo(mergedBytes, 1024);

            return mergedBytes;
        }

        private SecsMessage S1F13()
        {
            var msg = new SecsMessage(1, 13, true);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = _secsItemManager.CreateItem("MDLN");
            item1.Value = "TSK PG300";
            itemList.AddItem(item1);
            var item2 = _secsItemManager.CreateItem("SOFTREV");
            item2.Value = "V1.0";
            itemList.AddItem(item2);
            msg.Items.Add(itemList);
            // _logger.Info("S1F13:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }

        /// <summary>
        /// true表示远程命令执行成功，false表示远程命令执行失败
        /// </summary>
        /// <param name="isSuccess"></param>
        /// <returns></returns>
        private SecsMessage S2F42(bool isSuccess)
        {
            var msg = new SecsMessage(2, 42, false);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = new SecsItemBinary("B");
            if (isSuccess)
            {
                item1.Value = new byte[] { Convert.ToByte(0) };
            }
            else
            {
                item1.Value = new byte[] { Convert.ToByte(1) };
            }
            itemList.AddItem(item1);
            msg.Items.Add(itemList);
            //_logger.Info("S2F42:" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }
        private SecsMessage S2F42(bool isSuccess, string errorText)
        {
            var msg = new SecsMessage(2, 42, false);
            try
            {

                SecsItemList itemList = new SecsItemList("L");
                var item1 = new SecsItemBinary("B");
                if (isSuccess)
                {
                    item1.Value = new byte[] { Convert.ToByte(0) };
                }
                else
                {
                    item1.Value = new byte[] { Convert.ToByte(1) };
                }
                itemList.AddItem(item1);
                    
                //item2加不进去
                var item2 = new SecsItemAscii("CPACK");
                item2.Value = errorText;
                itemList.AddItem(item2);

                msg.Items.Add(itemList);

            }
            catch (Exception e)
            {
                _logger.Error(JsonConvert.SerializeObject(e));
            }

            //_logger.Info("S2F42:" + JsonConvert.SerializeObject(msg.Items[1], Formatting.Indented));
            return msg;
        }
        private SecsMessage S6F11(uint[] ceId, uint[] reportId, List<string> values)
        {
            var msg = new SecsMessage(6, 11, true);
            var secsitemList = new SecsItemList("L3");
            var item1 = _secsItemManager.CreateItem("DATAID");
            item1.Value = new uint[] { DataId };
            var item2 = _secsItemManager.CreateItem("CEID");
            item2.Value = ceId;
            secsitemList.AddItem(item1);
            secsitemList.AddItem(item2);
            var secsitemLa = new SecsItemList("La");
            var secsitemL2 = new SecsItemList("L2");
            var item3 = _secsItemManager.CreateItem("RPTID");
            item3.Value = reportId;
            secsitemL2.AddItem(item3);
            var secsitemLb = new SecsItemList("Lb");
            if (values != null)
            {
                foreach (var value in values)
                {
                    var item4 = _secsItemManager.CreateItem("V");
                    item4.Value = value;
                    secsitemLb.AddItem(item4);
                }
            }
            secsitemL2.AddItem(secsitemLb);
            secsitemLa.AddItem(secsitemL2);
            secsitemList.AddItem(secsitemLa);
            msg.Items.Add(secsitemList);
            //_logger.Info("S6F11" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;

        }
        
        private SecsMessage S6F11(uint[] ceId, List<byte[]> values)
        {
            var msg = new SecsMessage(6, 11, true);
            var secsitemList = new SecsItemList("L3");
            var item1 = _secsItemManager.CreateItem("DATAID");
            item1.Value = new uint[] { 1 };
            var item2 = _secsItemManager.CreateItem("CEID");
            item2.Value = ceId;
            secsitemList.AddItem(item1);
            secsitemList.AddItem(item2);
            var secsitemLa = new SecsItemList("La");
            var secsitemL2 = new SecsItemList("L2");
            var item3 = _secsItemManager.CreateItem("RPTID");
            item3.Value = new uint[] { 1021 };
            secsitemL2.AddItem(item3);
            var secsitemLb = new SecsItemList("Lb");
            if (values != null)
            {
                foreach (var value in values)
                {
                    var item4 = new SecsItemU1("V");
                    item4.Value = value;
                    secsitemLb.AddItem(item4);
                }
            }
            secsitemL2.AddItem(secsitemLb);
            secsitemLa.AddItem(secsitemL2);
            secsitemList.AddItem(secsitemLa);
            msg.Items.Add(secsitemList);
            //_logger.Info("S6F11" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }
        private SecsMessage S10F4()
        {
            var msg = new SecsMessage(10, 4, false);
            SecsItemList itemList = new SecsItemList("L");
            var item1 = new SecsItemBinary("B");
            if (1 == 1)
            {
                item1.Value = new byte[] { Convert.ToByte(0) };
            }
            else
            {
                item1.Value = new byte[] { Convert.ToByte(1) };
            }
            itemList.AddItem(item1);
            msg.Items.Add(itemList);
            //_logger.Info("S10F4" + JsonConvert.SerializeObject(msg, Formatting.Indented));
            return msg;
        }


        private SecsMessage S7F2()
        {
            var msg = new SecsMessage(7, 2, false);
            //SecsItemList itemList = new SecsItemList("L");
            var item1 = new SecsItemBinary("B", new byte[] { 0 });
           
            //itemList.AddItem(item1);
            msg.Items.Add(item1);
           
            return msg;
        }

        private SecsMessage S7F4(int ackc7)
        {
            var msg = new SecsMessage(7, 4, false);
            //SecsItemList itemList = new SecsItemList("L");
            var item1 = new SecsItemBinary("B", new byte[] { (byte)ackc7 });

            //itemList.AddItem(item1);
            msg.Items.Add(item1);

            return msg;
        }






    }
}
