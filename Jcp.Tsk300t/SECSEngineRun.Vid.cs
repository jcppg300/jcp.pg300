﻿using Ctg.XTap.SecsGem;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun 
    {
        private Dictionary<int, SvidDef> _dctSvidDef = new Dictionary<int, SvidDef>();

        private void LoadSvidDef()
        {
            try
            {
                _dctSvidDef.Clear();

                _dctSvidDef.Add(SvidDef.RecipeName, new SvidDef { Id = SvidDef.RecipeName, Name = nameof(SvidDef.RecipeName) });
                _dctSvidDef.Add(SvidDef.ProcessState, new SvidDef { Id = SvidDef.ProcessState, Name = nameof(SvidDef.ProcessState) });
                

                _dctSvidDef.Add(SvidDef.Sp1KcWMin, new SvidDef { Id = SvidDef.Sp1KcWMin, Name = nameof(SvidDef.Sp1KcWMin) });
                _dctSvidDef.Add(SvidDef.Sp1KcHalfW, new SvidDef { Id = SvidDef.Sp1KcHalfW, Name = nameof(SvidDef.Sp1KcHalfW) });
                _dctSvidDef.Add(SvidDef.Sp1KcDy, new SvidDef { Id = SvidDef.Sp1KcDy, Name = nameof(SvidDef.Sp1KcDy) });
                _dctSvidDef.Add(SvidDef.Sp1KcChippingSize, new SvidDef { Id = SvidDef.Sp1KcChippingSize, Name = nameof(SvidDef.Sp1KcChippingSize) });
                _dctSvidDef.Add(SvidDef.Sp1KcWMax, new SvidDef { Id = SvidDef.Sp1KcWMax, Name = nameof(SvidDef.Sp1KcWMax) });
                _dctSvidDef.Add(SvidDef.Sp1KcChannel, new SvidDef { Id = SvidDef.Sp1KcChannel, Name = nameof(SvidDef.Sp1KcChannel) });

                _dctSvidDef.Add(SvidDef.Sp2KcWMin, new SvidDef { Id = SvidDef.Sp2KcWMin, Name = nameof(SvidDef.Sp2KcWMin) });
                _dctSvidDef.Add(SvidDef.Sp2KcHalfW, new SvidDef { Id = SvidDef.Sp2KcHalfW, Name = nameof(SvidDef.Sp2KcHalfW) });
                _dctSvidDef.Add(SvidDef.Sp2KcDy, new SvidDef { Id = SvidDef.Sp2KcDy, Name = nameof(SvidDef.Sp2KcDy) });
                _dctSvidDef.Add(SvidDef.Sp2KcChippingSize, new SvidDef { Id = SvidDef.Sp2KcChippingSize, Name = nameof(SvidDef.Sp2KcChippingSize) });
                _dctSvidDef.Add(SvidDef.Sp2KcWMax, new SvidDef { Id = SvidDef.Sp2KcWMax, Name = nameof(SvidDef.Sp2KcWMax) });
                _dctSvidDef.Add(SvidDef.Sp2KcChannel, new SvidDef { Id = SvidDef.Sp2KcChannel, Name = nameof(SvidDef.Sp2KcChannel) });

            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
        }

        

        private string GetBladeModelNameForSp1()
        {
            var textValue = GetValueFromText(@"C:\dicer\acct_sys\DicerGUI.ini");
            return Path.GetFileNameWithoutExtension(textValue["Bld1"]);
        }

        private string GetBladeModelNameForSp2()
        {
            var textValue = GetValueFromText(@"C:\dicer\acct_sys\DicerGUI.ini");
            return Path.GetFileNameWithoutExtension(textValue["Bld2"]);
        }

       

    }
}
