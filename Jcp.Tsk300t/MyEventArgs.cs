﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jcp.Pg300
{
    public  class MyEventArgs:EventArgs
    {
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
    }
}
