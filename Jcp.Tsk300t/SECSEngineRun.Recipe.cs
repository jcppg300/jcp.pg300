﻿using Ctg.XTap.SecsGem;


using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using System.Runtime.InteropServices;
using System.Drawing;
using ManagedWinapi.Windows;
using Jcp.Pg300;

namespace Jcp.Pg300
{
    public partial class SECSEngineRun
    {
        //private FileSystemWatcher _watcherCurrentRecipe;
        //private string _currentRecipeName = string.Empty;

        //private void InitCurrentRecipeWatching()
        //{
        //    _watcherCurrentRecipe = new FileSystemWatcher();
        //    _watcherCurrentRecipe.Changed += _watcherCurrentRecipe_Changed;
        //    _watcherCurrentRecipe.Path = Constants.AppFolder;
        //    _watcherCurrentRecipe.Filter = Constants.CurrentRecipeFile; // "*.*";
        //    _watcherCurrentRecipe.NotifyFilter = NotifyFilters.LastWrite |
        //        NotifyFilters.FileName |
        //        NotifyFilters.Size;
        //    _logger.Info(_watcherCurrentRecipe.Path + " Moniting");
        //    _watcherCurrentRecipe.EnableRaisingEvents = true;
        //}

        //private void _watcherCurrentRecipe_Changed(object sender, FileSystemEventArgs e)
        //{
        //    try
        //    {
        //        _currentRecipeName = GetCurrentRecipeName();
        //    }
        //    catch(Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //}

        private string GetCurrentRecipeName()
        {
            var lines = File.ReadAllLines(Path.Combine(Constants.AppFolder, Constants.CurrentRecipeFile));

            //var temp = "CSP12-170U-100I-TI2683-00";
            var rcpName = RemoveInvalidHexChars(lines[0]);

            _logger.Info($"current recipe:[{rcpName}]");
            return rcpName;
        }

        private string RemoveInvalidHexChars(string input)
        {
            return new string(input.Where(value =>
                (value >= 0x0020 && value <= 0xD7FF) ||
                (value >= 0xE000 && value <= 0xFFFD) ||
                value == 0x0009 ||
                value == 0x000A ||
                value == 0x000D).ToArray());
        }


        private List<string> RecipeList()
        {
            if (Directory.Exists(Constants.RecipeOrderSetFolder))
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(Constants.RecipeOrderSetFolder);
                var recipefiles = directoryInfo.GetFiles("*.*");
                var arrPpid = recipefiles.Select(x => x.Name).ToArray();

                Array.Sort(arrPpid, StringComparer.Ordinal);

                return arrPpid.ToList();
            }
            else
            {
                throw new Exception($"folder does not exist: {Constants.RecipeOrderSetFolder}");
            }
        }

        /// <summary>
        /// 必须要有返回值，不能throw exception
        /// </summary>
        /// <param name="rcpName"></param>
        /// <returns></returns>
        private byte[] RecipeGet(string rcpName)
        {
            string filefullname = Path.Combine(Constants.RecipeFolder, rcpName);
            byte[] btData = null;
            if (File.Exists(filefullname))
            {
                using (FileStream fs = new FileStream(filefullname, FileMode.Open, FileAccess.ReadWrite))
                {
                    btData = new byte[fs.Length];
                    fs.Read(btData, 0, btData.Length);
                }
            }
            else
            {
                _logger.Error($"failed to get recipe: {rcpName}");
                return new byte[] { };
            }
            return btData;
        }
        /// <summary>
        /// 必须要有返回值，不能throw exception
        /// </summary>
        /// <param name="odName"></param>
        /// <returns></returns>
        private byte[] OrderGet(string odName)
        {
            string filefullname = Path.Combine(Constants.RecipeOrderSetFolder, odName);
            byte[] btData = null;
            if (File.Exists(filefullname))
            {
                using (FileStream fs = new FileStream(filefullname, FileMode.Open, FileAccess.ReadWrite))
                {
                    btData = new byte[fs.Length];
                    fs.Read(btData, 0, btData.Length);
                }
            }
            else
            {
                _logger.Error($"failed to get recipe: {odName}");
                return new byte[] { };
            }
            return btData;
        }


        //private void RecipeAdd(string rcpName, byte[] rcpBody)
        //{
        //    string rcpFullName = Path.Combine(Constants.RecipeOrderSetFolder, rcpName);
        //    File.WriteAllBytes(rcpFullName, rcpBody);
        //}
        private void RecipeAdd(string rcpName, byte[] rcpBody, byte[] odBody)
        {
            string recipeBodydestFile = Path.Combine(Constants.RecipeFolder, rcpName);
            File.WriteAllBytes(recipeBodydestFile, rcpBody);

            string orderdestFile = Path.Combine(Constants.RecipeOrderSetFolder, rcpName);
            File.WriteAllBytes(orderdestFile, odBody);
        }

        /// <summary>
        /// check mode button is shown on topmost gui
        /// </summary>
        /// <returns></returns>
        private bool IsModeButtonShown()
        {
            var upColor = GetColor(new Point(837, 706));
            var upColorHex = HexConverter(upColor);
            _logger.Info("upcolor:" + upColor + " " + HexConverter(upColor));

            var downColor = GetColor(new Point(856, 715));
            var downColorHex = HexConverter(downColor);
            _logger.Info("downColor:" + downColor + " " + HexConverter(downColor));

            if (upColorHex == "#808000" && downColorHex == "#808000")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsReturnButtonShown()
        {
            var upColor = GetColor(new Point(845, 709));
            var upColorHex = HexConverter(upColor);
            _logger.Debug("upcolor:" + upColor + " " + HexConverter(upColor));

            var downColor = GetColor(new Point(845, 727));
            var downColorHex = HexConverter(downColor);
            _logger.Info("downColor:" + downColor + " " + HexConverter(downColor));

            if (upColorHex == "#00FFFF" && downColorHex == "#00FFFF")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //private bool SelectRecipe(string rcpName)
        //{
        //    _logger.Info($"try to select recipe:{rcpName}");

        //    if (IsModeButtonShown() == false)
        //    {
        //        throw new Exception("machine is not in proper state");
        //    }

        //    ////menu window
        //    //var sw = SystemWindow.FromPoint(20, 676);

        //    var title = "MainWinForPg300";
        //    //mainwin window
        //    var sw = SystemWindow.FromPoint(18, 18);
        //    sw.Title = title;

        //    //_autoItX.ControlClick("", "ButtonMenu", "[CLASS: Button; INSTANCE: 19]");

        //    var ret1 = _autoIt.ControlClick(title, "", "[ID:2]");
        //    _logger.Info("control click lot operation ret1:" + ret1);

        //    Thread.Sleep(50);
        //    var ret2 = _autoIt.ControlClick(title, "", "[ID:21]"); //display OrderEdit Dialog
        //    _logger.Info("control click order select ret2:" + ret2);

        //    Thread.Sleep(50);
        //    _autoIt.ControlClick("", "SHOW", "[ID:30600]"); //let listbox to be displayed

        //    _lstPpid = RecipeList();

        //    if(_lstPpid.Exists(x => x == rcpName) ==false)
        //    {
        //        throw new Exception("recipe does not exist:" + rcpName);
        //    }

        //    var index = _lstPpid.FindIndex(x => x == rcpName);
        //    _logger.Info("current recipe index: " + index);


        //    sw = SystemWindow.FromPoint(158, 388); 
        //    var listbox = SystemListBox.FromSystemWindow(sw); //build listbox window
        //    listbox.Select(index);

        //    Thread.Sleep(50);
        //    _autoIt.ControlClick("", "SHOW", "[ID:30600]"); //let listbox to be disappeared

        //    Thread.Sleep(50);
        //    _autoIt.ControlClick("", "SHOW", "[ID:32501]"); //click "OK" on OrderEdit Dialog

        //    Thread.Sleep(50);
        //    var ret10 = _autoIt.ControlClick("Operation Confirm", "Continue", "[ID:1]"); //click "OK" on Operation Confirm Dialog, wafer size confirm
        //    _logger.Info("control click wafersize-confirm:" + ret10);

        //    Thread.Sleep(50);
        //    var ret11 = _autoIt.ControlClick("Operation Confirm", "Confirm", "[ID:1]"); //click "OK" on Operation Confirm Dialog, select file confirm
        //    _logger.Info("control click selectfile-confirm:" + ret11);

        //    Thread.Sleep(50);
        //    _autoIt.ControlClick(title, "", "[ID:28]");

        //    return true;
        //}


        private SystemWindow FindPg300Window()
        {
            var lstSw = SystemWindow.FilterToplevelWindows(x => x.Process.ProcessName == "pg300"
                && x.Size.Height == 768
                && x.Size.Width == 1024);

            if (lstSw == null || lstSw.Count() <= 0)
            {
                throw new Exception("pg300 application must be foreground.");
            }

            return lstSw[0];
        }

        private bool SelectRecipe(string rcpName)
        {
            _logger.Info($"try to select recipe:{rcpName}");

            //int sleepInterval = 30;
            int sleepInterval = 100;
            var title = "PG300 Window";

            var sw = FindPg300Window();
            sw.Title = title;

            //var sw = SystemWindow.ForegroundWindow;
            //if (sw.Process.ProcessName.ToUpper() != "PG300")
            //{
            //    throw new Exception("pg300 application must be foreground.");
            //}

            ////if (IsModeButtonShown() == false)
            ////{
            ////    throw new Exception("pg300 wrong state, make sure pg300 is active and idle.");
            ////}

            //////mainwin window
            ////var sw = SystemWindow.FromPoint(18, 18);
            ////if(sw.Process.ProcessName.Contains("pg300") == false)
            ////{
            ////    throw new Exception("pg300 must be active and topmost window.");
            ////}

            //点击可能出现的返回按钮
            if (_autoIt.ControlCommand(title, "", "[ID:18]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:18]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:18]");
            if (_autoIt.ControlCommand(title, "", "[ID:28]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:28]");
            if (_autoIt.ControlCommand(title, "", "[ID:38]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:38]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:38]");
            if (_autoIt.ControlCommand(title, "", "[ID:48]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:48]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:48]");
            if (_autoIt.ControlCommand(title, "", "[ID:58]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:58]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:58]");
            if (_autoIt.ControlCommand(title, "", "[ID:68]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:68]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:68]");
            if (_autoIt.ControlCommand(title, "", "[ID:78]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:78]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:78]");
            if (_autoIt.ControlCommand(title, "", "[ID:88]", "IsVisible", "") == "1" && _autoIt.ControlCommand(title, "", "[ID:88]", "IsEnabled", "") == "1")
                _autoIt.ControlClick(title, "", "[ID:88]");

            var isVisibleForLotOperation = _autoIt.ControlCommand(title, "", "[ID:2]", "IsVisible", "");
            var isEnabledForLotOperation = _autoIt.ControlCommand(title, "", "[ID:2]", "IsEnabled", "");
            var isVisibleForOrderSelect = _autoIt.ControlCommand(title, "", "[ID:21]", "IsVisible", "");
            var isEnabledForOrderSelect = _autoIt.ControlCommand(title, "", "[ID:21]", "IsEnabled", "");

            _logger.Info("isVisibleForLotOperation:" + isVisibleForLotOperation);
            _logger.Info("isEnabledForLotOperation:" + isEnabledForLotOperation);
            _logger.Info("isVisibleForOrderSelect:" + isVisibleForOrderSelect);
            _logger.Info("isEnabledForOrderSelect:" + isEnabledForOrderSelect);

            if (isVisibleForLotOperation == "0" && isVisibleForOrderSelect == "0")
            {
                throw new Exception("pg300 application must be ready for selecting recipe, make sure 'Lot Operation' button is visible.");
            }


            var ret = _autoIt.ControlClick(title, "", "[ID:2]");
            if (ret != 1) throw new Exception("failed to click lot operation");

            Thread.Sleep(sleepInterval);
            ret = _autoIt.ControlClick(title, "", "[ID:21]"); //display OrderEdit Dialog
            if (ret != 1) throw new Exception("failed to click order select");

            Thread.Sleep(sleepInterval);
            ret = _autoIt.ControlClick("", "SHOW", "[ID:30600]"); //let listbox to be displayed
            if (ret != 1) throw new Exception("failed to click listbox dropdown button");

            _lstPpid = RecipeList();

            if (_lstPpid.Exists(x => x == rcpName) == false)
            {
                throw new Exception("recipe does not exist:" + rcpName);
            }

            var index = _lstPpid.FindIndex(x => x == rcpName);
            _logger.Info("current recipe index: " + index);

            Thread.Sleep(sleepInterval * 3);
            sw = SystemWindow.FromPoint(158, 388);
            var listbox = SystemListBox.FromSystemWindow(sw); //build listbox window
            listbox.Select(index);

            Thread.Sleep(sleepInterval);
            ret = _autoIt.ControlClick("", "SHOW", "[ID:30600]"); //let listbox to be disappeared
            if (ret != 1) throw new Exception("failed to let listbox to be disappeared");

            Thread.Sleep(sleepInterval);
            ret = _autoIt.ControlClick("", "SHOW", "[ID:32501]"); //click "OK" on OrderEdit Dialog
            if (ret != 1) throw new Exception("failed to click OK on OrderEdit Dialog");

            //12/8 inch wafer切换的时候，会出现此界面
            Thread.Sleep(sleepInterval);
            if (_autoIt.WinExists("Operation Confirm", "WaferSize") == 1)
            {
                Thread.Sleep(sleepInterval);
                ret = _autoIt.ControlClick("Operation Confirm", "Continue", "[ID:1]"); //click "OK" on Operation Confirm Dialog, wafer size confirm
                if (ret != 1) throw new Exception("failed to click OK for wafer size confirm");
            }

            Thread.Sleep(sleepInterval);
            ret = _autoIt.ControlClick("Operation Confirm", "Confirm", "[ID:1]"); //click "OK" on Operation Confirm Dialog, select file confirm
            if (ret != 1) throw new Exception("failed to click OK for select file confirm");

            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval*10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");
            //Thread.Sleep(sleepInterval * 10);
            //_logger.Info($"isEnabledForReturnOperation24:{_autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "")} isEnabledForReturnOperation28:{_autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "")}");


            Thread.Sleep(sleepInterval * 20);
            //检查是否选择成功，失败则抛出异常
            string isEnabledForReturnOperation28 = _autoIt.ControlCommand(title, "", "[ID:28]", "IsEnabled", "");
            string isEnabledForReturnOperation24 = _autoIt.ControlCommand(title, "", "[ID:24]", "IsEnabled", "");
            _logger.Info($"isEnabledForReturnOperation28:{isEnabledForReturnOperation28} isEnabledForReturnOperation24:{isEnabledForReturnOperation24}");
            if (isEnabledForReturnOperation28 != "1" && isEnabledForReturnOperation24 != "1") throw new Exception("failed");
            
            var ret28 = _autoIt.ControlClick(title, "", "[ID:28]"); //sometimes return button'id is 24 ??
            var ret24 = _autoIt.ControlClick(title, "", "[ID:24]");
            if (ret28 != 1 && ret24 != 1) throw new Exception("failed to click return button");

            return true;
        }
        private bool SetLotId(string lotId)
        {

            _logger.Info($"try to Write LotId:{lotId}");

            int sleepInterval = 30;
            var title = "PG300 Window";

            var sw = FindPg300Window();
            sw.Title = title;

            //关闭"O_K"界面
            var ret = _autoIt.ControlClick("[CLASS:WaferRecipeEdit]", "", "[ID:32501]");
            //if (ret != 1) throw new Exception("failed to click O_K button");
            if (ret != 1) _logger.Info("failed to click O_K button");
            Thread.Sleep(sleepInterval);

            var isVisibleForLotOperation = _autoIt.ControlCommand(title, "", "[ID:2]", "IsVisible", "");
            var isEnabledForLotOperation = _autoIt.ControlCommand(title, "", "[ID:2]", "IsEnabled", "");
            var isVisibleForSetLotIdSelect = _autoIt.ControlCommand(title, "", "[ID:22]", "IsVisible", "");
            var isEnabledForSetLotIdSelect = _autoIt.ControlCommand(title, "", "[ID:22]", "IsEnabled", "");
            _logger.Info("isVisibleForLotOperation:" + isVisibleForLotOperation);
            _logger.Info("isEnabledForLotOperation:" + isEnabledForLotOperation);
            _logger.Info("isVisibleForSetLotIdSelect:" + isVisibleForSetLotIdSelect);
            _logger.Info("isEnabledForSetLotIdSelect:" + isEnabledForSetLotIdSelect);
            if (isVisibleForLotOperation == "0" && isVisibleForSetLotIdSelect == "0")
            {
                throw new Exception("pg300 application must be ready for set LotId, make sure 'Lot Operation' button is visible.");
            }

            ret = _autoIt.ControlClick(title, "", "[ID:2]");
            if (ret != 1) throw new Exception("failed to click lot operation");
            Thread.Sleep(sleepInterval);

            ret = _autoIt.ControlClick(title, "", "[ID:22]");
            if (ret != 1) throw new Exception("failed to click setlotid select");
            Thread.Sleep(sleepInterval);

            //填写LotId
            ret = _autoIt.ControlSend("[CLASS:LotIdEnt]", "", "Edit1", lotId);
            if (ret != 1) throw new Exception("failed to write lotid");
            Thread.Sleep(sleepInterval);

            //点击port按钮
            var isVisibleForPort1Button = _autoIt.ControlCommand("[CLASS:LotIdEnt]", "", "[ID:32501]", "IsVisible", "");
            var isEnabledForPort1Button = _autoIt.ControlCommand("[CLASS:LotIdEnt]", "", "[ID:32501]", "IsEnabled", "");
            var isVisibleForPort2Button = _autoIt.ControlCommand("[CLASS:LotIdEnt]", "", "[ID:32508]", "IsVisible", "");
            var isEnabledForPort2Button = _autoIt.ControlCommand("[CLASS:LotIdEnt]", "", "[ID:32508]", "IsEnabled", "");
            _logger.Info("isVisibleForPort1Button:" + isVisibleForPort1Button);
            _logger.Info("isVisibleForPort2Button:" + isVisibleForPort2Button);
            _logger.Info("isEnabledForPort1Button:" + isEnabledForPort1Button);
            _logger.Info("isEnabledForPort2Button:" + isEnabledForPort2Button);
            if (isVisibleForPort1Button == "1" && isEnabledForPort1Button == "1")
            {
                ret = _autoIt.ControlClick("[CLASS:LotIdEnt]", "", "[ID:32501]");
                if (ret != 1) throw new Exception("failed to click Port1 operation");
            }
            else if (isVisibleForPort2Button == "1" && isEnabledForPort2Button == "1")
            {
                ret = _autoIt.ControlClick("[CLASS:LotIdEnt]", "", "[ID:32508]");
                if (ret != 1) throw new Exception("failed to click Port2 operation");
            }
            else
            {
                throw new Exception("failed to click Port1/Port2 operation,please check port lock state");
            }

            return true;
        }


        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("gdi32.dll")]
        private static extern int GetPixel(IntPtr hdc, Point p);

        public Color GetColor(Point p)
        {
            IntPtr hdc = GetDC(new IntPtr(0));//取到设备场景(0就是全屏的设备场景)
            int c = GetPixel(hdc, p);//取指定点颜色
            int r = (c & 0xFF);//转换R
            int g = (c & 0xFF00) / 256;//转换G
            int b = (c & 0xFF0000) / 65536;//转换B
            return Color.FromArgb(r, g, b);
        }

        private static String HexConverter(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private static String RGBConverter(System.Drawing.Color c)
        {
            return "RGB(" + c.R.ToString() + "," + c.G.ToString() + "," + c.B.ToString() + ")";
        }
    }
}
