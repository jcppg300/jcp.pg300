﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Jcp.Pg300
{
    
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new SECSEngineRun());
            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger("App").Info(JsonConvert.SerializeObject(e, Formatting.Indented));
                //MessageBox.Show(JsonConvert.SerializeObject(e, Formatting.Indented));
                //throw;
            }


        }
    }
}
