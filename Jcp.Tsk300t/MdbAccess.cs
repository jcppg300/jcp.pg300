﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace net20autox.Mdb
{
    class MdbAccess
    {
        private log4net.ILog _logger = log4net.LogManager.GetLogger("App");

        private OleDbConnection _connection = null;        

        public static string PB_Query = @"SELECT top 10 
                                        PB.PB00 as BldAttached, 
                                        PB.PB02 as BldRemoved, 
                                        PB.PB12 as WaferCount, 
                                        PB.PB07 as BladeName, 
                                        PB.PB14 as CutLine, 
                                        PB.PB13 as CutLength, 
                                        PB.PB36 as SpindleNo, 
                                        PB.PB08 as BladeLotName
                                        FROM PB
                                        Where (((PB.PB00)>#DATETIME#))
                                        ORDER BY PB.PB00 Desc";
        public static string PC_Query = @"SELECT top 10
                                        PC.[PC00] as BladeSetDate, 
                                        PC.[PC01] as 實行時間, 
                                        PC.[PC03] as 切割據距離, 
                                        PC.[PC04] as 切割線數, 
                                        PC.[PC05] as 測高高度, 
                                        PC.[PC10] as BladeName, 
                                        PC.[PC12] as 主轉軸編號, 
                                        PC.[PC16] as BladeExposure,
                                        PC.[PC13] as 工件
                                        FROM PC 
                                        Where (((PC.PC01)>#DATETIME#))
                                        order by PC.[PC01] desc";
        public static string PE_Query = @"SELECT top 10
                                        PE.[PE00] as 發生時間, 
                                        PE.[PE01] as 恢復日期, 
                                        PE.[PE02] as Error解除時間, 
                                        PE.[PE03] as ErrorNo, 
                                        PE.[PE06] as 訊息, 
                                        PE.[PE07] as 裝置狀況, 
                                        PE.[PE09] as 協助, 
                                        PE.[PE14] as 工件名稱
                                        FROM PE
                                        Where (((PE.PE00)>#DATETIME#))
                                        ORDER BY PE.[PE00] DESC
                                        ";
        public static string PM_Query = @"SELECT top 10
                                        PM.[PM00] as CstSetDate, 
                                        PM.[PM02] as 開始時間, 
                                        PM.[PM05] as 卡匣名偁, 
                                        PM.[PM06] as 放入片數, 
                                        PM.[PM08] as 切割片數, 
                                        PM.[PM12] as SP1切割距離, 
                                        PM.[PM13] as SP1切割刀數, 
                                        PM.[PM30] as SP2切削距離, 
                                        PM.[PM31] as SP2切割刀數, 
                                        PM.[PM25] as SP1BladeName, 
                                        PM.[PM40] as SP2BladeName
                                        FROM PM
                                        Where (((PM.PM00)>#DATETIME#))
                                        ORDER BY PM.[PM00] DESC";
        public static string PQ_Query = @"SELECT top 10 
                                        PQ.[PQ00] as CstStartDate, 
                                        PQ.[PQ01] as CstStatus, 
                                        PQ.[PQ02] as SlotNo, 
                                        PQ.[PQ03] as WorkStatus, 
                                        PQ.[PQ04] as WorkLoadDate, 
                                        PQ.[PQ07] as WRKIDFWRKID,
                                        PQ.[PQ09] as SpindleLot, 
                                        PQ.[PQ10],
                                        PQ.[PQ20]as CutSpeed,
                                        PQ.[PQ25]as ExecDate
                                        FROM PQ
                                        Where (((PQ.PQ00)>#DATETIME#))
                                        order by PQ.[PQ00] desc";
        public static string PR_Query = @"SELECT top 10
                                        PR.[PR00] as 卡匣處理日時, 
                                        PR.[PR02] as SlotNO, 
                                        PR.[PR04] as 開始日時, 
                                        PR.[PR07] as 品種名稱, 
                                        PR.[PR21] as 工件尺寸, 
                                        PR.[PR31] as 切割距離SP1, 
                                        PR.[PR32] as 切割刀數SP1, 
                                        PR.[PR45] as 切割距離SP2, 
                                        PR.[PR46] as 切割刀數SP2
                                        FROM PR
                                        Where (((PR.PR00)>#DATETIME#))
                                        ORDER BY PR.[PR00] DESC";
        public static string DA_Query = @"SELECT top 10
                                        DA.[DA002] as 記録日時, 
                                        DA.[DA005] as AUTO稼働時間, 
                                        DA.[DA006] as MANUAL稼働時間, 
                                        DA.[DA046] as 切削m数SP1, 
                                        DA.[DA047] as 切削m数SP2, 
                                        DA.[DA048] as 切割线数SP1, 
                                        DA.[DA049] as 切割线数SP2, 
                                        DA.[DA050] as DRESS切削時間, 
                                        DA.[DA076] as 生産時間_ProductiveTime 
                                        FROM DA
                                        Where (((DA.DA002)>#DATETIME#))
                                        ORDER BY DA.[DA002] DESC";
        public static string DC_Query = @"SELECT top 10
                                        DC.[DC001] as 刀片安装日期和时间, 
                                        DC.[DC003] as 刀片拆卸日期和时间, 
                                        DC.[DC008] as 刀片名称, 
                                        DC.[DC009] as 刀片批次名称, 
                                        DC.[DC010] as 刀片尺寸, 
                                        DC.[DC012] as 刀片伸直量, 
                                        DC.[DC014] as 切削m数, 
                                        DC.[DC015] as 切割线数量, 
                                        DC.[DC016] as DRESS切割m数,
                                        DC.[DC018] as 覆盖切割m数 
                                        FROM DC
                                        Where (((DC.DC001)>#DATETIME#))
                                        ORDER BY DC.[DC001] DESC";
        public static string DF_Query = @"SELECT top 10
                                        DF.[DF001] as 盒式处理开始时间, 
                                        DF.[DF003] as 插槽号, 
                                        DF.[DF008] as WRKID, 
                                        DF.[DF010] as 工作厚度, 
                                        DF.[DF014] as 切割开始时间, 
                                        DF.[DF015] as 切割恢复时间, 
                                        DF.[DF025] as CH1切割速度, 
                                        DF.[DF039] as 主轴转速SP1, 
                                        DF.[DF069] as 洗涤时间,
                                        DF.[DF070] as 卸载时间 
                                        FROM DF
                                        Where (((DF.DF001)>#DATETIME#))
                                        ORDER BY DF.[DF001] DESC";


        public void Start(string mdbFullPath)
        {
            //string connectionString =
            //        @"Provider=Microsoft.Jet.OLEDB.4.0;" +
            //        $@"Data Source={mdbFullPath};" +
            //        @"User Id=;Password=;";/// +
            //        //@"Mode=12;";
        
            string connectionString =
                    @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                    $@"Data Source={mdbFullPath};" +
                    @"User Id=;Password=;" +
                    @"Mode=Share Deny None;";

            _connection = new OleDbConnection(connectionString);
            
            //_connection.Open();
        }

        public DataTable GetDataTableByQuerying(string query)
        {
            try
            {
                _connection.Open();

                using (OleDbCommand command = new OleDbCommand(query, _connection))
                {
                    OleDbDataReader reader = command.ExecuteReader();

                    DataTable myDataTable = new DataTable();
                    myDataTable.Load(reader);
                    reader.Close();

                    return myDataTable;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
            finally
            {
                _connection.Close();
            }
        }

        public void DeleteDataTableByDay(int keepDays)
        {
            try
            {
                var datetime = DateTime.Now.AddDays(0 - keepDays);
                
                var queryTempate = "DELETE * FROM PX Where (((PX.PX00)<#DATETIME#))";
                queryTempate = queryTempate.Replace("DATETIME", datetime.ToString("yyyy/MM/dd HH:mm:ss"));

                string tableNames = "BCEKMQRWX";
                var lstQuery = new List<string>();
                foreach (var name in tableNames)
                {
                    var tableQuery = queryTempate.Replace('X', name);
                    lstQuery.Add(tableQuery);
                };

                foreach(var query in lstQuery)
                {
                    using (OleDbCommand command = new OleDbCommand(query, _connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }                                              
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }



        public void Stop()
        {
            _connection.Dispose();
        }
    }
}
