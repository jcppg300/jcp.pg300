﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Newtonsoft.Json;

namespace Jcp.Pg300
{
    internal class LinesAddedArgs : EventArgs
    {
        public List<string> LstLins { get; set; }
    }

    internal class LogWatcher
    {
        log4net.ILog _logger  = log4net.LogManager.GetLogger("App");

        FileSystemWatcher _watcher;

        private long _currentFileLength = 0;
        private long _previousFileLength = 0;
        private List<string> _lstPreviousLines = null;
        private List<string> _lstCurrentLines = null;
        private List<string> _lstAddedLines = null;

        internal event EventHandler<LinesAddedArgs> LinesAddedEvent;

        internal LogWatcher(string monitorDir)
        {
            //_lstPreviousLines = LoadPreviousLines(monitorDir);
            _watcher = new FileSystemWatcher();
            _watcher.Changed += Watcher_Changed;
            _watcher.Path = monitorDir;
            _watcher.Filter = "*.*";
            _watcher.NotifyFilter = NotifyFilters.LastWrite |
                NotifyFilters.FileName |
                NotifyFilters.Size;
            _logger.Info(_watcher.Path + " Moniting");
            _watcher.EnableRaisingEvents = true;

        }

        //private List<string> LoadPreviousLines(string monitorDir)
        //{
        //    var latestFile = Directory.GetFiles(monitorDir).ToList()
        //        .Select(x => new FileInfo(x)).OrderByDescending(x => x.LastWriteTime).FirstOrDefault();

        //    return File.ReadAllLines(latestFile.FullName).ToList();
        //}

        //private void Watcher_Changed(object sender, FileSystemEventArgs e)
        //{
        //        try
        //        {
        //            _logger.Info(String.Format("File: {0} {1}", e.FullPath, e.ChangeType.ToString()));

        //            //删除去年明日的日志，避免BUG
        //            for (int i = 0; i < GetNextDayLogFileName().Length; i++)
        //            {
        //                if (File.Exists(_watcher.Path + GetNextDayLogFileName()[i]))
        //                {
        //                    File.Delete(_watcher.Path + GetNextDayLogFileName()[i]);
        //                }
        //            }

        //            //只检查当日日志
        //            if (!e.FullPath.EndsWith(GetCurrentDayLogSuffix()))
        //            {
        //                _logger.Info("非当日日志更改,忽略");
        //                return;
        //            }

        //            _lstCurrentLines = File.ReadAllLines(e.FullPath).ToList();
        //            _lstAddedLines = _lstCurrentLines.Except(_lstPreviousLines).ToList();

        //            _lstPreviousLines = _lstCurrentLines;

        //            if (LinesAddedEvent != null)
        //                LinesAddedEvent(this, new LinesAddedArgs() { LstLins = _lstAddedLines });
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error(ex);
        //        }
        //}
        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                _logger.Info(String.Format("File: {0} {1}", e.FullPath, e.ChangeType.ToString()));
                //只检查当日日志
                if (!e.FullPath.EndsWith(GetCurrentDayLogSuffix()))
                {
                    _logger.Info("not today log change , ignore");
                    return;
                }

                FileStream fileStream = File.Open(e.FullPath,FileMode.Open,FileAccess.Read,FileShare.ReadWrite);
                //_lstCurrentLines = File.ReadAllLines(e.FullPath).ToList();
                _currentFileLength = fileStream.Length;
                byte[] bytes = new byte[_currentFileLength];
                fileStream.Read(bytes, 0, (int)_currentFileLength);
                fileStream.Close();
                if (_previousFileLength > _currentFileLength) _previousFileLength = 0;//新的一天，_previousFileLength归0；
                byte[] addedBytes = new byte[_currentFileLength - _previousFileLength];
                Array.ConstrainedCopy(bytes, (int)_previousFileLength, addedBytes, 0, addedBytes.Length);
                string stringOfAddedBytes = Encoding.UTF8.GetString(addedBytes);
                //_lstAddedLines = _lstCurrentLines.Except(_lstPreviousLines).ToList();
                
                _lstAddedLines = new List<string>(stringOfAddedBytes.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
                //_lstPreviousLines = _lstCurrentLines;
                _previousFileLength = _currentFileLength;

                _logger.Info(JsonConvert.SerializeObject(_lstAddedLines, Formatting.Indented));
                LinesAddedEvent?.Invoke(this, new LinesAddedArgs() { LstLins = RemoveLinesByTime(_lstAddedLines, 0.1) });

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }


        private string GetCurrentDayLogSuffix()
        {
            DateTime today = DateTime.Now;

            string hexMonthSuffix = int.Parse(today.ToString("MM")).ToString("X");
            switch (hexMonthSuffix)
            {
                case "A":
                    hexMonthSuffix = "X";
                    break;
                case "B":
                    hexMonthSuffix = "Y";
                    break;
                case "C":
                    hexMonthSuffix = "Z";
                    break;
                default:
                    break;
            }
            string decimalDaySuffix = today.ToString("dd");
            string logSuffix = hexMonthSuffix + decimalDaySuffix;
            _logger.Info($"logSuffix: {logSuffix}");
            return logSuffix;
        }


        private List<string> RemoveLinesByTime(List<string> lstLins, double time)
        {
            List<string> filteredLstLins = new List<string>();
            foreach (var line in lstLins)
            {
                if (DateTime.TryParse(line, out DateTime dt)) continue;

                DateTime.TryParse(line.Substring(0, 8), out DateTime dateTime);
                if (dateTime.AddMinutes(time) > DateTime.Now) filteredLstLins.Add(line);
            }

            _logger.Info("filteredLstLins : " + JsonConvert.SerializeObject(filteredLstLins, Formatting.Indented));
            return filteredLstLins;
        }



    }





}
